@extends('layouts.app')

@section('content')
  
 <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-albums"></i></span>

            <div id="ca" class="info-box-content">
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa icon ion-closed-captioning"></i></span>

            <div id="pe" class="info-box-content">
             
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div id="Trx" class="info-box-content">
           
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div id="cst" class="info-box-content">
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-9 pull-left">
          <div class="box">

         <!--    <div class="box-header with-border">
              <h3 class="box-title">Grafik Penjualan</h3>
            </div> -->

            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
   
   <script src="{{ asset('/js/highcharts.js') }}"></script>
  <script src="{{ asset('/js//data.js') }}"></script>
  <script src="{{ asset('/js//drilldown.js') }}"></script>
  <script src="{{ asset('/js/dasboard.js') }}"></script>

   <div class="nbf"></div>
  <div id="container" style="float:left;border:1px solid #ddd;width: 100%; height: 400px; margin: 0 auto"></div>



      </div>  




</div> 


      </div>  
            </div>     


        </div>

        <div class="col-md-3 pull-right">
     <div class="box box-danger">
        <div class="box-header with-border">
              <h3 class="box-title">Bagikan Ke Media Sosial </h3>        
        </div>
  

                <div class="box-body no-padding">
                    <input type="hidden" name="sosmed" id="Sosmed">

                          <ul id="shrd"  class="users-list clearfix">

                           
                         </ul>  
                 
                </div>
              </div>
        
        </div> 

        <div class="col-md-3 pull-right">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Member Terbaru</h3>

                  <div id="mcount" class="box-tools pull-right">
                    
                    
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul id="latesM" class="users-list clearfix">
                   
                    

                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="javascript:void(0)" class="uppercase">View All Users</a>
                </div>
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>


 @role('admin')
<div class="col-md-9">

<div class="box box-info">
        <!--     <div class="box-header with-border">
              <h3 class="box-title">Transaksi Terahir</h3>

              <div class="box-tools pull-right">
                
              </div>
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
             
                <div id="container-cabang" style="float:left;border:1px solid #ddd;width: 100%; height: 400px; margin: 0 auto"></div>

             
                
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

       
          </div>

           <div class="col-md-3 pull-right">

           <div class="box box-default">
            <!-- <div class="box-header with-border">
              <h3 class="box-title">Makanan & Minuman</h3>

              
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  
                    <div id="pieChart"></div>
                  
                  <!-- ./chart-responsive -->
                </div>
                
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
            
            <!-- /.footer -->
          </div>
            
            </div>

</div> 
 @endrole   

</div> 
 
      				
      
   
@endsection





      
   





  


