
<div class="form-group col-sm-4">

</div>
<div class="form-group col-sm-6" style="margin-top:40px;">
<div class="form-group" >
    {!! Form::label('id', 'Code Promo:') !!}
    <p >{!! $post->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Promo:') !!}
    <p>{!! $post->title !!}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Description:') !!}
    <p>{!! $post->content !!}</p>
</div>


@if($type =="promo")
<!-- Start Date Field -->
<div class="form-group">
    {!! Form::label('start_date', 'Start Date:') !!}
    <p>{!! $post->start_date->format('d F Y') !!}</p>
</div>
<!-- End Date Field -->
<div class="form-group">
    {!! Form::label('end_date', 'End Date:') !!}
    <p>{!! $post->end_date->format('d F Y') !!}</p>
</div>
@endif


<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>@if($post->status == '1')Publish @else No Publish @endif</p>
</div>
</div>

<div class="form-group col-sm-12">
<div class="row">
<div class="col-md-6">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Scan Members</h3>

                  <div class="box-tools pull-right">
                    <span id="getcount" class="label label-danger"></span>
                    
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul id="getpoint" class="users-list clearfix">
                    <!-- @foreach($member as $member)
                    <li>
                        <img width="60" height="60" src="{!! empty($member->user->photo) ? config('app.photo_default') : url($member->user->photo)  !!} " alt="User Image">
                      <a class="users-list-name" href="#">{!! $member->user->name !!}</a>
                      <span class="users-list-date">{!! $member->created_at->diffForHumans() !!}</span>
                    </li>
                   @endforeach -->

                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div style="display:none;" class="open box-body no-padding">
                     <ul id="getpointall" class="users-list clearfix">
                    

                    </ul>    

                 </div>   

                <div class="box-footer text-center">
                  <a href="#" class="uppercase buka">View All Users</a>
                  <a style="display:none;" href="#" class="uppercase tutup">Close All Users</a>
                </div>


                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>


</div>    
</div>    



