@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Scan {!! $type !!}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('home.show_fields')
                    <div class="form-group col-sm-12">
                        <a href="{!! url('post?type='.$type) !!}" class="btn btn-default">Back</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
