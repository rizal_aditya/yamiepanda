<!-- Nama -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama Lengkap:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('cabang', 'Cabang:') !!}
    <select name="cabang" id="cabang" class="selectpicker form-control">
      <option value="0">Pilih Cabang</option>
    @foreach($cabang as $cabang)
          
                    <option value="{!! $cabang->id !!}" >{!! $cabang->name !!}</option>
             
    @endforeach
    </select>
</div>



<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control',"placeholder" => "Email"]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telp:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control',"placeholder" => "Telp"]) !!}
</div>



<div class="form-group col-sm-6">
    {!! Form::label('religion', 'Agama:') !!}
    <select id="religion" name="religion" class="selectpicker form-control">
      <option value="Other">Pilih Agama</option>
      <option value="Islam">Islam</option>
      <option value="Nasrani">Nasrani</option>
      <option value="Katolik">Katolik</option>
      <option value="Budha">Budha</option>
      <option value="Hindu">Hindu</option>
      <option value="Konghucu">Konghucu</option>
       <option value="Other">Lain - lain</option>
    </select>
    
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Tanggal Lahir:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control date']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Alamat:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>




<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, '') }} Aktif<br>
    {{ Form::radio('status', 0, '')  }} Tidak Aktif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('petugas.index') !!}" class="btn btn-default">Cancel</a>
</div>
