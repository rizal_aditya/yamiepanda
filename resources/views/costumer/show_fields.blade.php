<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Kode petugas:') !!}
    <p>{!! $petugas[0]->id !!}</p>
</div>

<!-- Discount Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p><img width="100" height="100" src="{!! $petugas[0]->photo !!}" ></p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('name', 'Nama Lengkap:') !!}
    <p>{!! $petugas[0]->name !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('cabang', 'Cabang:') !!}
    <p>{!! $petugas[0]->cabang !!}</p>
</div>

<!-- Post Id Field -->
<div class="form-group">
    {!! Form::label('phone', 'Telp:') !!}
    <p>{!! $petugas[0]->phone !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $petugas[0]->email !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('religion', 'Agama:') !!}
    <p>{!! $petugas[0]->religion !!}</p>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('brithday', 'Tanggal Lahir:') !!}
    <p>{!! $dates !!}</p>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('address', 'Alamat:') !!}
    <p>{!! $petugas[0]->address !!}</p>
</div>




<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>@if($petugas[0]->status =="0") Tidak Aktif @else Aktif @endif</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Dibuat:') !!}
    <p>{!! $create !!}</p>
</div>


