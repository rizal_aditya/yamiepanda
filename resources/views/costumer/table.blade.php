<table class="table table-responsive" id="penjualan-table">
    <thead>
        <th>Photo</th>
        <th>Nama Lengkap</th>
        <th>Email</th>
        <th>Telp</th>
        <th>Poin</th>
        <th>Type</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($costumer as $row)
        <tr>
           <td><img width="50" height="50" src="{!! $row->photo !!}"></td> 
                 
            <td>{!! $row->name !!}</td> 
            <td>{!! $row->email !!}</td> 
            <td>{!! $row->phone !!}</td> 
            <td>{!! $row->point !!}</td> 
            <td>{!! $row->type !!}</td> 
            <td>@if($row->status =="0" ) Tidak Aktif @else Aktif @endif</td>
            <td>
                     {!! Form::open(['route' => ['costumer.destroy', $row->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('costumer.show', [$row->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        @if($row->status =="0")
                             <a href="{!! url('costumer/'.$row->id.'/enable') !!}" class='btn btn-default btn-xs'><i class="fa fa-check"></i></a>
                        @else
                    <a href="{!! url('costumer/'.$row->id.'/disable') !!}" class='btn btn-default btn-xs'><i class="fa fa-times"></i></a>
                        @endif
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>     
        </tr>        
    @endforeach
    </tbody>
</table>