<table class="table table-responsive" id="voucher-table">
    <thead>
        <th>Name</th>
        <th>Point</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($voucher as $voucher)
        <tr>
            <td>{!! $voucher->name !!}</td>
            <td>{!! $voucher->point !!}</td>
            <td>@if($voucher->status =="0")No Publish@else Publish @endif</td>
            <td>
                {!! Form::open(['route' => ['voucher.destroy', $voucher->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('voucher.show', [$voucher->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('voucher.edit', [$voucher->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>