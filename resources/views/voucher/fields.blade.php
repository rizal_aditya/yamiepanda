<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Point Field -->
<div class="form-group col-sm-12">
    {!! Form::label('point', 'Point:') !!}
    {!! Form::number('point', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    {{ Form::radio('status', 1, isset($voucher) ? $voucher->status == 1 : false) }} Publish<br>
    {{ Form::radio('status', 0, isset($voucher) ? $voucher->status == 0 : true) }} No Publish
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('voucher.index') !!}" class="btn btn-default">Cancel</a>
</div>
