<table class="table table-responsive" id="device-table">
    <thead>
        <th>User Id</th>
        <th>Name</th>
        <th>Model</th>
        <th>Platform</th>
        <th>Platform Version</th>
        <th>Uuid</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($device as $device)
        <tr>
            <td>{!! $device->user_id !!}</td>
            <td>{!! $device->name !!}</td>
            <td>{!! $device->model !!}</td>
            <td>{!! $device->platform !!}</td>
            <td>{!! $device->platform_version !!}</td>
            <td>{!! $device->uuid !!}</td>
            <td>{!! $device->status !!}</td>
            <td>
                {!! Form::open(['route' => ['device.destroy', $device->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('device.show', [$device->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('device.edit', [$device->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>