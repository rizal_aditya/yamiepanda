<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $device->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $device->user_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $device->name !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $device->model !!}</p>
</div>

<!-- Platform Field -->
<div class="form-group">
    {!! Form::label('platform', 'Platform:') !!}
    <p>{!! $device->platform !!}</p>
</div>

<!-- Platform Version Field -->
<div class="form-group">
    {!! Form::label('platform_version', 'Platform Version:') !!}
    <p>{!! $device->platform_version !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $device->uuid !!}</p>
</div>

<!-- Gcm Field -->
<div class="form-group">
    {!! Form::label('gcm', 'Gcm:') !!}
    <p>{!! $device->gcm !!}</p>
</div>

<!-- Qrcode Field -->
<div class="form-group">
    {!! Form::label('qrcode', 'Qrcode:') !!}
    <p>{!! $device->qrcode !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $device->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $device->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $device->updated_at !!}</p>
</div>

