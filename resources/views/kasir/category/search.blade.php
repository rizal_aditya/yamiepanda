<div id="categori" class="ats">
  <div class="pull-left jdl">
    <h4>Category</h4>
  </div>

  <div  class="pull-right cac">
    <div class="box-tools">
        <div id="divsearch-category" class="input-group input-group-sm" style="width: 150px;">
          <input id="cari-category" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
          <div class="input-group-btn">
            <button id="search-category" type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- header category -->

<div id="mn-category" class="box-form form-group col-sm-12">
<ul class="category">
    @if($category->isEmpty())
            <li><div class="center"><h4>Category Not Found</h4></div></li>
    @else 
            @foreach($category as $category)
              <li>
                <div class="center">
                  <a onClick="GetCatalog({!! $category->id !!});"  href="#" >
                    <img src="{!! url($category->photo) !!}" width="70" >
                    <h4>{!! $category->name !!}</h4>
                  </a>
                </div>
              </li>
            @endforeach

    @endif   
</ul>
</div> 
<!-- list category -->
 <script src="{{ asset('/js/script-search.js') }}"></script>












