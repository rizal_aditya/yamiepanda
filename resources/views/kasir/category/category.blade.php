
<ul class="category ly">
    @if($category->isEmpty())
            <li>
                <div class="center">
                  <a  href="#" >
                    <img src="{!! url('img/no-images.jpg') !!}" width="70" >
                    <h4>Category Not found</h4>
                  </a>
                </div>
              </li>
    @else 
            @foreach($category as $category)
              <li>
                <div class="center">
                  <a onClick="GetViewCatalog({!! $category->id !!});"  href="#" >
                    <img src="{!! url($category->photo) !!}" width="70" >
                    <h4>{!! $category->name !!}</h4>
                  </a>
                </div>
              </li>
            @endforeach

    @endif   
</ul>

<!-- list category -->
