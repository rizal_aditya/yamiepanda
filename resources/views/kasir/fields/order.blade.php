<div id="categori" class="ats">
 <!--  <div class="pull-left jdl">
    <h4>Category</h4>
  </div> -->

  <div style="display:none;" class="pull-left bcv">
    <div  class="blk-category btn-default">
      <i class="fa fa-undo" aria-hidden="true"></i>
    </div>
  </div>
  <div style="display:none;"  class="nama-category"><h4 class="viewname"></h4></div>

<div  class="pull-right cac">
    <div class="box-tools">

    <div id="divsearch-category" class="input-group input-group-sm" style="width: 150px;">
      <input id="get-search-category" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      <div class="input-group-btn">
        <button id="search-category" type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </div>
    </div>

    <div  id="divsearch-catalog" class="input-group input-group-sm" style="width: 150px;display:none;">
      <input id="get-search-catalog" type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      <div id="categorykode"></div>
      <div class="input-group-btn">
        <button id="search-katalog" type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
      </div>
    </div>

  </div>
</div>

</div>




<div id="tr-bayar" style="display:none;"  class="ats">
     <div class="pull-left bcv">
          <div  class="back-category btn-default">
            <i class="fa fa-undo" aria-hidden="true"></i>
          </div>

           <div  style="display:none;" class="back-calculate btn-default">
            <i class="fa fa-undo" aria-hidden="true"></i>
          </div>

      </div>
          <div class="nama-tr"><h4>Transaction</h4></div>

<!-- <div class="pull-right cace">
 
</div> -->

  
</div>



<div id="mn-category" class="box-form form-group col-sm-12">
<ul class="category"></ul>
  <div style="display:none;" class="divcatalog">
  	<table class="table table-responsive">
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody id="catalog">
        </tbody>
    </table> 	
  </div>

<!--form bayar -->



</div> 

<div id="user_0" class="alert alert-danger" style="display:none;">
  <strong>Peringatan!</strong> Data costumer masih kosong.
</div> 

<div id="sutr" class="alert alert-danger" style="display:none;">
  <strong>Sukses!</strong> Data transaksi berhasil disimpan.
</div> 


<div id="cash" style="display:none;">
  <div id="alert"></div>
<div class="brs-kcl col-md-7">
    <div class="step">
      <input class="form-control" id="display" placeholder="Pay" type="text">
    </div>  

      <div id="hitung" ></div>
        <div class="step">
      <div id="form-tmbl" class="">
          <ul class="st">
             <li><a class="num-button seven btn btn-danger btn-lg">7</a></li>
             <li><a class="num-button eight btn btn-danger btn-lg">8</a></li>
             <li><a class="num-button nine btn btn-danger btn-lg">9</a></li>
             <li><a class="num-button four btn btn-danger btn-lg">4</a></li>
             <li><a class="num-button five btn btn-danger btn-lg">5</a></li>
             <li><a class="num-button six btn btn-danger btn-lg">6</a></li>
             <li><a class="num-button one btn btn-danger btn-lg">1</a></li>
             <li><a class="num-button two btn btn-danger btn-lg">2</a></li>
             <li><a class="num-button three btn btn-danger btn-lg">3</a></li>
             <li><a class="num-button zero nol btn btn-danger btn-lg">0</a></li>
             <li><a class="num-button dot koma btn btn-danger btn-lg">,</a></li>
          </ul>
      </div>
      <div id="form-tmbl2" class="">
      <ul class="s2">
              <li><a class="er clear-button clear btn-danger btn-lg"><i class="fa fa-times" aria-hidden="true"></i></a></li>
              <li>
                <a class="btn btn-danger btn-lg">
                <div class="passu">
                  <div id="pass-money"></div>
                  Uang Pass
                </div>
                </a>
              </li>
              <li><a id="sum" class="ery btn btn-danger btn-lg">=</a></li>
          </ul>
      </div>
     </div> 

     <div class="transaksi_end" style="display:none;">
          <div class="bord">
            <div class="head">Pembayaran</div>
          </div>
       
            <table class="colom" >
                <tr class="ift bayar"></tr>
                <tr class="ift total"></tr> 
                <tr class="ift">
                    <td class="cde"> Pembayaran </td> 
                    <td class="pull-left"> : </td> 
                    <td class="fer"> 
                      <input id ="tunai" class="pull-left" type="radio" name="jenis" value="cash" checked> <div id ="tunait" class="pull-left cqe"> Tunai </div>
                      <input id ="debit" class="pull-left" type="radio" name="jenis"  value="debit" > <div id ="debitt" class="pull-left cqe" > Debit </div>
                      <!-- <input id ="point" class="pull-left" type="radio" name="jenis"  value="point" > <div id ="pointt" class="pull-left cqe" > Point </div> -->
                    </td>
                </tr>
                <tr class="ift deb" style="display:none;">
                    <td class="cde"> Bank </td> 
                    <td class="pull-left"> : </td> 
                    <td class="fer"> 
                          <select id="bank" type="text" class="form-control selectpicker" name="bank">
                            <option value="0"> Pilih Bank</option>
                            <option value="bca"> BCA</option>
                            <option value="mandiri"> MANDIRI</option>
                            <option value="bri"> BRI</option>
                            <option value="bni"> BNI</option>
                           
                          </select>
                    </td>
                </tr>

                <tr class="ift deb" style="display:none;">
                    <td class="cde"> No ATM </td> 
                    <td class="pull-left"> : </td> 
                    <td class="fer"> 
                          <input id="no_atm" type="text" name="no_atm" class="form-control" >
                    </td>
                </tr>

                <tr class="ift poe" style="display:none;">

                </tr> 



                <tr class="ift">
                  <td colspan="3">
                    <button id="save_transaksi" class="btn btn-danger" type="button">
                    <div id="totaly"></div>
                    <input id="tolr_tt"  name="total" type="hidden"> 
                    <input id="bayar_tt"  name="bayar" type="hidden">  
                    <input type="hidden" id="id_usery">
                    <input type="hidden" id="id_oerder">
                    <input type="hidden" id="status_o">
                    
                    <input type="hidden" id="jenis_o">

                    Simpan</button> 
                    <button style="display:none;" data-toggle="modal" data-target="#myScan"   id="scan" class="btn btn-danger" type="button">Scan Promo</button>

                    <button style="display:none;" id="print" class="btn btn-danger" type="button">Print</button> 
                     </td>
                </tr> 
              </table>


     </div>

</div>


<div class="col-md-5">
    <div class="mne">
        <div class="ngitung"></div>
    </div>
    <div id="scan-qr"  class="qrcodeid"></div> 
    <div class="mne">
        <div class="">
            <button id="j_transaksi" style="display:none;" type="button" class="btn btn-danger btn-yrc">           
                  <div class="drc"> Lanjut </div>
            </button>
            
            <button id="new_order" style="display:none;" type="button" class="btn btn-danger btn-yrc">           
                  <i class="fa fa-plus  fa-2x" aria-hidden="true"></i><div class="drc"> Order Baru </div>
            </button>

        </div>   
    </div> 

     

</div>

</div>
<!--end cash-->
<div id="debit" style="display:none">

</div>





