<div id="categori" class="ats">
  <div style="" class="pull-left bcv">
    <div class="blk-category btn-default">
      <i class="fa fa-undo" aria-hidden="true"></i>
    </div>
  </div>

<div class="nama-category">
  <h4>{!! $category !!}</h4>
</div>

<div class="pull-right cac">
    <div class="box-tools">

      <div id="divsearch-catalog" class="input-group input-group-sm" style="width: 150px;">
        <input id="search-catalog" name="cari" class="form-control pull-right" placeholder="Search" type="text">
        <input  id="category_id" value="{!! $id !!}" type="hidden">

        <div class="input-group-btn">
          <button id="btn-katalog" type="submit" class="btn btn-danger"><i class="fa fa-search"></i></button>
        </div>
      </div>

  </div>
</div>

</div> 


<div id="mn-category" class="box-form form-group col-sm-12">
  <div class="divcategori">
  <ul class="category ul-fixed yk" >
    @if(empty($kategori))
            <li >
                <div class="center">
                  <a  href="#" >
                    <img src="{!! url('img/no-images.jpg') !!}" width="70" >
                    <h4>Category Not found</h4>
                  </a>
                </div>
              </li>
    @else 
            @foreach($kategori as $category)
              <li>
                <div class="center">
                  <a onClick="GetListCatalog({!! $category->id !!});"  href="#" >
                    <img src="{!! url($category->photo) !!}" width="70" >
                    <h4>{!! $category->name !!}</h4>
                  </a>
                </div>
              </li>
            @endforeach

    @endif   
</ul>



  </div>
  <div style="" class="divcatalog">
    <table class="table table-responsive table-fixed">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
          </tr>
        </thead>
        <tbody id="catalog" >
            @include('kasir.catalog.catalog')               
        </tbody>
    </table>  
  </div>
</div>
<script src="{{ asset('/js/script-search.js') }}"></script>