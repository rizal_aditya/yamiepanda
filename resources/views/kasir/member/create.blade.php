    <div class="modal-header">
       <h4 id="tl-mm" class="pull-left modal-title">Tambah Data Costumer</h4>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>  

    <div class="modal-body">
      <div class="box-body">


    <div id="insert-member" > 
          <!-- Name Field -->
          <div class="form-group col-sm-12">
              {!! Form::label('name', 'Name:') !!}
              {!! Form::text('name', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group col-sm-12">
              {!! Form::label('phone', 'Phone:') !!}
              {!! Form::text('phone', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group col-sm-12">
              {!! Form::label('address', 'Address:') !!}
              {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
          </div>

    </div>         


       </div>  
            </div>
              </div>  
            </div>


            <div class="modal-footer">
               <input  value="Back" id="btn-back-member" type="button" class="pull-left btn btn-danger">
              <button id="savenonmember" type="button" class="pull-right btn btn-danger"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Save </button>
          </div>

           <script src="{{ asset('/js/script-search.js') }}"></script>    
          