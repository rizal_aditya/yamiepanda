    <div class="modal-header">
       <h4 id="tl-mm" class="pull-left modal-title">Edit Data Costumer</h4>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>  

    <div class="modal-body">
      <div class="box-body">


    <div id="insert-member" > 
          <!-- Name Field -->
          @foreach($member as $member)
          <div class="form-group col-sm-12">
              {!! Form::label('name', 'Name:') !!}
               <input id="id" type="hidden" class="form-control" value="{!! $member->id !!}" >
              <input id="name" type="text" class="form-control" value="{!! $member->name !!}" >
          </div>

          <div class="form-group col-sm-12">
              {!! Form::label('phone', 'Phone:') !!}
             <input id="phone" type="text" class="form-control" value="{!! $member->phone !!}" >
          </div>

          <div class="form-group col-sm-12">
              {!! Form::label('address', 'Address:') !!}
              <textarea id="address" class="form-control" >{!! $member->address !!}</textarea>
          </div>
          @endforeach
    </div>         


       </div>  
            </div>
              </div>  
            </div>


            <div class="modal-footer">
               <input  value="Back" id="btn-back-member" type="button" class="pull-left btn btn-danger">
              <button id="updateMember" type="button" class="pull-right btn btn-danger"> <i class="fa fa-floppy-o" aria-hidden="true"></i> Update </button>
          </div>

           <script src="{{ asset('/js/script-search.js') }}"></script>    
          