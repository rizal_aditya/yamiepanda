<thead>
  <tr>
    <th style="text-align:center;">Qty</th>
    <th>Item</th>
    <th>Price</th>
    <th>Discount</th>
    <th>Total</th>
    <th>Action</th>
  </tr>
</thead>

 @if($order->isEmpty())
  <tr><td colspan="6">Order Not Found</td></tr>
@else  
  @include('kasir.table.listorder')

<tbody>
   @include('kasir.table.total')
</tbody>
@endif
 


