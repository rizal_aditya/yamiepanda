<table class="table table-responsive" id="order-table">
    <thead>
        <th>Branch Id</th>
        <th>User Id</th>
        <th>Costumer Id</th>
        <th>Post Id</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($order as $order)
               
        <tr>
            <td>{!! $order->branch_id !!}</td>
            <td>{!! $order->user_id !!}</td>
            <td>{!! $order->costumer_id !!}</td>
            <td>{!! $order->post_id !!}</td>
            <td>{!! $order->status !!}</td>
            <td>
                {!! Form::open(['route' => ['order.destroy', $order->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('order.show', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('order.edit', [$order->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>