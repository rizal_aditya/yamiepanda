<table class="table table-responsive" id="modal-table">
    <thead>
        <th>Nominal</th>
        <th>Branch Id</th>
        <th>User Id</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($modal as $modal)
        <tr>
            <td>{!! $modal->nominal !!}</td>
            <td>{!! $modal->branch_id !!}</td>
            <td>{!! $modal->user_id !!}</td>
            <td>{!! $modal->status !!}</td>
            <td>
                {!! Form::open(['route' => ['modal.destroy', $modal->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modal.show', [$modal->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('modal.edit', [$modal->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>