<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{!! config('app.title') !!}</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

   <!-- Bootstrap 3.3.6 -->
   <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
   <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
   <link rel="stylesheet"  href="{{ asset('/css/blue.css') }}">
    <script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.autocomplete.min.js') }}"></script>

     
   <!-- <link rel="stylesheet" href="{{ asset('/css/select2.min.css') }}"> -->
  
    
  

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body id="body" class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('/home') }}">{!! config('app.title') !!}</a>
    </div>

    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <div id="searchfield">
        
        <input id="full" type="hidden" value="1">

       <form id="searchform" method="post" action="{{ url('/login') }}">
            {!! csrf_field() !!}
          
              
                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input  type="email"  class="form-control" name="email" value="" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                     <!-- <div id="suggestions"></div> -->
                  

                    @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                 <div class="form-group has-feedback">
                   <select name="branch_id" class="form-control selectpicker">
                       <option value="">Pilih Cabang</option>
                       @foreach(App\Models\Branch::get() as $row)
                        <option value="{!! $row->id !!}"> {!! $row->name !!}</option>

                       @endforeach
                   </select>

                   @if ($errors->has('branch_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('branch_id') }}</strong>
                        </span>
                    @endif
                </div>

             
                </div>

                   
           

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                   <!--  <input id="fullid" type="button" value="click to toggle fullscreen" > -->
                    <button type="submit" class="btn btn-danger btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

  </div><!-- @end #searchfield -->


        <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
   

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->



<script  src="{{ asset('/js/jquery.autocomplete.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script  src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('/js/icheck.min.js') }}"></script>
<script>


$(document).ready(function() {
	
 
	
	
	
var element = document.getElementById("body");

  $('#fullid').on({
  click: function(e) {
    requestFullScreen(element);
  },
  mouseenter: function(e) {
    console.log('enter!');
  }
    });


   
}); 





function isFullScreen()
{
    return (document.fullScreenElement && document.fullScreenElement !== null)
         || document.mozFullScreen
         || document.webkitIsFullScreen;
}


function requestFullScreen(element)
{
    if (element.requestFullscreen)
        element.requestFullscreen();
    else if (element.msRequestFullscreen)
        element.msRequestFullscreen();
    else if (element.mozRequestFullScreen)
        element.mozRequestFullScreen();
    else if (element.webkitRequestFullscreen)
        element.webkitRequestFullscreen();
}

function exitFullScreen()
{
    if (document.exitFullscreen)
        document.exitFullscreen();
    else if (document.msExitFullscreen)
        document.msExitFullscreen();
    else if (document.mozCancelFullScreen)
        document.mozCancelFullScreen();
    else if (document.webkitExitFullscreen)
        document.webkitExitFullscreen();
}

function toggleFullScreen(element)
{
    if (isFullScreen())
        exitFullScreen();
    else
        requestFullScreen(element || document.documentElement);
}



    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });


function toggleFullScreen(elem) {
    

    // // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    // if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    // } else {
    //     if (document.cancelFullScreen) {
    //         document.cancelFullScreen();
    //     } else if (document.mozCancelFullScreen) {
    //         document.mozCancelFullScreen();
    //     } else if (document.webkitCancelFullScreen) {
    //         document.webkitCancelFullScreen();
    //     } else if (document.msExitFullscreen) {
    //         document.msExitFullscreen();
    //     }
    // }
}

</script>

</body>
</html>
