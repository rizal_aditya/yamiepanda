<table class="table table-responsive" id="pointUser-table">
    <thead>
        <th>User Id</th>
        <th>Transaction Id</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($pointUser as $pointUser)
        <tr>
            <td>{!! $pointUser->user_id !!}</td>
            <td>{!! $pointUser->transaction_id !!}</td>
            <td>{!! $pointUser->status !!}</td>
            <td>
                {!! Form::open(['route' => ['pointUser.destroy', $pointUser->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('pointUser.show', [$pointUser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('pointUser.edit', [$pointUser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>