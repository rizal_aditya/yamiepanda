@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">User
            <small>{{ $type }}</small>
        </h1>
    </section>
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('user.show_fields')
                    <a href="{!! url('user?type='.$type) !!}" class="btn btn-default">Back</a>

                    @role('user')

                     <a href="{!! url('user/'.$user->id.'/edit?type='.$type) !!}" class="btn btn-primary">Edit</a>
                    @endrole
                </div>
            </div>
        </div>
    </div>
@endsection
