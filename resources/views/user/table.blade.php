
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="dynamic-table" >

    <thead>
        <th>Photo</th>
        @role('admin')
        <th>Corporate</th>
        @endrole
        <th>Nama</th>
        <th>Email</th>
        <th>Alamat</th>
        <th>Type</th>
        <th>Status</th>
        <th colspan="3">Aksi</th>
    </thead>
    <tbody>
       
     @foreach($user as $user)
        
        <tr>
            <td><img width="50" height="50" src="{!! $user->photo == '' ? config('app.photo_default') : url($user->photo)  !!} "></td>
             @role('admin')
             <td>{!! $user->client->name !!}</td> 
             @endrole
             <td>{!! $user->name !!}</td>
             <td>{!! $user->email !!}</td>
             <td>{!! $user->address !!}</td>
             <td>
            @if($user->access=="premium")
                <a onclick="return confirm('Are you sure?');"  href="{!! url('user/'.$user->id.'/premium?access=standar'.'&type='.$type) !!}">Premium</a>
             @else
                <a onclick="return confirm('Are you sure?');"  href="{!! url('user/'.$user->id.'/standar?access=premium'.'&type='.$type) !!}">Standar</a>
             @endif

                </td>
             <td> 
            @if($user->status>0)
                <a onclick="return confirm('Are you sure?');" href="{!! url('user/'.$user->id.'/disable?status=0'.'&type='.$type) !!}">Aktif</a>
             @else
                <a onclick="return confirm('Are you sure?');"  href="{!! url('user/'.$user->id.'/enable?status=1'.'&type='.$type) !!}">Non Aktif</a>
             @endif

             </td>
             @role('user')
             <td>
              
                {!! Form::open(['route' => ['user.destroy', $user->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('user.show', [$user->id.'?type='.$type]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                   @if($type != "client")
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                   @endif 
                </div>
                {!! Form::close() !!}
             </td>
             @endrole

              @role('admin')
             <td>
               {!! Form::open(['route' => ['user.destroy', $user->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>

                    <a href="{!! route('user.show', [$user->id.'?type='.$type]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    @if($type <> 'parent')
                     <a href="{!! url('user/'.$user->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    @endif
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
             </td>
             @endrole


        </tr>
    @endforeach
    </tbody>
</table>
</div>



