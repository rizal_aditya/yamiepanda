@role('admin')
<div class="form-group col-sm-12">
   {!! Form::label('client_id', 'Corporate:') !!}
   {{ Form::select('client_id', App\Models\Client::get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}
</div>
@endrole

@role('user')
     {!! Form::hidden('client_id', Auth::user()->client_id, ['class' => 'form-control', "placeholder" => "Name"]) !!}
@endrole
<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', "placeholder" => "Name"]) !!}
    {!! Form::hidden('type', $type, ['class' => 'form-control']) !!} 
</div>

<div style="display:none;">
{!! Form::select('role_id', App\Role::where('name','user')->get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'style'=>'display:none;']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control',"placeholder" => "Email"]) !!}
</div>

@if(!empty($user->password))
<div class="form-group col-sm-12">
      {!! Form::label('password', 'Password:') !!}
      <input class="form-control" placeholder="Password" name="password" id="password" type="password">
    @if(Route::currentRouteName() == 'user.edit')
        <div class="alert alert-info">
          <span class="fa fa-info-circle"></span> Leave the password field blank if you wish to keep it the same.
        </div>
    @endif
</div>
@else
<div class="form-group col-sm-12">
{!! Form::label('password', 'Password:') !!}
{{ Form::password('password', array('class' => 'form-control','placeholder'=>'Password')) }}

</div>
@endif

@if($type != 'client')
<div class="form-group col-sm-12">
    {!! Form::label('birthday', 'Birthday:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control date','data-date-format' => 'yyyy-mm-dd','placeholder' => 'Birthday']) !!}   
</div>
<div class="form-group col-sm-12">
    {!! Form::label('religion', 'Religon:') !!}
    {!! Form::select('religion', ['Islam' => 'Islam', 'Nasrani' => 'Nasrani', 'Buddha' => 'Buddha', 'Hindu' => 'Hindu', 'Konghucu' => 'Konghucu', 'Other' => 'Other'], null, ['class' => 'form-control selectpicker']) !!}
</div>
@endrole

<div class="form-group col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control','placeholder' => 'Phone']) !!}
</div>
<div class="form-group col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($user) ? $user->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($user) ? $user->status == 0 : true) }} No
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('person/?type='.$type) !!}" class="btn btn-default">Cancel</a>
</div>