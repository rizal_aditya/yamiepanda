<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{!! config('app.title') !!}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="csrf-token" content="{{ Session::token() }}"> 
   
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/font-awesome/4.5.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/font-awesome.css') }}" media="screen">
    <link rel="stylesheet" href="{{ asset('/ionicons/2.0.1/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('/plugins/timepicker/bootstrap-timepicker.css') }}">
    <link href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/colorbox.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    
     <!-- jQuery 2.1.4 -->
    <script src="{{ asset('/js/jQuery-2.2.0.min.js') }}"></script>

    
</head>

<body class="skin-blue sidebar-mini sidebar-collapse">
@if (!Auth::guest())
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">

            <!-- Logo -->
            <a href="{{ url('home') }}" class="logo">
                
                <span class="logo-mini"><img width="40" height="40" src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}" class="img-circle" alt="User Image"/></span>
                <span class="logo-lg">{!! config('app.title') !!}</span>
            </a>



            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                               <!--  <img src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}"
                                     class="user-image"  alt="User Image"/> -->
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs">
                                      @if (Auth::guest())
                                        {!! config('app.title') !!}
                                    @else
                                    {!! Auth::user()->name !!}
                                    @endif
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="{!! Auth::user()->photo == '' ? config('app.photo_default') : url(Auth::user()->photo)  !!}" class="img-circle" alt="User Image"/>
                                    <p>
                                        @if (Auth::guest())
                                            {!! config('app.title') !!}
                                        @else
                                            {!! Auth::user()->name !!}
                                        @endif
                                        <small>Member since {!! Auth::user()->created_at->format('M. Y') !!}</small>

                                            <?php $id = Auth::user()->id;?>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{!! url('user/'.$id.'?type=client') !!}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <!-- Main Footer -->
        <footer class="main-footer" style="max-height: 100px;text-align: center">
            <strong>Copyright © {!! config('app.copyright_date') !!} <a href="{!! config('app.company_link') !!}" target="_blank">{!! config('app.company_name') !!}</a>.</strong> All rights reserved.
        </footer>

    </div>
@else
   <script>
       
var url = "login";
$(location).prop('href', url);

   </script>
    @endif
   
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/js/icheck.min.js') }}"></script>
    <script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/js/app.min.js') }}"></script>
	
    
 
    <!-- Datatables -->
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap.min.js') }}"></script>
    <script  src="{{ asset('/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/js/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
 
  
    <script type="text/javascript">
        $('.date').datepicker({
          autoclose: true,
           format: 'yyyy-mm-dd'
        });
        // $("textarea").wysihtml5({
        //     toolbar: {
        //         "link": false,
        //         "image": false
        //     }
        // });

// $(document).ready(function () {
//   $('.fixed-table-body').slimScroll({});
// });



        
    </script>

        <script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
  });


</script>

<script>
//fullscrean
function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}
</script>


<script type="text/javascript">
$(function () {   
    //Timepicker
    $(".timepicker").timepicker({
        showMeridian:false,
      showInputs: false
    });
   
  });
</script>

    @yield('scripts')
</body>
</html>