


@role('admin')
		 <li class="treeview">
	          <a href="home">
	            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
	          </a>
          
          </li>	

		@foreach (App\Models\Menu::where('role_id', App\Role::where('name', 'admin')->first()->id)->orderBy('position','asc')->get() as $menu) 

			@if($menu->parent ==0)
        	<li class="treeview">
	          
        		@if($menu->url =="#")
		           <a href="{{ url($menu->url) }}">
			            <i class="fa {{ $menu->icon }}"></i> 
			            <span>{{ $menu->name }}</span> <i class="fa fa-angle-left pull-right"></i>
		           </a>
		           <ul class="treeview-menu">

		          	@foreach (App\Models\Menu::where('parent', $menu->id)->orderBy('position','asc')->get() as $menus) 
		            <li><a href="{{ url($menus->url) }}"><i class="fa {{ $menus->icon }}"></i>{{ $menus->name }}</a></li>  
					@endforeach
					
		          </ul>
	           	@endif	
	           
		          
        	</li>	

        	@endif




		@endforeach

@endrole


@role('petugas')
		 <li class="treeview">
	          <a href="home">
	            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
	          </a>
          
          </li>	

		@foreach (App\Models\Menu::where('role_id', App\Role::where('name', 'petugas')->first()->id)->orderBy('position','asc')->get() as $menu) 

			@if($menu->parent ==0)
        	<li class="treeview">
	          
        		@if($menu->url =="#")
		           <a href="{{ url($menu->url) }}">
			            <i class="fa {{ $menu->icon }}"></i> 
			            <span>{{ $menu->name }}</span> <i class="fa fa-angle-left pull-right"></i>
		           </a>
		           <ul class="treeview-menu">

		          	@foreach (App\Models\Menu::where('parent', $menu->id)->orderBy('position','asc')->get() as $menus) 
		            <li><a href="{{ url($menus->url) }}"><i class="fa {{ $menus->icon }}"></i>{{ $menus->name }}</a></li>  
					@endforeach
					
		          </ul>
	           	@endif	
	           
		          
        	</li>	

        	@endif




		@endforeach

@endrole



