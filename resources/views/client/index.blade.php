@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Akses</h1>
        <h1 class="pull-right">
           <a class="btn btn-danger pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('client.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-danger">
            <div class="box-body">

               <div class="box-header">
  <h3 class="box-title">Data Akses</h3>


  <form class="navbar-form navbar-right" method="GET" >
  <div class="box-tools">
    <div class="input-group input-group-sm" style="width: 150px;">
      <input type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      
      <div class="input-group-btn">
        <button  type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
      </div>
    </div>
  </div>
  </form>

</div>              
     
              
                @if($client->isEmpty())
                    <div class="text-center">No Artis found.</div>
                @else
                     @include('client.table')

                {{ $client->links() }}
                
                @endif
                 
            </div>
        </div>
    </div>
@endsection

