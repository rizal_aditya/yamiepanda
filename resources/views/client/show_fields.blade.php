<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $client->id !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $client->type !!}</p>
</div>

<!-- Access Field -->
<div class="form-group">
    {!! Form::label('access', 'Access:') !!}
    <p>{!! $client->access !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $client->name !!}</p>
</div>

<!-- Owner Field -->
<div class="form-group">
    {!! Form::label('owner', 'Owner:') !!}
    <p>{!! $client->owner !!}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{!! $client->photo !!}</p>
</div>

<!-- Reg Number Field -->
<div class="form-group">
    {!! Form::label('reg_number', 'Reg Number:') !!}
    <p>{!! $client->reg_number !!}</p>
</div>

<!-- Phohe Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phohe:') !!}
    <p>{!! $client->phone !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $client->address !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $client->created_at->format('d F Y') !!}</p>
</div>


