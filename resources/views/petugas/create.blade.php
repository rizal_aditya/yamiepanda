@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Petugas
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'petugas.store','enctype'=>'multipart/form-data']) !!}

                        @include('petugas.fields_create')

                    
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


