@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Petugas Edit
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($petugas, ['route' => ['petugas.update', $petugas->id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('petugas.fields_edit')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection