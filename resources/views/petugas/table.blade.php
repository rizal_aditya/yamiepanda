<table class="table table-responsive" id="penjualan-table">
    <thead>
        <th>Photo</th>
        <th>Nama Lengkap</th>
        <th>Email</th>
        <th>Telp</th>
        <th>Cabang</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($petugas as $row)
        <?php $cabang = DB::table('detail_petugas as a')->join('branch as b', 'a.branch_id', '=', 'b.id')->select('b.name')->where('a.user_id',$row->id)->get();?>

   
        <tr>
          
             <td><img width="50" height="50" src="{!! $row->photo !!}"></td> 
               
            <td>{!! $row->name !!}</td> 
            <td>{!! $row->email !!}</td> 
            <td>{!! $row->phone !!}</td> 
            

            <td>@foreach($cabang as $cabang) {!! $cabang->name !!}  @endforeach</td> 
            <td>@if($row->status =="0" ) Tidak Aktif @else Aktif @endif</td>
            <td>
                     {!! Form::open(['route' => ['petugas.destroy', $row->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('petugas.show', [$row->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('petugas.edit', [$row->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>     
        </tr>        
    @endforeach
    </tbody>
</table>