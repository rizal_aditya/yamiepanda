<!-- Nama -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama Lengkap:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


@if($jml > 1)
 
  <div class="form-group col-sm-6">
    {!! Form::label('cabang', 'Cabang:') !!}
    <select name="cabang[]" id="cabang" class="selectpicker form-control">
      <option value="0">Pilih Cabang</option>
    @foreach($cabang1 as $cabang)
            @if($cabang_id[0]->branch_id == $cabang->id)
                  <option value="{!! $cabang->id !!}" selected>{!! $cabang->name !!}</option>
              @else
                    <option value="{!! $cabang->id !!}" >{!! $cabang->name !!}</option>
              @endif
    @endforeach
    </select>
</div>


  <div class="form-group col-sm-6">
    {!! Form::label('cabang', 'Cabang:') !!}
    <select name="cabang[]" id="cabang" class="selectpicker form-control">
      <option value="0">Pilih Cabang</option>
     @foreach($cabang2 as $row)
           
             @if($cabang_id[1]->branch_id == $row->id)
                  <option value="{!! $row->id !!}" selected>{!! $row->name !!}</option>
              @else
                    <option value="{!! $row->id !!}" >{!! $row->name !!}</option>
              @endif
    @endforeach
    </select>
</div>




@else

<div class="form-group col-sm-6">
    {!! Form::label('cabang', 'Cabang:') !!}
    <select name="cabang[]" id="cabang" class="selectpicker form-control">
      <option value="0">Pilih Cabang</option>
    @foreach($cabang as $cabang)
            @if($petugas->cabang_id == $cabang->id)
                  <option value="{!! $cabang->id !!}" selected>{!! $cabang->name !!}</option>
              @else
                    <option value="{!! $cabang->id !!}" >{!! $cabang->name !!}</option>
              @endif
    @endforeach
    </select>
</div>

@endif




















<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control',"placeholder" => "Email"]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telp:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control',"placeholder" => "Telp"]) !!}
</div>



<div class="form-group col-sm-6">
    {!! Form::label('religion', 'Agama:') !!}
    <select id="religion" name="religion" class="selectpicker form-control">
      <option value="Other">Pilih Agama</option>
        @if($petugas->religion !="")
            <option value="{!! $petugas->religion !!}" selected="">{!! $petugas->religion !!}</option>
        @else
            <option value="Islam">Islam</option>
      <option value="Nasrani">Nasrani</option>
      <option value="Katolik">Katolik</option>
      <option value="Budha">Budha</option>
      <option value="Hindu">Hindu</option>
      <option value="Konghucu">Konghucu</option>
       <option value="Other">Lain - lain</option>
        @endif

      
    </select>
    
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Tanggal Lahir:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control date']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Alamat:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>




<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::file('photo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
  @if($petugas->photo !="")
  <img width="50" height="50" src="{!! url($petugas->photo) !!}">
  @endif
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($petugas) ? $petugas->status == 1 : false) }} Aktif<br>
    {{ Form::radio('status', 0, isset($petugas) ? $petugas->status == 0 : true) }} Tidak Aktif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('petugas.index') !!}" class="btn btn-default">Cancel</a>
</div>
