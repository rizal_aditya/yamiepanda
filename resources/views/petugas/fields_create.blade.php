<!-- Nama -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nama Lengkap:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('cabang', 'Cabang:') !!}
    <select name="cabang[]" id="cabang" class=" form-control">
      <option value="0">Pilih Cabang</option>
    @foreach($cabang as $cabang)
          
          <option value="{!! $cabang->id !!}" >{!! $cabang->name !!}</option>
             
    @endforeach
    </select> <input id='tmbh' value='+' class='btn btn-danger' type='button'>
</div>

<div id="cbng"></div>



<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control',"placeholder" => "Email"]) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Telp:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control',"placeholder" => "Telp"]) !!}
</div>



<div class="form-group col-sm-6">
    {!! Form::label('religion', 'Agama:') !!}
    <select id="religion" name="religion" class=" form-control">
      <option value="Other">Pilih Agama</option>
      <option value="Islam">Islam</option>
      <option value="Nasrani">Nasrani</option>
      <option value="Katolik">Katolik</option>
      <option value="Budha">Budha</option>
      <option value="Hindu">Hindu</option>
      <option value="Konghucu">Konghucu</option>
       <option value="Other">Lain - lain</option>
    </select>
    
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('birthday', 'Tanggal Lahir:') !!}
    {!! Form::text('birthday', null, ['class' => 'form-control date']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Alamat:') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group col-sm-6">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>


<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, '') }} Aktif<br>
    {{ Form::radio('status', 0, '')  }} Tidak Aktif
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
    <a href="{!! route('petugas.index') !!}" class="btn btn-default">Cancel</a>
</div>


<script type="text/javascript">

$(document).ready(function() {

    //getcabang();


    var x = 1; //initlal text box count
    var max_fields = 100;
    $("#tmbh").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
         
            getcabang(x);

        }
    });


    




});

function getcabang(x){
  
  $.ajax({
  type:"GET",  
    url:"cabang",
    dataType: "json",
    cache: false,
    success: function(respons){
         jmlData = respons.length;
        var slct = "";
            slct +='<div id="sde'+x+'" class="form-group col-sm-6">';
              slct +="<label class='form-group' for='cabang'>cabang:</label>";
            slct +="<select id='cabang' name='cabang[]' class=' form-control'>"; 
                  slct +="<option value='0'>Pilih Cabang</option>"; 
              for(a = 0; a < jmlData; a++)
              {
                  slct +="<option value='" + respons[a]["id"] + "'>"+ respons[a]["name"] +"</option>";

               } 

            slct += "</select><a href='#' onclick='getdelete("+x+");'>Hapus</a></div> ";
              slct +='</div>';
            
       
            $("#cbng").append(slct);
      },
    error: function (respons) {
      //alert("Gagal Get katalog");
        
      }
  });



}

function getdelete(x){

  var del = "";

  $("#sde"+x+"").remove(del);

}






</script>


