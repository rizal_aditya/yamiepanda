<form method="post" action="{{ url('/register/check') }}">
   <div class="box-footer ">
  
  

 <div class="form-group has-feedback {{ $errors->has('client_id') ? ' has-error' : '' }}"> 
    
    <div style="width:586px;float:left;">
        <select name="client_id" class="form-control selectpicker" data-live-search="true">
        <option value="">Select Client</option>
            @foreach(App\User::where('type','client')->get() as $rows)
            <option value="{!! $rows->client_id !!}">{!! $rows->name !!}</option>
            @endforeach
        </select> 
    </div>    
      <span style="float:left;"  class="input-group-btn">
        <button type="submit" class="btn btn-primary">Search</button>
      </span>
    </div>


   

    </div>
</form>
