<section class="row section text-light" style="background-color:#4fcead;background-image: url('{{ url('img/loop.png') }}');">
	<div class="row-content buffer even clear-after">	
		
		@foreach(App\Models\Post::where('type','fitur-screnshoot')->orderBy('id', 'desc')->get() as $src)
		<div class="column six">
			<h2>{{ $src->title }}</h2>
			<p>{!! $src->content !!}</p>
		</div>
		<div class="side-mockup right-mockup animation">
			<div class="slider desktop-slider white-controls" data-autoplay="3000">
				<figure>
					@foreach(App\Models\Asset::where('post_id', $src->id)->get() as $gmb)
					<div><img src="{{ url($gmb->file) }}" alt=""></div>
					 @endforeach
				</figure>
			</div>
		</div>
		@endforeach
	</div>	
</section>	

<section class="row section call-to-action">
	<div class="row-content buffer even animation">
		<p>Sudahkah Sekolah Anda Menggunakan Aplikasi Tutwuri ??</p>
		<a class="button red" href="register.html">Daftar Sekarang Juga!</a>
	</div>
</section>	