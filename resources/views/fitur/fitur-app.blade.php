<section class="row section text-light" style="background-color: #FC6E51;background-image: url('{{ url('img/loop3.png') }}');">
	<div class="row-content buffer even clear-after">
		 @foreach(App\Models\Post::where('type','fitur')->orderBy('id', 'asc')->limit(15)->get() as $value=>$ft)
		   <?php 
            $data = $ft->content;
            $content1 =  str_replace('<p>','', $data);
            $content2 =  str_replace('</p>','', $content1);
            $content3 =  str_replace('<br>','', $content2);
            $content4 =  str_replace('</br>','', $content3);
            ?>

        @if($value == 0)  
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 1)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 2)
		<div class="column four last centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
				
			</div>
		</div>
		@elseif($value == 3)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 4)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 5)
		<div class="column four last centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
				
			</div>
		</div>
		@elseif($value == 6)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 7)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 8)
		<div class="column four last centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
				
			</div>
		</div>
		@elseif($value == 9)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 10)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 11)
		<div class="column four last centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
				
			</div>
		</div>
		@elseif($value == 12)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 13)
		<div class="column four centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
			</div>
		</div>
		@elseif($value == 14)
		<div class="column four last centertxt">
			<div class="big-icon red"><i class="{!! $ft->bg !!}"></i></div>
			<div class="big-icon-text clear-after">
				<h4>{{ $ft->title }}</h4>
				<p class="text-s">{{ $content3 }}</p>
				
			</div>
		</div>
		@endif
		@endforeach
	</div>
</section>	