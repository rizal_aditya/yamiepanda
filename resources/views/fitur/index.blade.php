   @include('frontend.header')
        @include('frontend.page')
        <main role="main">
             @include('frontend.slider')
            <div id="main">
                <section class="row section call-to-action">
					<div class="row-content buffer even">
						<p>Beragam Fitur yang Lengkap, Memudahkan, dan Inovatif, Tunggu Apa lagi ?</p>
						<a class="button aqua" href="#">Get it now!</a>
					</div>
				</section>
                @include('fitur.fitur-app')
             	@include('fitur.screnshoot-fitur')
                            
            </div><!-- id-main -->
        </main><!-- main -->
 @include('frontend.footer')
