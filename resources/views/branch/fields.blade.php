<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>




<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('description', 'Alamat:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>



<style type="text/css">
#areamap {
    float: left;
    width: 100%;
}
.rowe {
    border-radius:3px;
    border: 1px solid #EAEAEA;
    background: #EAEAEA;
}
.center h2 {
   
    margin-top: 35px;
   
}
#areamap .koordinat {
    float: left;
    width: 100%;
    color: #FFF;
    font-weight: bold;
    background: none repeat scroll 0% 0% #313131;
    text-shadow: none;
}
#areamap .koordinat .pade {
    float: left;
    padding: 10px;
    width: 100%;
}

#areamap .koordinat {
    float: left;
    width: 100%;
    color: #FFF;
    font-weight: bold;
    background: none repeat scroll 0% 0% #313131;
    text-shadow: none;
}
#areamap .koordinat .pade .kiri {
width: 100%;
height: auto;
float: left;
margin: 0px 0px 14px;
}
#areamap .koordinat .pade .kananew {
   width: 100%;
    height: auto;
    float: left;
}


  </style>

<div class="form-group col-sm-12">

<div id="areamap">


<div class="koordinat">
    <div class="pade">
    Ancer - ancer / Denah Lokasi (Geser <strong>PETA</strong> atau <strong>TANDA PANAH MERAH</strong> ke lokasi yang tepat.<br>
    Perbesar peta (zoom) untuk tampilan peta yang lebih detail.)
    </div>
</div>

<div id="map_canvas" style="width:100%; height: 400px; position: relative; background-color: #E5E3DF; overflow: hidden;"></div>

 <div class="koordinat" style="margin-bottom:30px;"></div>


   

</div>

</div>
                            
                        
<script>
$(document).ready(function() {
    myMap();
});
var map;
var arrMarker = [];
var myLatlng;

function myMap() {
  myLatlng = new google.maps.LatLng(-7.791838134699915, 110.37154953652339);
  var myOptions = {
    center: myLatlng,
    zoom: 12,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    
  }
   map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
    var styleOptions = {
            name: "Dummy Style"
        };
        
    var MAP_STYLE = [
        {
            featureType: "road",
            elementType: "all",
            stylers: [
                { visibility: "on" }
            ]
        }
    ];
     var mapType = new google.maps.StyledMapType(MAP_STYLE, styleOptions);

    map.mapTypes.set("Dummy Style", mapType);
    map.setMapTypeId("Dummy Style");  
    createPosition();
}
function createPosition(){
    marker1 = new google.maps.Marker({map : map,
                                        draggable: true,
                                        icon: '{!! asset('img/input.png') !!}',
                                        position: myLatlng});
    google.maps.event.addListener(marker1, 'dragend', function (event) {
    document.getElementById("latitude").value = this.getPosition().lat();
    document.getElementById("longitude").value = this.getPosition().lng();
    });

}


</script>


<script src="https://maps.googleapis.com/maps/api/js?sensor=false&language=id&key=AIzaSyDzC9NRcR-_gLQdcOGqAn4ayVtWWEXexzA&callback=myMap"></script>

<!-- Latitude Field -->
<div class="form-group col-sm-12">
    {!! Form::label('telp', 'Telp:') !!}
    {!! Form::text('telp', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-12">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-12">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>




<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($branch) ? $branch->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($branch) ? $branch->status == 0 : true) }} No
</div>






<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cabang.index') !!}" class="btn btn-default">Cancel</a>
</div>
