@extends('layouts.app')

@section('content')
    <section class="content-header">
     
        <h1>Edit Cabang</h1>
     


   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($branch, ['route' => ['cabang.update', $branch->id], 'method' => 'patch']) !!}

                        @include('branch.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection