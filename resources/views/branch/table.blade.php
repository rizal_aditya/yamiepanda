<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="branch-table">
    <thead>
         <th>Nama Cabang</th>
        <th>Alamat</th>
         <th>Telp</th>
      
        <th>Status</th>
        <th colspan="3">Aksi</th>
    </thead>
    <tbody>
         
     @foreach($branch as $branch)
 
        <tr>
          
            <td>{!! $branch->name !!}</td>
          
            <td>{!! $branch->description !!}</td>
             <td>{!! $branch->telp !!}</td>
                       <td>
             @if($branch->status>0)
               Aktif
             @else
                Non Aktif
             @endif
               

            </td>
            <td>

                {!! Form::open(['route' => ['cabang.destroy', $branch->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('cabang.show', [$branch->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('cabang.edit', [$branch->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                   
                  
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Apakah anda yakin?')"]) !!}
                  
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
