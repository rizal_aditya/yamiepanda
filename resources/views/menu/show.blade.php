@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Menu
            <small>{{ $type }}</small>
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('menu.show_fields')
                    <a href="{!! url('menu?type='.$type) !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
