
<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

@role('user')
<!-- Type Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('role_id', 'Type:') !!}
     {{ Form::select('role_id', App\Role::get()->lists('display_name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true'])}}
</div>
@endrole
  
{!! Form::hidden('role_id', $id, ['class' => 'form-control']) !!}


<!-- Icon Field -->
<div class="form-group col-sm-12">
    {!! Form::label('icon', 'Icon:') !!}
    {!! Form::text('icon', null, ['class' => 'form-control']) !!}
</div>



<!-- Type Id Field -->

<div class="form-group col-sm-12">
    {!! Form::label('parent', 'Parent:') !!}
    
     <select class="form-control selectpicker" name="parent">
        <option value="0">None</option>
                    @if(empty($menu->id))
                         @foreach(App\Models\Menu::where(['parent'=>'0','role_id' => $id])->get() as $pa)
                        
                            <option value="{{ $pa->id }}" selected>{{ $pa->name }}</option>
                         
                         @endforeach
                    @else
                         @foreach(App\Models\Menu::where(['parent'=>'0','role_id' => $id])->get() as $pa)
                        
                            <option value="{{ $pa->id }}" selected>{{ $pa->name }}</option>
                         
                         @endforeach
                    @endif
                
     </select>

</div>

<!-- Icon Field -->
<div class="form-group col-sm-12">
    {!! Form::label('position', 'Position:') !!}
    {!! Form::select('position', ['1' => '1','2' => '2', '3' => '3', '4' => '4','5' => '5','6' => '6','7' => '7', '8' => '8','9' => '9','10'=>'10'], null, ['class' => 'form-control selectpicker']) !!}
        
</div>


<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($menu) ? $menu->status == 1 : false) }} Yes<br>
    {{ Form::radio('status', 0, isset($menu) ? $menu->status == 0 : true) }} No
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! url('menu?type='.$type) !!}" class="btn btn-default">Cancel</a>
</div>
