@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">History Transaksi</h1>
        <h1 class="pull-right">
         
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

   <div class="clearfix"></div>

<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Produk Item</h3>

              
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-8 ngte2">
                  <p class="text-center">
                    <strong>List Item produk</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                       <table id="History"  width="500" height="128" class="table table-responsive" id="voucher-table"></table>

                       <ul class="pagination">
             <li class="first"></li>   
             <li class="navigation"></li>
             <li class="last"></li>
        </ul>

                    
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <p >
                    <strong>Pilih riwayat yang ingin dilihat :</strong>
                  </p>

                  <div id="pilihan" >
                   
                  </div>
                   
                
                  
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="description-block border-right">
                    
                    <button onclick="printhistory();" type="button" class="btn btn-default"><i class="fa fa-print"></i></button>
                  </div>
                    
                 
                </div>
                <!-- /.col -->
               
                
               
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>








   
     <!--    <div class="box box-primary">
            <div class="box-body">

                <div id="pilihan" class="vdcx"></div>   


 <div class="pull-left col-sm-8">

 <div class="derc pull-left col-sm-8">
       <div class="pull-left  col-sm-8"><h4 class="bgyo">Produk Item</h4></div>
        <table id="History" class="table table-responsive" id="voucher-table"></table>

        <ul class="pagination">
             <li class="first"></li>   
             <li class="navigation"></li>
             <li class="last"></li>
        </ul>

</div>

<div class="derc pull-left col-sm-8">
    <div class="pull-left  col-sm-8"><h4 class="bgyo">Transaksi Tunai</h4></div>
    <table id="debit" class="table table-responsive" id="voucher-table"></table>
 </div>


 </div>


 <div class="pull-right col-sm-8">

 <div class="derc pull-right col-sm-8">
    <div class="pull-left  col-sm-8"><h4 class="bgyo">Makanan Dan Minuman</h4></div>
    <table id="food" class="table table-responsive" id="voucher-table"></table>
 </div>

 <div class="derc pull-right col-sm-8">
    <div class="pull-left  col-sm-8"><h4 class="bgyo">Transaksi Debit</h4></div>
    <table id="debit" class="table table-responsive" id="voucher-table"></table>
 </div>

</div>


            </div>
        </div> -->
    </div>


<div class="box-body">

<div class="row">
            <div class="col-md-6">
              <!-- DIRECT CHAT -->
              <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Tunai</h3>  
                </div>
               
               <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Total</th>
                   
                  </tr>
                  <tbody id="Trtunai">
                      
                  </tbody>
                  </thead>
                  
                </table>
              </div>
             
              </div>
              <!--/.direct-chat -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Debit</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                     <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Bank</th>
                    <th>Total</th>
                   
                  </tr>
                  <tbody id="Trdebit">
                      
                  </tbody>
                  </thead>
                  
                </table>
                  
                </div>
                <!-- /.box-body -->
              
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>
            <!-- /.col -->
          </div>

</div>  

<div class="box-body">

<div class="row">
            <div class="col-md-6">
              <!-- DIRECT CHAT -->
              <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Makanan Dan Minuman</h3>  
                </div>
               
               <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Jenis</th>
                    <th>Total</th>
                   
                  </tr>
                  <tbody id="Trfood">
                      
                  </tbody>
                  </thead>
                  
                </table>
              </div>
             
              </div>
              <!--/.direct-chat -->
            </div>
            <!-- /.col -->

            <div class="col-md-6">
              <!-- USERS LIST -->
              <div class="box box-danger">
                <div class="box-header with-border">
                  <h3 class="box-title">Gratis</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                     <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Qty</th>
                    <th>Item</th>
                    <th>Total</th>
                   
                  </tr>
                  <tbody id="Trfree">
                      
                  </tbody>
                  </thead>
                  
                </table>
                  
                </div>
                <!-- /.box-body -->
              
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>
            <!-- /.col -->
          </div>

</div> 
     <script src="{{ asset('/js/script-history.js') }}"></script>
@endsection




