@extends('layouts.app')

@section('content')
    <section class="content-header">
   
        <h1 class="pull-left">Stock</h1>
   

        <h1 class="pull-right">
         
           <a data-toggle="modal" onclick="createstokmodal();" data-target="#createstok"  class="btn btn-danger pull-right" style="margin-top: -10px;margin-bottom: 5px" href="">Tambah</a>
        
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div id="alertstock"></div>

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
  <div class="box-header">
   

 

      <select onchange="gettablestock(this);" id="category" class="selectpicker"></select>
 


  <table id="stock" class="table table-responsive">
    <thead>
       <tr>
     <th>Kategori</th>
      <th>Katalog</th>
      <th>Harga</th>
      <th>Status</th>
      <th>Action</th>
      </tr>
    </thead>
    <tbody id="stokis">
      

    </tbody>

  </table>

    <ul class="pagination">
             <li class="first"></li>   
             <li class="navigation"></li>
             <li class="last"></li>
        </ul>

</div>              
     <script src="{{ asset('/js/script-stock.js') }}"></script>

               

            </div>
        </div>
    </div>


    <div id="createstok" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="false" >
      
  


    </div>
@endsection

  


