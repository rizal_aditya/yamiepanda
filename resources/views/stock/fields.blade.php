<!-- Name Field -->
<div class="form-group col-sm-12 col-lg-12">
{!! Form::label('category_id', 'Category:') !!}
 {{ Form::select('category_id', App\Models\Category::get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true','onchange'=>'getkatalog(this);']) }}   
</div>




<!-- Name Field -->
<div class="form-group col-sm-12">
    
    <div id="katalog"></div>
</div>





<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
     {{ Form::radio('status', 0, isset($stock) ? $stock->status == 0 : true) }} Stok Habis<br>
    {{ Form::radio('status', 1, isset($stock) ? $stock->status == 1 : false) }} publish
   
</div>



  <script src="{{ asset('/js/script-stock.js') }}"></script>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cabang.index') !!}" class="btn btn-default">Cancel</a>
</div>
