@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Category
        </h1>
    </section>
    <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($category, ['route' => ['category.update', $category->id], 'method' => 'patch']) !!}

                        @include('category.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
    </div>
@endsection