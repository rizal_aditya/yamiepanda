

@role('admin') 


    {!! Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']) !!}
 @endrole

@if($type == "news")
  @include('post.fields.news')
@elseif($type == "promo")
  @include('post.fields.promo')
@elseif($type == "katalog")
  @include('post.fields.katalog')
@elseif($type == "meja")
  @include('post.fields.meja')
@endif

