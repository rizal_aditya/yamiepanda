

<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Judul:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Deskripsi:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

{!! Form::hidden('type', 'news', ['class' => 'form-control']) !!}
{!! Form::hidden('category_id', '0', ['class' => 'form-control']) !!}

<div class="form-group col-sm-12">
    {!! Form::label('broadcast_type', 'Broadcast Type:') !!}<br/>
    {{ Form::radio('broadcast_type', 'all', isset($post) ? $post->broadcast_typedcast == 'all' : true,['class'=>'pilih1']) }} Semua<br>
    {{ Form::radio('broadcast_type', 'private', isset($post) ? $post->broadcast_type == 'private' : false,['class'=>'pilih2']) }} Pribadi<br>
</div>



<div id="user" style="display:none;" class="form-group col-sm-12">
    <select name="costumer" class="form-control selectpicker">
    @foreach($user as $user)
    <option value="{!! $user->id !!}">{!! $user->name !!}</option>
    @endforeach
    </select>
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Publish<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No Publish
</div>

<div class="form-group col-sm-12">
@if(!empty($post->photo))
    <img src="{!! empty($post->photo) ? config('app.photo_default') : url($post->photo)  !!}" width="100" height="100">
@endif  
</div>

<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
    <a href="{!! url('post?type='.$type) !!}" class="btn btn-default">Cancel</a>
</div>
