
<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Nama Produk:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('description', 'Nama Produk Lengkap:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>


<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Deskripsi:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
{!! Form::label('category_id', 'Kategori:') !!}
 {{ Form::select('category_id', App\Models\Category::get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}   
</div>

<div class="form-group col-sm-12 col-lg-12">
{!! Form::label('jenis', 'Jenis:') !!}
 {{ Form::select('jenis', array('food' => 'food', 'beverages' => 'beverages','other' => 'other') , null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}   
</div>

<!-- Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('price', 'Harga:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('discount', 'Diskon:') !!}
    {!! Form::text('discount', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group col-sm-12">
    {!! Form::label('point', 'Poin:') !!}
    {!! Form::text('point', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('start_date', 'Mulai:') !!}
    {!! Form::text('start_date', null, ['class' => 'form-control date','data-date-format' => 'yyyy-mm-dd'])  !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-12">
    {!! Form::label('end_date', 'Selesai:') !!}
    {!! Form::text('end_date', null, ['class' => 'form-control date','data-date-format' => 'yyyy-mm-dd']) !!}
</div>



{!! Form::hidden('type', 'promo', ['class' => 'form-control']) !!}

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('broadcast_type', 'Pilih type Sebarkan:') !!}<br/>
    {{ Form::radio('broadcast_type', 'premium', isset($post) ? $post->broadcast_type == 'premium' : false) }} Premium<br>
    {{ Form::radio('broadcast_type', 'standar', isset($post) ? $post->broadcast_type == 'standar' : true) }} Standar<br>
    {{ Form::radio('broadcast_type', 'semua', isset($post) ? $post->broadcast_type == 'semua' : true) }} Semua
</div>

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Publish<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No Publish
</div>

<!-- Status Field -->
<!-- <div class="form-group col-sm-12"> -->
<!--     {!! Form::label('shared_id', 'Share Sosmed:') !!}<br/>
    {{ Form::select('shared_id', App\Models\Shared::get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }} -->
<!-- </div> -->


<div class="form-group col-sm-12">
@if(!empty($post->photo))
    <img src="{!! empty($post->photo) ? config('app.photo_default') : url($post->photo)  !!}" width="100" height="100">
@endif  
</div>

<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Simpan', ['class' => 'btn btn-danger']) !!}
    <a href="{!! url('post?type='.$type) !!}" class="btn btn-default">Batal</a>
</div>
