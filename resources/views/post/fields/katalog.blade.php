
<!-- Title Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Nama Produk :') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('description', 'Nama Produk Lengkap:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Deskripsi:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
{!! Form::label('category_id', 'Kategori:') !!}
 {{ Form::select('category_id', App\Models\Category::get()->lists('name', 'id'), null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}   
</div>

<div class="form-group col-sm-12 col-lg-12">
{!! Form::label('jenis', 'Jenis:') !!}
 {{ Form::select('jenis', array('food' => 'food', 'beverages' => 'beverages','other' => 'other') , null, ['class' => 'form-control selectpicker', 'data-live-search'=>'true']) }}   
</div>

<!-- Price Field -->
<div class="form-group col-sm-12">
    {!! Form::label('price', 'Harga:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>





{!! Form::hidden('type', 'katalog', ['class' => 'form-control']) !!}
{!! Form::hidden('broadcast_type', 'semua', ['class' => 'form-control']) !!}

<!-- Status Field -->
<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}<br/>
    {{ Form::radio('status', 1, isset($post) ? $post->status == 1 : false) }} Publish<br>
    {{ Form::radio('status', 0, isset($post) ? $post->status == 0 : true) }} No Publish
</div>


<div class="form-group col-sm-12">
@if(!empty($post->photo))
    <img src="{!! empty($post->photo) ? config('app.photo_default') : url($post->photo)  !!}" width="100" height="100">
@endif  
</div>

<div class="form-group col-sm-12">
    {!! Form::label('photo', 'Photo:') !!}
    {!! Form::text('photo', null, ['class' => 'form-control', 'name'=>'photo', 'readonly'=>'readonly', 'placeholder'=>'Gunakan button "Select Image"']) !!}
    <a href="{!! route('fileupload') !!}" class="btn btn-success popup_selector form-control" data-inputid="photo">Select Image</a>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-danger']) !!}
    <a href="{!! url('post?type='.$type) !!}" class="btn btn-default">Cancel</a>
</div>
