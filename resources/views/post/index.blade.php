@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Post
                <small>{{ $type }}</small>
        </h1>

        <h1 class="pull-right">
           <a class="btn btn-danger pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! url('post/create?type='.$type) !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
  <div class="box-header">
  <h3 class="box-title">Data {{ $type }}</h3>


  <form class="navbar-form navbar-right" method="GET" >
  <div class="box-tools">
    <div class="input-group input-group-sm" style="width: 150px;">
      <input type="text" name="cari" class="form-control pull-right" placeholder="Search"  >
      <input type="hidden" name="type" value="{{ $type }}">
      <div class="input-group-btn">
        <button  type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
      </div>
    </div>
  </div>
  </form>

</div>              
     

                @if($post->isEmpty())
                    <div class="text-center">No Artis found.</div>
                @else
                     @include('post.table')

                {{ $post->links() }}
                
                @endif

            </div>
        </div>
    </div>
@endsection

