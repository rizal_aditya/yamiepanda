@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>Post
            <small>{{ $type }}</small>
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($post, ['route' => ['post.update', $post->id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('post.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection