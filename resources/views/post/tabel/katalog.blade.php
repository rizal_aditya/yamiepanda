
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="post-table">
    <thead>
        <th>Image</th> 
        <th>Produk</th>
        <th>Kategori</th>
        <th>Jenis</th>
        <th>Deskripsi</th>
        <th>Harga</th>
        <th>Sebarkan</th>
        <th>Status</th>
        <th colspan="3">Aksi</th>
    </thead>
    <tbody>
    @foreach($post as $post)
        <tr>
             <td><img width="50" height="50" src="{!! empty($post->photo) ? config('app.photo_default') : url($post->photo)  !!} "></td>
            <td>{!! $post->title !!}</td>
             <td>{!! $post->category->name !!}</td>
              <td>{!! $post->jenis !!}</td>
            <td>{!! $post->content !!}</td>
            <td>Rp {!! number_format($post->price, 0, ".",".") !!},00</td>
            <td>{!! $post->broadcast_type !!}</td>
            <td>@if($post->status == '1')Publish @else No Publish @endif</td>
            <td>
                {!! Form::open(['route' => ['post.destroy', $post->id.''.'?type='.$type], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! url('post/'.$post->id.'/?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! url('post/'.$post->id.'/edit?type='.$type) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

</div>



<style type="text/css">
.red, i.icon.red, i.fa.red {
    color: #fd685c;
}
.small-icon, .small-icon-text, .small-icon-text h4, .small-icon-text p {
    clear: none;
}
.small-icon {
    display: block;
    float: left;
    font-size: 1.667em;
}
.small-icon, .big-icon {
    width: 2em;
    height: 2em;
    -webkit-border-radius: 25%;
    -moz-border-radius: 25%;
    border-radius: 25%;
    background-color: rgba(0,0,0,0.03);
    line-height: 2;
}
.icon {
    display: inline-block;
}
</style>