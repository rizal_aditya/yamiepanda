

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'Server:') !!}
    <p>{!! $transaction->user->name !!}</p>
</div>

<!-- Costumer Id Field -->
<div class="form-group">
    {!! Form::label('costumer_id', 'Costumer Id:') !!}
    <p>
         <?php      
             $costumer =  DB::table('users')->where('id',$transaction->costumer_id)->first()->name;

             ?>   
             {!! $costumer !!}
      

    </p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>
        {!! $transaction->order_id !!}
        <table class="table">
            <thead>
                <th><b>Product</b></td>
                <th><b>Qty</b></td>
                <th><b>Diskon</b></td>  
             </thead>
             <tbody>
                @foreach($order as $order)
                <tr>
                    <td>
                        <?php 
                        $post =  DB::table('post')->where('id',$order->post_id)->first()->title; 

                    ?>
                        {!! $post !!}
                </td>
                    <td>{!! $order->qty !!}</td>
                    <td>{!! $order->discount !!} %</td>
                 </tr>   
                @endforeach
               </tbody> 

               
         </table>   


    </p>
</div>



<!-- Pay Field -->
<div class="form-group">
    {!! Form::label('pay', 'Bayar:') !!}
    <p>
         <?php $pay = number_format($transaction->pay,0,'','.'); ?> 
              Rp  {!! $pay !!}

    </p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>
          <?php $total = number_format($transaction->total,0,'','.'); ?> 

              Rp  {!! $total !!}

    </p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>
        @if($transaction->status =="0") Proses @else Terjual @endif

    </p>
</div>

