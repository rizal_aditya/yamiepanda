<table class="table table-responsive" id="transaction-table">
    <thead>
        <th>Costumer</th>
        <th>ID Order</th>
        <th>Bayar</th>
        <th>Total</th>
        <th>Poin</th>
         <th>Server</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($transaction as $transaction)
        <tr>
            
            <td>
                <?php      
                    $costumer =  DB::table('users')->where('id',$transaction->costumer_id)->first()->name;

             ?>   
             {!! $costumer !!}
            </td>
            <td>{!! $transaction->order_id !!}</td>
            
            <td>
                 <?php $pay = number_format($transaction->pay,0,'','.'); ?> 
              Rp  {!! $pay !!}

            </td>
            <td> 
                <?php $total = number_format($transaction->total,0,'','.'); ?> 

              Rp  {!! $total !!}
            </td>
            <td>{!! $transaction->point !!} Poin</td>
            <td>{!! $transaction->user->name !!}</td>
            <td>@if($transaction->status =="0") Proses @else Terjual @endif</td>
            <td>
                {!! Form::open(['route' => ['transaction.destroy', $transaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('transaction.show', [$transaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>