<table class="table table-responsive" id="asset-table">
    <thead>
        <th>Post Id</th>
        <th>Branch Id</th>
        <th>Type</th>
        <th>Name</th>
        <th>Content</th>
        <th>File</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($asset as $asset)
        <tr>
            <td>{!! $asset->post_id !!}</td>
            <td>{!! $asset->branch_id !!}</td>
            <td>{!! $asset->type !!}</td>
            <td>{!! $asset->name !!}</td>
            <td>{!! $asset->content !!}</td>
            <td>{!! $asset->file !!}</td>
            <td>{!! $asset->status !!}</td>
            <td>
                {!! Form::open(['route' => ['asset.destroy', $asset->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('asset.show', [$asset->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('asset.edit', [$asset->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>