<?php

namespace App\Repositories;

use App\Models\Post;
use InfyOm\Generator\Common\BaseRepository;

class PostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'branch_id',
        'user_id',
        'category_id',
        'costumer_id',
        'point',
        'price',
        'discount',
        'jenis',
        'date',
        'type',
        'title',
        'content',
        'keyword',
        'description',
        'start_date',
        'end_date',
        'broadcast_type',
        'status',
        'shared_id',
        'photo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Post::class;
    }
}
