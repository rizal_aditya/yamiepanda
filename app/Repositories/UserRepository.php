<?php

namespace App\Repositories;

use App\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
        'password',
        'client_id',
        'birthday',
        'religion',
        'phone',
        'address',
        'photo',
        'point',
        'status',
        'access',
        'type'
        
       
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}