<?php

namespace App\Repositories;

use App\Models\Keranjang;
use InfyOm\Generator\Common\BaseRepository;

class KeranjangRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'costumer_id',
        'meja_id',
        'order_id',
        'branch_id',
        'bayar',
        'point',
        'total',
        'potongan',
        'kembali',
        'jenis',
        'type',
        'bank',
        'no_atm',
        'status'
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Keranjang::class;
    }
}
