<?php

namespace App\Repositories;

use App\Models\Shared;
use InfyOm\Generator\Common\BaseRepository;

class SharedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'logo',
        'logo_hover',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Shared::class;
    }
}
