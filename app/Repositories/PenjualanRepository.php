<?php

namespace App\Repositories;

use App\Models\Penjualan;
use InfyOm\Generator\Common\BaseRepository;

class PenjualanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'post_id',
        'order_id',
        'category_id',
        'code',
        'price',
        'qty',
        'discount',
        'total',
        'point',
        'type',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Penjualan::class;
    }
}
