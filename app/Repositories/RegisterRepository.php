<?php

namespace App\Repositories;

use App\Models\Register;
use InfyOm\Generator\Common\BaseRepository;

class RegisterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'access',
        'name',
        'owner',
        'photo',
        'reg_number',
        'phone',
		'address',
		'decscription',
		'whatapps',
		'bbm',
		'color',
		'background',
		'website',
		'youtube',
		'api_key',
		'sender_id',
		'link',
		'packet',
		'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Register::class;
    }
}
