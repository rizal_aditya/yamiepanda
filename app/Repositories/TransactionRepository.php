<?php

namespace App\Repositories;

use App\Models\Transaction;
use InfyOm\Generator\Common\BaseRepository;

class TransactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'costumer_id',
        'order_id',
        'keranjang_id',
        'branch_id',
        'qty',
        'jenis',
        'type',
        'post_id',
        'price',
        'noted',
        'point',
        'discount',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transaction::class;
    }
}
