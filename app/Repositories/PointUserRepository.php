<?php

namespace App\Repositories;

use App\Models\PointUser;
use InfyOm\Generator\Common\BaseRepository;

class PointUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'order_id',
        'costumer_id',
        'point',
        'qrcode',
        'img_qrcode',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PointUser::class;
    }
}
