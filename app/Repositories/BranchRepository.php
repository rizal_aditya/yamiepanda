<?php

namespace App\Repositories;

use App\Models\Branch;
use InfyOm\Generator\Common\BaseRepository;

class BranchRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'user_id',
        'description',
        'telp',
        'latitude',
        'longitude',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Branch::class;
    }
}
