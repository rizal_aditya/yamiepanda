<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Device",
 *      required={"user_id", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="model",
 *          description="model",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="platform",
 *          description="platform",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="platform_version",
 *          description="platform_version",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="uuid",
 *          description="uuid",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="gcm",
 *          description="gcm",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="qrcode",
 *          description="qrcode",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Device extends Model
{
    //use SoftDeletes;

    public $table = 'device';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'name',
        'model',
        'platform',
        'platform_version',
        'uuid',
        'gcm',
        'qrcode',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'name' => 'string',
        'model' => 'string',
        'platform' => 'string',
        'platform_version' => 'string',
        'uuid' => 'string',
        'gcm' => 'string',
        'qrcode' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'status' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
}
