<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Client",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="role_id",
 *          description="role_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="owner",
 *          description="owner",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="photo",
 *          description="photo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Register extends Model
{
    //use SoftDeletes;

    public $table = 'client';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'type',
        'role_id',
        'name',
        'owner',
        'photo',
        'reg_number',
        'phone',
        'email',
        'address',
		'decscription',
		'whatapps',
		'bbm',
		'color',
		'background',
		'website',
		'youtube',
		'api_key',
		'sender_id',
		'link',
		'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string',
        'role_id' => 'integer',
        'name' => 'string',
        'owner' => 'string',
        'photo' => 'string',
        'phone' => 'string',
        'email' => 'string',
		'address'=>'text',
		'decscription'=>'text',
		'whatapps'=>'string',
		'bbm'=>'string',
		'color'=>'string',
		'background'=>'string',
		'website'=>'string',
		'youtube'=>'string',
		'api_key'=>'text',
		'sender_id'=>'text',
		'link'=>'string',
		
		'status'=>'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type' => 'required',
        'role_id' => 'required',
        'name' => 'required',
        'owner' => 'required',
        'email' => 'required|unique:client',
		'phone' => 'required'
		

    ];

    public function Role()
    {
        return $this->belongsTo('App\Role');
    }

    public function user() {
        return $this->hasMany('App\User');
    }

    public function branch() {
        return $this->hasMany('App\Models\Branch');
    }

    public function transaction() {
        return $this->hasMany('App\Models\Branch');
    }

    
}
