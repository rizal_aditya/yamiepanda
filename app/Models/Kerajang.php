<?php namespace App;

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Keranjang extends Model
{

    //use SoftDeletes;
    
    public $table = 'keranjang';
    
    //protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     public $fillable = [
        
        'user_id',
        'costumer_id',
        'meja_id',
        'order_id',
        'branch_id',
        'bayar',
        'total',
        'potongan',
        'kembali',
        'jenis',
        'bank',
        'no_atm',
        'status'
       
       
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */

    
    protected $casts = [
        
        'user_id' => 'integer',
        'costumer_id' => 'integer',
        'meja_id' => 'integer',
        'order_id' => 'integer',
        'branch_id' => 'integer',
        'bayar' => 'integer',
        'total' => 'integer',
        'potongan'=>'integer',
        'kembali' => 'integer',
        'jenis'=>'string',
        'bank' => 'string',
        'no_atm' => 'integer',
        'status' => 'integer'
        
    ];

    public static $rules = [
         'user_id' => 'required'
    ];


  

    

    

    

}
