<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Transaction",
 *      required={"user_id", "costumer_id", "qty", "pay", "total", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="costumer_id",
 *          description="costumer_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="order_id",
 *          description="order_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="qty",
 *          description="qty",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pay",
 *          description="pay",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="total",
 *          description="total",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Transaction extends Model
{
    use SoftDeletes;

    public $table = 'transaction';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'costumer_id',
        'order_id',
        'keranjang_id',
        'qty',
        'jenis',
        'type',
        'post_id',
        'branch_id',
        'price',
        'total',
        'noted',
        'point',
        'discount',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'costumer_id' => 'integer',
        'order_id' => 'string',
         'keranjang_id' => 'integer',
        'qty' => 'integer',
        'jenis'=>'string',
        'type'=>'string',
        'post_id' => 'integer',
        'branch_id' => 'integer',
        'price' => 'integer',
        'total' => 'integer',
        'noted' => 'string',
        'point' =>'integer',
        'discount' =>'integer',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
     
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    
}
