<?php namespace App;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Acoustep\EntrustGui\Contracts\HashMethodInterface;
use Hash;



class User extends Model implements AuthenticatableContract, CanResetPasswordContract, ValidatingModelInterface, HashMethodInterface
{
  use Authenticatable, CanResetPassword, ValidatingModelTrait, EntrustUserTrait;

    protected $dates = ['dob'];
    //protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $throwValidationExceptions = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'client_id',
        'birthday',
        'religion',
        'phone',
        'address',
        'photo',
        'point',
        'status',
        'access',
        'type'
        

             ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    //protected $hashable = ['password'];

    protected $rulesets = [

        'creating' => [
            'email'      => 'required|email',
           // 'password'   => 'required',
            
        ],

        'updating' => [
            'email'      => 'required|email',
            'password'   => '',
            
        ],
    ];

    public static $rules = [
        
        'email' => 'required|email',
       // 'password' => 'required',
        'name' => 'required',
    ];


    public function entrustPasswordHash() 
    {
        $this->password = Hash::make($this->password);
        $this->save();
    }


    public function device()
    {
        return $this->hasMany('App\Models\Device');
    }



    


    public function role_user()
    {
        return $this->hasMany('App\RoleUser');
    }

    function getDobAttribute()
    {
        return $this->attributes['dob']->format('m/d/Y');
    }

    

    

    

}
