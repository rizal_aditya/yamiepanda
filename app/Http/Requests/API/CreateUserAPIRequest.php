<?php

namespace App\Http\Requests\API;

use App\User;
use InfyOm\Generator\Request\APIRequest;

class CreateUserAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'client_id' => 'required|unique:users,client_id,NULL,id,email,' . $this->email,
            'email' => 'required|email|unique:users,email,NULL,id,client_id,' . $this->client_id,
            'password' => 'required',
            'name' => 'required',
        ];
    }
}
