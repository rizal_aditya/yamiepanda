<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateStockRequest;
use App\Http\Requests\UpdateStockRequest;
use App\Repositories\StockRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\StockCriteria;
use Response;
use Auth;
use Illuminate\Routing\UrlGenerator;
use DB;

class StockController extends AppBaseController
{
    /** @var  StockRepository */
    private $StockRepository;

    public function __construct(StockRepository $StockRepo,UrlGenerator $url)
    {
        $this->StockRepository = $StockRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Stock.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

       

        return view('stock.index');
           
    }

    /**
     * Show the form for creating a new Stock.
     *
     * @return Response
     */
    public function create()
    {

       

        return view('stock.create');
        
        

        
    }

    public function editstock(Request $request)
    {

       $id = $request->get('id');
        $stok = DB::table('stock')->where('id',$id)->get();
        if($stok[0]->status ==1)
        {
            DB::table('stock')->where('id',$id)->update(['status'=>2]);
        }else{
            DB::table('stock')->where('id',$id)->update(['status'=>1]);
        }
        

        
    }

    public function deletestock(Request $request)
    {

       $id = $request->get('id');
       DB::table('stock')->where('id',$id)->delete();
       
        

        
    }

    public function checkstock(Request $request)
    {

         $user = Auth::user();
         $post_id = $request->get('post_id');
        $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();

        $stok = DB::table('stock as a')->select('a.post_id')->where(['a.branch_id'=>$cabang[0]->branch_id,'a.post_id'=>$post_id])->get();

        $return['data'] =   $stok;
        return $return;

    }

      public function getstock(Request $request)
    {

        $user = Auth::user();
        $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
         $category_id = $request->get('category_id');
         if($category_id =="")
         { 


        
        $stok = DB::table('stock as a')->join('post as b', 'a.post_id','=','b.id')->join('category as c', 'b.category_id','=','c.id')->select('a.id','b.price','b.title','c.name as kategori','a.status')->where(['a.branch_id'=>$cabang[0]->branch_id])->paginate(10);

    }else{

        if($category_id =="0")
        {

            $stok = DB::table('stock as a')->join('post as b', 'a.post_id','=','b.id')->join('category as c', 'b.category_id','=','c.id')->select('a.id','b.price','b.title','c.name as kategori','a.status')->where(['a.branch_id'=>$cabang[0]->branch_id])->paginate(10);
        }else{

           $stok = DB::table('stock as a')->join('post as b', 'a.post_id','=','b.id')->join('category as c', 'b.category_id','=','c.id')->select('a.id','b.price','b.title','c.name as kategori','a.status')->where(['a.branch_id'=>$cabang[0]->branch_id,'a.category_id'=>$category_id])->paginate(10);  
        }    
           
    }

        
        return $stok;
       
    }

    public function getcatalog(Request $request)
    {

        $category_id = $request->get('category_id');

        $katalog = DB::table('post')->select('id','title')->where(['category_id'=>$category_id,'status'=>1])->get();

        $return['data'] =  $katalog;
        return $return;
       
    }

     public function getcategori(Request $request)
    {

        

        $kategori = DB::table('category')->select('id','name')->get();

        $return['data'] =  $kategori;
        return $return;
       
    }

    /**
     * Store a newly created Stock in storage.
     *
     * @param CreateStockRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
        $category_id = $request->get('category_id');
        $post_id = $request->get('post_id');
        $status1 = $request->get('status1');
        $status2 = $request->get('status2');

        if(empty($status1))
        {

            $st = $status2;

        }else{
            $st = $status1;
        }    

        DB::table('stock')->insert(['user_id'=>$user->id,'category_id'=>$category_id,'post_id'=>$post_id,'branch_id'=>$cabang[0]->branch_id,'status'=>$st,'created_at'=>new \DateTime()]);


       
    }

    /**
     * Display the specified Stock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $Stock = $this->StockRepository->findWithoutFail($id);

        if (empty($Stock)) {
            Flash::error('Cabang Kosong');

            return redirect(route('cabang.index'));
        }

        return view('Stock.show')->with('Stock', $Stock);
    }

    /**
     * Show the form for editing the specified Stock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $Stock = $this->StockRepository->findWithoutFail($id);

        if (empty($Stock)) {
            Flash::error('Cabang Kosong');

            return redirect(route('Stock.index'));
        }

        return view('Stock.edit')->with('Stock', $Stock);
    }

    /**
     * Update the specified Stock in storage.
     *
     * @param  int              $id
     * @param UpdateStockRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockRequest $request)
    {
        $Stock = $this->StockRepository->findWithoutFail($id);

        if (empty($Stock)) {
            Flash::error('Stock not found');

            return redirect(route('cabang.index'));
        }

        $Stock = $this->StockRepository->update($request->all(), $id);

        Flash::success('Cabang berhasil di update.');

       return redirect(route('cabang.index'));
    }

    /**
     * Remove the specified Stock from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $Stock = $this->StockRepository->findWithoutFail($id);

        if (empty($Stock)) {
            Flash::error('Cabang Kosong');

            return redirect(route('cabang.index'));
        }

        $this->StockRepository->delete($id);

        Flash::success('Cabang berhasil dihapus.');

        return redirect(route('cabang.index'));
    }
}
