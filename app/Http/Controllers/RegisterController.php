<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\ClientRepository;
use App\Repositories\PersonRepository;
use App\Repositories\UserRepository;
use App\Repositories\PersonUserRepository;
use App\Repositories\RoleUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\PersonSearchCriteria;
use App\Criteria\ClientCriteria;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Response;
use Auth;
use Hash;
use App\User;
class RegisterController extends AppBaseController
{
   /** @var  ClientRepository */
    private $clientRepository;
    private $personRepository;
    private $userRepository;
    private $roleuserRepository;
    use AuthenticatesAndRegistersUsers;

    public function __construct(ClientRepository $clientRepo,PersonRepository $personRepo,UserRepository $userRepo,PersonUserRepository $personUserRepo,RoleUserRepository $roleuserRepo)
    {
        $this->clientRepository = $clientRepo;
        $this->personRepository = $personRepo;
        $this->userRepository = $userRepo;
        $this->personUserRepository = $personUserRepo;
        $this->RoleUserRepository =  $roleuserRepo;
    }


     /**
     * Display a listing of the Client.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
  
       $cari = $request->get('name', null);
       $type ="teacher";

          $client = $request['client_id']; 
          $branch = $request['branch_id']; 
          $br = $request['branch'];
          $name = $request['name'];
          $endcodename = strtr(base64_encode($name), '+/', '-_');
          $endcodec = strtr(base64_encode($client), '+/', '-_');
          $endcodecbranch = strtr(base64_encode($branch), '+/', '-_');
          $decodeb = base64_decode(strtr($br, '-_', '+/'));

      $this->personRepository->pushCriteria(new RequestCriteria($request));
      $this->personRepository->pushCriteria(new PersonSearchCriteria($type,$decodeb));
      $register = $this->personRepository->paginate(10);

       if ($cari!=null)
       {
         return redirect(url('register/?access='.$endcodec.'&branch='.$endcodecbranch.'&search=name:'. $name.'&searchFields=name:like'));
         
       }


        return view('register.teacher')
            ->with(['register' => $register,'type' => $type]);
    }

    public function check(Request $request)
    {

    	  $client = $request['client_id'];
    	  $endcodec = strtr(base64_encode($client), '+/', '-_');
	      return redirect(url('register?access='.$endcodec));
    	 
    	  	
    }

    public function search(Request $request)
    {
          $this->clientRepository->pushCriteria(new RequestCriteria($request));
          $client = $request['client_id']; 
          $branch = $request['branch_id']; 
          $name = $request['name'];
          $endcodec = strtr(base64_encode($client), '+/', '-_');
          $endcodecbranch = strtr(base64_encode($branch), '+/', '-_');
          return redirect(url('register/?access='.$endcodec.'&branch='.$endcodecbranch.'&search=name:'.$name.'&searchFields=name:like'));
         
            
    }

    public function teacher(Request $request)
    {

           $person = $request['person_id'];
          $endcodep = strtr(base64_encode($person), '+/', '-_');
          $client = $request['client_id'];
          $endcodec = strtr(base64_encode($client), '+/', '-_');

          

         return redirect(url('register/show?access='.$endcodec.'&account='.$endcodep));



    } 

    public function show(Request $request)
    {

         $client = $request['client_id']; 
         $decode = base64_decode(strtr($client, '-_', '+/'));

         return view('register.form-register-teacher')
        ->with(['register' => $decode,'client' => $decode]);
    }  

     protected function create(array $data)
    {

        $data['password'] =  Hash::make($data['password']);
        $user = User::create($data);

        /*if ($user) {
            $role = Role::where('name', $data['role'])->first();
            $roleUser = new RoleUser;
            $roleUser->user_id = $user->id;
            $roleUser->role_id = $role->id;
        }*/

        return $user;
    }



    public function store(CreateUserRequest $request)
    {


        $input = $request->all();
        $person = $request->person_id;

        $user = $this->create($input);
        $id   = $user->id;

        $personUser['user_id'] = $id;
        $personUser['person_id'] = $person;
        $personUser['type'] = 'teacher';
        $personUser['status'] = '1';
        $personUser = $this->personUserRepository->create($personUser);

        $roleuser['user_id'] = $id;
        $roleuser['role_id'] = $request->role_id;

        $role = $this->RoleUserRepository->create($roleuser);


        Flash::success('Register saved successfully.');
        return redirect(url('access'));

    }  
}
