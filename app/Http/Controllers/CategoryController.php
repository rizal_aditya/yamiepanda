<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\CategoryCriteria;
use Response;
use App\Models\Category;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->categoryRepository->pushCriteria(new RequestCriteria($request));
        $category = $this->categoryRepository->all();

        return view('category.index')
            ->with('category', $category);
    }

    public function getcategory(Request $request)
    {

        $category = $this->categoryRepository->all();
        $content = '';
        $data = $category;

        if($data->isEmpty())
        {
            $content  .= '<li><div class="center"><h4>Category Not Found</h4></div></li>';
        }else{    

            foreach($data as $data)
            {
                $content .= '<li><div class="center"><a onClick="getcatalog('.$data->id.');"  href="#" ><img src="'.url($data->photo).'" width="70" ><h4>'.$data->name.'</h4></a></div></li>';
            }

        }    
        
        return $content;
        
    }

    public function name(Request $request)
    {

        $id = $request->get('id');
        $category = $this->categoryRepository->findByField('id',$id);
        $content = '';
        $data = $category;
        
            foreach($data as $data)
            {
                $content .= '<h4>'.$data->name.'</h4>';
            }
        
        return $content;

    }

    public function value(Request $request)
    {

        $id = $request->get('id');
        $category = $this->categoryRepository->findByField('id',$id);
        $content = '';
        $data = $category;
        
            foreach($data as $data)
            {
                $content .= '<input type="hidden" name="code_category" id="code_category" value="'.$data->id.'">';
            }
        
        return $content;

    }

                   


    public function searchcategory(Request $request)
    {

        $search = $request->get('search');
        $category = Category::where('name','like', '%'.$search.'%')->orderBy('id','desc')->get(); 
        $content = '';
        $data = $category;

        if($data->isEmpty())
        {
            $content  .= '<li><div class="center"><h4>Category Not Found</h4></div></li>';
        }else{   

            foreach($data as $data)
            {
                $content .= '<li><div class="center"><a onClick="getcatalog('.$data->id.');" href="#" ><img src=" '.url($data->photo).'" width="70" ><h4>'.$data->name.'</h4></a></div></li>';
            }
        }
        return $content;
        
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $input = $request->all();

        $category = $this->categoryRepository->create($input);

        Flash::success('Category saved successfully.');

        return redirect(route('category.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('category.index'));
        }

        return view('category.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('category.index'));
        }

        return view('category.edit')->with('category', $category);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('category.index'));
        }

        $category = $this->categoryRepository->update($request->all(), $id);

        Flash::success('Category updated successfully.');

        return redirect(route('category.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->findWithoutFail($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect(route('category.index'));
        }

        $this->categoryRepository->delete($id);

        Flash::success('Category deleted successfully.');

        return redirect(route('category.index'));
    }
}
