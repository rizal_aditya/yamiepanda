<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSharedAPIRequest;
use App\Http\Requests\API\UpdateSharedAPIRequest;
use App\Models\Shared;
use App\Repositories\SharedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SharedController
 * @package App\Http\Controllers\API
 */

class SharedAPIController extends AppBaseController
{
    /** @var  SharedRepository */
    private $sharedRepository;

    public function __construct(SharedRepository $sharedRepo)
    {
        $this->sharedRepository = $sharedRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/shared",
     *      summary="Get a listing of the Shared.",
     *      tags={"Shared"},
     *      description="Get all Shared",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Shared")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->sharedRepository->pushCriteria(new RequestCriteria($request));
        $this->sharedRepository->pushCriteria(new LimitOffsetCriteria($request));
        $shared = $this->sharedRepository->all();

        return $this->sendResponse($shared->toArray(), 'Shared retrieved successfully');
    }


      public function status(Request $request)
    {
            $status = "1";
        
            $shared = $this->sharedRepository->all();
                
                foreach($shared as $row)
                {
                    
                            $sh = $this->sharedRepository->findByField('status', $status);
                            return $this->sendResponse($sh, 'Shared By '.$shared[0]->name);
                    
                }

            
            
              
        
       
    }

    /**
     * @param CreateSharedAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/shared",
     *      summary="Store a newly created Shared in storage",
     *      tags={"Shared"},
     *      description="Store Shared",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Shared that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Shared")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shared"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateSharedAPIRequest $request)
    {
        $input = $request->all();

        $shared = $this->sharedRepository->create($input);

        return $this->sendResponse($shared->toArray(), 'Shared saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/shared/{id}",
     *      summary="Display the specified Shared",
     *      tags={"Shared"},
     *      description="Get Shared",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shared",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shared"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Shared $shared */
        $shared = $this->sharedRepository->find($id);

        if (empty($shared)) {
            return Response::json(ResponseUtil::makeError('Shared not found'), 404);
        }

        return $this->sendResponse($shared->toArray(), 'Shared retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateSharedAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/shared/{id}",
     *      summary="Update the specified Shared in storage",
     *      tags={"Shared"},
     *      description="Update Shared",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shared",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Shared that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Shared")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Shared"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateSharedAPIRequest $request)
    {
        $input = $request->all();

        /** @var Shared $shared */
        $shared = $this->sharedRepository->find($id);

        if (empty($shared)) {
            return Response::json(ResponseUtil::makeError('Shared not found'), 404);
        }

        $shared = $this->sharedRepository->update($input, $id);

        return $this->sendResponse($shared->toArray(), 'Shared updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/shared/{id}",
     *      summary="Remove the specified Shared from storage",
     *      tags={"Shared"},
     *      description="Delete Shared",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Shared",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Shared $shared */
        $shared = $this->sharedRepository->find($id);

        if (empty($shared)) {
            return Response::json(ResponseUtil::makeError('Shared not found'), 404);
        }

        $shared->delete();

        return $this->sendResponse($id, 'Shared deleted successfully');
    }
}
