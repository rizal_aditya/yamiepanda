<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBranchAPIRequest;
use App\Http\Requests\API\UpdateBranchAPIRequest;
use App\Models\Branch;
use App\Models\PersonUser;
use App\Repositories\BranchRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

/**
 * Class BranchController
 * @package App\Http\Controllers\API
 */

class BranchAPIController extends AppBaseController
{
    /** @var  BranchRepository */
    private $branchRepository;

    public function __construct(BranchRepository $branchRepo)
    {
        $this->branchRepository = $branchRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/branch",
     *      summary="Get a listing of the Branch.",
     *      tags={"Branch"},
     *      description="Get all Branch",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Branch")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->branchRepository->pushCriteria(new RequestCriteria($request));
        $this->branchRepository->pushCriteria(new LimitOffsetCriteria($request));
        $branch = $this->branchRepository->all();

        return $this->sendResponse($branch->toArray(), 'Branch retrieved successfully');
    }

    public function clientBranch(Request $request)
    {
        $this->branchRepository->pushCriteria(new RequestCriteria($request));
        $this->branchRepository->pushCriteria(new LimitOffsetCriteria($request));
        $branch = $this->branchRepository->findByField('client_id', Auth::user()->client_id);

        return $this->sendResponse($branch->toArray(), 'Branch retrieved successfully');
    }

    public function myBranch(Request $request)
    {

        $this->branchRepository->pushCriteria(new RequestCriteria($request));
        $this->branchRepository->pushCriteria(new LimitOffsetCriteria($request));

        $myBranch = [];

        $personUsers = PersonUser::where('user_id', Auth::user()->id)->get();
        foreach ($personUsers as $personUser) {
            array_push($myBranch, $personUser->person->branch_id);
        }

        $branch = $this->branchRepository->findWhereIn('id', $myBranch);

        return $this->sendResponse($branch->toArray(), 'Branch retrieved successfully');
    }

    /**
     * @param CreateBranchAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/branch",
     *      summary="Store a newly created Branch in storage",
     *      tags={"Branch"},
     *      description="Store Branch",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Branch that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Branch")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Branch"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBranchAPIRequest $request)
    {
        $input = $request->all();

        $branch = $this->branchRepository->create($input);

        return $this->sendResponse($branch->toArray(), 'Branch saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/branch/{id}",
     *      summary="Display the specified Branch",
     *      tags={"Branch"},
     *      description="Get Branch",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Branch",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Branch"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Branch $branch */
        $branch = $this->branchRepository->find($id);

        if (empty($branch)) {
            return Response::json(ResponseUtil::makeError('Branch not found'), 404);
        }

        return $this->sendResponse($branch->toArray(), 'Branch retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBranchAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/branch/{id}",
     *      summary="Update the specified Branch in storage",
     *      tags={"Branch"},
     *      description="Update Branch",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Branch",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Branch that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Branch")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Branch"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBranchAPIRequest $request)
    {
        $input = $request->all();

        /** @var Branch $branch */
        $branch = $this->branchRepository->find($id);

        if (empty($branch)) {
            return Response::json(ResponseUtil::makeError('Branch not found'), 404);
        }

        $branch = $this->branchRepository->update($input, $id);

        return $this->sendResponse($branch->toArray(), 'Branch updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/branch/{id}",
     *      summary="Remove the specified Branch from storage",
     *      tags={"Branch"},
     *      description="Delete Branch",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Branch",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Branch $branch */
        $branch = $this->branchRepository->find($id);

        if (empty($branch)) {
            return Response::json(ResponseUtil::makeError('Branch not found'), 404);
        }

        $branch->delete();

        return $this->sendResponse($id, 'Branch deleted successfully');
    }
}
