<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePointUserAPIRequest;
use App\Http\Requests\API\UpdatePointUserAPIRequest;
use App\Models\PointUser;
use App\Repositories\PointUserRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use DB;
use App\User;

/**
 * Class PointUserController
 * @package App\Http\Controllers\API
 */

class PointUserAPIController extends AppBaseController
{
    /** @var  PointUserRepository */
    private $pointUserRepository;
    private $transactionRepository;
    private $userRepository;

    public function __construct(UserRepository $userRepo,PointUserRepository $pointUserRepo,TransactionRepository $transactionRepo)
    {
        $this->pointUserRepository = $pointUserRepo;
        $this->transactionRepository = $transactionRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/pointUser",
     *      summary="Get a listing of the PointUser.",
     *      tags={"PointUser"},
     *      description="Get all PointUser",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PointUser")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->pointUserRepository->pushCriteria(new RequestCriteria($request));
        $this->pointUserRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pointUser = $this->pointUserRepository->all();

        return $this->sendResponse($pointUser->toArray(), 'PointUser retrieved successfully');
    }

    /**
     * @param CreatePointUserAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/pointUser",
     *      summary="Store a newly created PointUser in storage",
     *      tags={"PointUser"},
     *      description="Store PointUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PointUser that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PointUser")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PointUser"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePointUserAPIRequest $request)
    {
        $user =  Auth::user();
        $input = $request->all();
        $input['status'] = '1';

         $point = $request->point;
         $data = DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->get();
         $id = $data[0]->id;

        $pointUser = $this->pointUserRepository->find($id);

        if (empty($pointUser)) {
            return Response::json(ResponseUtil::makeError('Point not found'), 404);
        }

        if($data[0]->status ==1)
        {
            return Response::json(ResponseUtil::makeError('Point sudah digunakan'), 404);

        }else{

             $hasil = $user->point+$data[0]->point;
             DB::table('users')->where('id', $user->id)->update(['point' => $hasil]);   

             $pointUser = $this->pointUserRepository->update($input, $id);
             return $this->sendResponse($pointUser->toArray(), 'Point sukses disimpan');
        }    

       
      
      // $point =  DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->get();
      // if(empty($point))
      // {



      // }else{

      //           if($point[0]->status>0)
      //           { 

              
                
      //           }else{

      //                 DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->Update(['status'=>'1']);
      //           }

      // }  

      

        $data = DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->get();
        return $this->sendResponse($data, 'Point sukses disimpan');

       // if($point !='0')
       // {
       //        DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->Update(['status'=>'1']);
       //        // $hasil = $user->point+$point;
       //        // DB::table('users')->where('id', $user->id)->update(['point' => $hasil]);
       //        // $pointy =  DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->get();

       //        // return $this->sendResponse($pointy, 'Point sukses disimpan');

       // }else{
       //           DB::table('point_user')->where(['qrcode'=>$point,'costumer_id'=>$user->id])->update(['status'=>1]);
       //          return Response::json(ResponseUtil::makeError('Point sudah digunakan'), 404);


       // }
       
        // $id = $user->id;
        // if($user->status != "0")
        // {  


        //      if($user->point != "0")
        //      {

        //        $hasil = $user->point+$point;

        //        DB::table('users')
        //         ->where('id', $id)
        //         ->update(['point' => $hasil]);

        //      }

        //         $dbpoint = DB::table('point_user')->get();
        //         if(!empty($dbpoint))
        //         {
        //                 foreach ($dbpoint as $row)
        //                 {
                              
        //                       if($row->transaction_id !="")
        //                       {

        //                          return Response::json(ResponseUtil::makeError('Point sudah digunakan'), 404);  
        //                       }  
        //                 }

        //         }else{

        //                         $pointUser = $this->pointUserRepository->create($input);

        //                         DB::table('transaction')
        //                         ->where('id', $id_transaction)
        //                         ->update(['status' => $input['status']]);
        //                         return $this->sendResponse($pointUser->toArray(), 'Point sukses disimpan');

        //         }
             




               
         

        // }else{

        //   return Response::json(ResponseUtil::makeError('Bukan costumer Premium'), 404);  
        // }

    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/pointUser/{id}",
     *      summary="Display the specified PointUser",
     *      tags={"PointUser"},
     *      description="Get PointUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PointUser",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PointUser"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PointUser $pointUser */
        $pointUser = $this->pointUserRepository->find($id);

        if (empty($pointUser)) {
            return Response::json(ResponseUtil::makeError('PointUser not found'), 404);
        }

        return $this->sendResponse($pointUser->toArray(), 'PointUser retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePointUserAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/pointUser/{id}",
     *      summary="Update the specified PointUser in storage",
     *      tags={"PointUser"},
     *      description="Update PointUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PointUser",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PointUser that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PointUser")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PointUser"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePointUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var PointUser $pointUser */
        $pointUser = $this->pointUserRepository->find($id);

        if (empty($pointUser)) {
            return Response::json(ResponseUtil::makeError('PointUser not found'), 404);
        }

        $pointUser = $this->pointUserRepository->update($input, $id);

        return $this->sendResponse($pointUser->toArray(), 'PointUser updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/pointUser/{id}",
     *      summary="Remove the specified PointUser from storage",
     *      tags={"PointUser"},
     *      description="Delete PointUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PointUser",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PointUser $pointUser */
        $pointUser = $this->pointUserRepository->find($id);

        if (empty($pointUser)) {
            return Response::json(ResponseUtil::makeError('PointUser not found'), 404);
        }

        $pointUser->delete();

        return $this->sendResponse($id, 'PointUser deleted successfully');
    }
}
