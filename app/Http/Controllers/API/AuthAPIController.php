<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use File;
use Auth;
use Config;

class AuthAPIController extends Controller
{
    use ResetsPasswords;

    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password', 'client_id');
        $credentials['status'] = 1;

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'message' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'could_not_create_token'], 500);
        }

        $userDir  =   base64_encode(Auth::user()->email);
        if (!File::exists('files')) {
            File::makeDirectory('files',0775);
        }
        if (!File::exists(public_path('files').'/'.$userDir)) {
            File::makeDirectory(public_path('files').'/'.$userDir,0775);
        }
        Config::set('elfinder.dir',["files/$userDir"]);

        // all good so return the token
        return response()->json(['success' => true, 'data' => compact('token'), 'message' => 'Token success generated']);
    }


    public function forgot(Request $request)
    {
        $this->validateSendResetLinkEmail($request);

        $broker = $this->getBroker();

        $response = Password::broker($broker)->sendResetLink(
            $this->getSendResetLinkEmailCredentials($request),
            $this->resetEmailBuilder()
        );

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return response()->json(['success' => true, 'message' => 'Rest password link send']);
            case Password::INVALID_USER:
            default:
                return response()->json(['success' => false, 'message' => 'Email not valid'], 401);
        }
    }
}