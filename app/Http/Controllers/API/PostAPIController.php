<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePostAPIRequest;
use App\Http\Requests\API\UpdatePostAPIRequest;
use App\Repositories\PostRepository;
use App\Repositories\AssetRepository;
use App\Repositories\UserRepository;
use App\Repositories\ConfirmationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\MyCriteria;
use App\Criteria\PostCriteria;
use Response;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Intervention\Image\ImageManagerStatic as Image;
use Config;
use Hash;
use Auth;
use App\User;
use App\Role;
use App\RoleUser;
use App\Models\Branch;
use App\Models\Client;
use App\Models\Post;

use App\Models\Device;

/**
 * Class PostController
 * @package App\Http\Controllers\API
 */

class PostAPIController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;
    private $userRepository;
    public function __construct(PostRepository $postRepo,UserRepository $userRepo)
    {
        $this->postRepository = $postRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/post",
     *      summary="Get a listing of the Post.",
     *      tags={"Post"},
     *      description="Get all Post",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Post")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {

        $this->postRepository->pushCriteria(new MyCriteria($request));
        $this->postRepository->pushCriteria(new LimitOffsetCriteria($request));
        $post = $this->postRepository->all();

         

        return $this->sendResponse($post->toArray(), 'Post retrieved successfully');
    }

    public function getNews(Request $request)
    {
        $type ="";
        $this->postRepository->pushCriteria(new PostCriteria($type));    
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $this->postRepository->pushCriteria(new LimitOffsetCriteria($request));

        $post = $this->postRepository->findWhere([
            'type' => 'news',
            'status' => '1'
           
        ]);

        return $this->sendResponse($post, 'News retrieved successfully');
    }

    public function getPromo(Request $request)
    {
        $type ="";

        $user = Auth::user();
        $this->postRepository->pushCriteria(new PostCriteria($type));
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $this->postRepository->pushCriteria(new LimitOffsetCriteria($request));

        if($user->access =="premium")
        {

    
        $first = $this->postRepository->findWhere([
            'type' => 'promo',
            'status' => '1',
            'broadcast_type' => 'semua'
        ]);

            $post = $this->postRepository->findWhere([
                'type' => 'promo',
                'status' => '1',
                'broadcast_type' => 'premium'
            ])->union($first);

        }else{

            $first = $this->postRepository->findWhere([
                'type' => 'promo',
                'status' => '1',
                'broadcast_type' => 'semua'
            ]);  

              $post = $this->postRepository->findWhere([
                'type' => 'promo',
                'status' => '1',
                'broadcast_type' => 'standar'
              ])->union($first); 



        }

       
        return $this->sendResponse($post, 'Katalog Promo retrieved successfully');
    }


    public function getKatalog(Request $request)
    {
        $type ="";
         $user = Auth::user();
        $this->postRepository->pushCriteria(new PostCriteria($type));
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $this->postRepository->pushCriteria(new LimitOffsetCriteria($request));

             $post = $this->postRepository->findWhere([
                'type' => 'katalog',
                'status' => '1',
                'broadcast_type' => 'semua'
              ]); 



        return $this->sendResponse($post, 'Katalog Standar retrieved successfully');
    }


    public function getslider(Request $request)
    {
        $type ="";
        $this->postRepository->pushCriteria(new PostCriteria($type));
        $this->postRepository->pushCriteria(new RequestCriteria($request));
        $this->postRepository->pushCriteria(new LimitOffsetCriteria($request));

        $post = Post::where('type','news')->orWhere('type', 'promo')->limit(6)->get();

        return $this->sendResponse($post, 'News retrieved successfully');
    }
    

    /**
     * @param CreatePostAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/post",
     *      summary="Store a newly created Post in storage",
     *      tags={"Post"},
     *      description="Store Post",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Post that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Post")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Post"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
   public function store(CreatePostAPIRequest $request)
    {


        $input = $request->all();
        $post = $this->postRepository->create($input);
        return $this->sendResponse($post->toArray(), 'Post saved successfully');
        
        
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/post/{id}",
     *      summary="Display the specified Post",
     *      tags={"Post"},
     *      description="Get Post",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Post",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Post"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Post $post */
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            return Response::json(ResponseUtil::makeError('Post not found'), 404);
        }

        return $this->sendResponse($post->toArray(), 'Post retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePostAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/post/{id}",
     *      summary="Update the specified Post in storage",
     *      tags={"Post"},
     *      description="Update Post",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Post",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Post that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Post")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Post"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePostAPIRequest $request)
    {
        $input = $request->all();

        /** @var Post $post */
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            return Response::json(ResponseUtil::makeError('Post not found'), 404);
        }

        $post = $this->postRepository->update($input, $id);

        return $this->sendResponse($post->toArray(), 'Post updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/post/{id}",
     *      summary="Remove the specified Post from storage",
     *      tags={"Post"},
     *      description="Delete Post",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Post",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Post $post */
        $post = $this->postRepository->find($id);

        if (empty($post)) {
            return Response::json(ResponseUtil::makeError('Post not found'), 404);
        }

        $post->delete();

        return $this->sendResponse($id, 'Post deleted successfully');
    }

     public function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
       
        //Google cloud messaging GCM-API url
        $code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
        // Update your Google Cloud Messaging API Key
        //define("GOOGLE_API_KEY", $api_key);       
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);               
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}
