<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDeviceAPIRequest;
use App\Http\Requests\API\UpdateDeviceAPIRequest;
use App\Http\Requests\API\CheckDeviceAPIRequest;
use App\Models\Device;
use App\Repositories\DeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;

/**
 * Class DeviceController
 * @package App\Http\Controllers\API
 */

class DeviceAPIController extends AppBaseController
{
    /** @var  DeviceRepository */
    private $deviceRepository;

    public function __construct(DeviceRepository $deviceRepo)
    {
        $this->deviceRepository = $deviceRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/device",
     *      summary="Get a listing of the Device.",
     *      tags={"Device"},
     *      description="Get all Device",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Device")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->deviceRepository->pushCriteria(new RequestCriteria($request));
        $this->deviceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $device = $this->deviceRepository->all();

        return $this->sendResponse($device->toArray(), 'Device retrieved successfully');
    }

    /**
     * @param CreateDeviceAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/device",
     *      summary="Store a newly created Device in storage",
     *      tags={"Device"},
     *      description="Store Device",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Device that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Device")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Device"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDeviceAPIRequest $request)
    {
        $input = $request->all();

        $device = $this->deviceRepository->create($input);

        return $this->sendResponse($device->toArray(), 'Device saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/device/{id}",
     *      summary="Display the specified Device",
     *      tags={"Device"},
     *      description="Get Device",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Device",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Device"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Device $device */
        $device = $this->deviceRepository->find($id);

        if (empty($device)) {
            return Response::json(ResponseUtil::makeError('Device not found'), 404);
        }

        return $this->sendResponse($device->toArray(), 'Device retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDeviceAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/device/{id}",
     *      summary="Update the specified Device in storage",
     *      tags={"Device"},
     *      description="Update Device",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Device",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Device that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Device")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Device"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDeviceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Device $device */
        $device = $this->deviceRepository->find($id);

        if (empty($device)) {
            return Response::json(ResponseUtil::makeError('Device not found'), 404);
        }

        $device = $this->deviceRepository->update($input, $id);

        return $this->sendResponse($device->toArray(), 'Device updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/device/{id}",
     *      summary="Remove the specified Device from storage",
     *      tags={"Device"},
     *      description="Delete Device",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Device",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Device $device */
        $device = $this->deviceRepository->find($id);

        if (empty($device)) {
            return Response::json(ResponseUtil::makeError('Device not found'), 404);
        }

        $device->delete();

        return $this->sendResponse($id, 'Device deleted successfully');
    }

    public function check(CheckDeviceAPIRequest $request) {
        $input = $request->except('user_id', 'status');

        $input['user_id'] = Auth::user()->id;
        $input['status'] = 1;
        $device = $this->deviceRepository->findByField('uuid', $input['uuid']);

        if (empty($device->toArray())) {
            $device = $this->deviceRepository->create($input);
            $device['gcm_server'] = Auth::user()->client->api_key;

            return $this->sendResponse($device->toArray(), 'Device saved successfully');
        } else {
            $id = $device[0]->id;

            $device = $this->deviceRepository->update($input, $id);
            $device['gcm_server'] = Auth::user()->client->api_key;

            return $this->sendResponse($device->toArray(), 'Device updated successfully');
        }
    }
}
