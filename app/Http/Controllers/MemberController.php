<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleUserRepository;
use App\Http\Controllers\AppBaseController;
use App\Criteria\UserCriteria;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use App\User;
use QrCode;
use Illuminate\Routing\UrlGenerator;
use File;
use Storage;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Config;

class MemberController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    private $roleUserRepository;
    protected $url;

    public function __construct(UserRepository $userRepo,RoleUserRepository $roleUserRepo,UrlGenerator $url)
    {
        $this->userRepository = $userRepo;
        $this->roleUserRepository = $roleUserRepo;
         $this->url = $url;
       
    }

     /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return User
     */
   
    public function index(Request $request)
    {
       


        $type = $request->get('type');

        // if($status == "1")
        // {
        //     $type = "costumer";
        // }else{
        //     $type = "noncostumer";
        // }    

        $search = $request->get('search');
        if(empty($id))
        {

             if(empty($search))
             {
                $content = DB::table('users')->where(['type'=>$type,'status'=>'1'])->select('id','name','phone','photo','address','point')->orderBy('id','desc')->paginate();
             }else{
                $content = DB::table('users')->where(['type'=>$type,'status'=>'1'])->select('id','name','phone','photo','address','point')->where("name", "LIKE","%$search%")->orderBy('id','desc')->paginate();
             }   

              
        }else{
            $content = DB::table('users')->where(['type'=>$type,'status'=>'1'])->select('id','name','phone','photo','address','point')->orderBy('id','desc')->paginate();

        }    
        
        return $content;     
        
       
    }


    public function draft(Request $request)
    {
       

             $user = Auth::user();
       

            $search = $request->get('search');
       

             if(!empty($search))
             {

                $content = DB::table('keranjang as a')->join('users as b','a.costumer_id','=','b.id')->where(['a.user_id'=>$user->id,'a.status'=>'3'])->select('b.id as costumer_id','b.name','a.total','a.point','a.order_id')->where('b.name', 'like', ''.$search.'%')->orderBy('a.id','desc')->get();


             }else{

    $content = DB::table('keranjang as a')->join('users as b','a.costumer_id','=','b.id')->where(['a.user_id'=>$user->id,'a.status'=>'3'])->select('b.id as costumer_id','b.name','a.total','a.point','a.order_id')->orderBy('a.id','desc')->get();


             }   

        $return['data'] = $content;
        
        return $return;     
        
       
    }


   


      public function draftcancel(Request $request)
    {
       
        $order = $request->order_id;
        $costumer_id = $request->costumer_id;  

        DB::table("keranjang")->where(['order_id'=>$order,'costumer_id'=>$costumer_id])->update(['status'=>'0']); 
        DB::table("transaction")->where(['order_id'=>$order,'costumer_id'=>$costumer_id])->update(['status'=>'0']);

       $member = DB::table('keranjang as a')->join('users as b','a.costumer_id','=','b.id')->where(['a.order_id'=>$order,'costumer_id'=>$costumer_id])->select('a.id as keranjang_id','b.name','a.meja_id','a.costumer_id','a.order_id','a.status')->get();

       $keranjang =  DB::table("keranjang")->select('id as keranjang_id','total','point','order_id','status')->where(['order_id'=>$order,'costumer_id'=>$costumer_id])->get();

       $return['member'] = $member;
       $return['keranjang'] = $keranjang;
       $return['total'] = $keranjang[0]->total;
       $return['point'] = $keranjang[0]->point;
       return $return;
    }

   
    public function store(Request $request)
     {


             $input = $request->all();
            $phone = $request->phone;

             if(!empty($request->name))
              { 

                if(empty($request->phone))
                {

                    $phone = "000000000";
                } 

        $client = "1";
        DB::table('users')->insert(['client_id'=>$client,'name'=>$request->name,'phone'=>$phone,'address'=>$request->address,'access'=>'standar','type'=>'noncostumer','status'=>'1','photo'=>'img/avatar.png','created_at'=>new \DateTime()]);

            }

                      


     } 


    public function show(Request $request)
     {
        $id = $request->get('id');
      
        $content = DB::table('users')->where(['id'=>$id])->orderBy('id','desc')->get();

        return  $content;

     } 


      public function point(Request $request)
     {

        $user = Auth::user();

        $order_id = $request->get('order_id');
        $costumer_id = $request->get('costumer_id');
        $top_point = $request->get('point');
        $total = $request->get('total');
        $status = "0";

         $point = DB::table('point_user')->where(['user_id'=>$user->id,'status'=>$status])->get();

         if(empty($point))
         {   

                $kode = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 6)), 0, 6);
                
                $fileFormatqr = ".png";
                $qrPath = base64_encode('qrcode');
                $dir = Config::get('elfinder.dir')[0];
                $files = $dir."/".$qrPath;

                $fileQr = base64_encode($kode.$costumer_id . time());
                $fullqr = $dir."/".$qrPath."/".$fileQr.$fileFormatqr;
                File::makeDirectory($files, 0777, true, true);


                $png = QrCode::format('png')->size(500)->generate(''.$kode.$costumer_id.'', '../public/'.$fullqr);


              

        DB::table('point_user')->insert(['point'=>$top_point,'user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id,'qrcode'=>$kode.$costumer_id,'img_qrcode'=>$this->url->to($fullqr),'status'=>$status,'created_at'=> new \DateTime()]);


        // DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id])->update(['total'=>$total,'point'=>$top_point]);

            $pointx = DB::table('point_user as a')->select('a.id','c.name','a.img_qrcode','b.order_id','b.point')->join('keranjang as b', 'a.order_id', '=', 'b.order_id')->join('users as c', 'a.costumer_id', '=', 'c.id')->where(['a.user_id'=>$user->id,'a.status'=>$status])->get();

        }
             $pointx = DB::table('point_user as a')->select('a.id','c.name','a.img_qrcode','b.order_id','b.point')->join('keranjang as b', 'a.order_id', '=', 'b.order_id')->join('users as c', 'a.costumer_id', '=', 'c.id')->where(['a.user_id'=>$user->id,'a.status'=>$status])->get();

            return $pointx;
     } 


      public function statuspoint(Request $request)
     {

        $user = Auth::user();
        $point_id = $request->get('point_id');
        $costumer_id = $request->get('costumer_id');
        $meja_id = $request->get('meja_id');

        $status = "1";
        $qty = "1";

        $point = DB::table('point_user')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id])->get();


        if($point[0]->costumer_id  ==  $costumer_id)
        {
            
             DB::table('point_user')->where(['id'=>$point_id,'user_id'=>$user->id,'costumer_id'=>$costumer_id])->update(['status'=>$status]);

             $pointx = DB::table('point_user as a')->select('a.id','c.name','d.order_id')->join('users as c', 'a.costumer_id', '=', 'c.id')->join('keranjang as d', 'a.costumer_id', '=', 'd.costumer_id')->where(['a.user_id'=>$user->id,'a.status'=>$status,'a.id'=>$point_id])->get();


             //$costumer = DB::table('users')->where(['id'=>$costumer_id])->get();
            // $jml = $costumer[0]->point+$point[0]->point;
            // DB::table('users')->where(['id'=>$costumer_id])->update(['point'=>$jml]);

              
             return $pointx;

        }    

    

     } 



     public function hapus(Request $request)
     {

            
            $id = $request->get('id');
            
            DB::table('users')->where('id',$id)->delete();

     } 



     

      
   
}
