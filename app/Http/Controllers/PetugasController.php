<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;
use Config;
use File;


class PetugasController extends AppBaseController
{
    /** @var  OrderRepository */
 
    private $userRepository;
    protected $url;

    public function __construct(UserRepository $userRepo,UrlGenerator $url)
    {
         $this->userRepository = $userRepo;
         $this->url = $url;
    }


    public function index(Request $request)
   {

      $user = Auth::user();
        if(empty($user->id))
        {
           return redirect(url('login'));
        }

      $petugas = DB::table('users as a')->select('a.id','a.photo','a.name','a.email','a.phone','a.status','b.branch_id')->join('client as b', 'a.client_id', '=', 'b.id')->where('a.type','petugas')->paginate(10);

  

       return view('petugas.index')->with(['petugas' => $petugas]);


   }


    public function cabang(Request $request)
    {
      $return ="";
       $cabang = DB::table('branch')->get();
     

       return $cabang; 
     }


    public function create(Request $request)
    {

         $branch = DB::table('branch')->get();
         if(empty($branch))
         {
          $cabang = "";

         }else{
           $cabang = $branch;

         } 

        return view('petugas.create')->with(['cabang' => $cabang ]);
           

    }

    public function store(CreateUserRequest $request)
    {
      
      $input = $request->all();
      $default ="admin";
      $input['password'] = bcrypt($default);

      $name = $request->name;
      $cabang =  $request->cabang;
      $role_id = "2";


      $latest = DB::table('client')->max('branch_id');

        if (!empty($latest)) { 
         $kode_user = 'NV00010';
        }

        $kode_user = ++$latest;                         
        if($request->photo =="")
        {
          $input['photo'] = 'img/avatar.png';
        }                                                                      


      DB::table('client')->insert(['role_id'=>$role_id,'name'=>$name,'branch_id'=>$kode_user,'status'=>'0','created_at'=>DB::raw('now()')]);

       $client = DB::table('client')->where(['status'=>'0'])->first();
      $input['client_id'] = $client->id;
      $input['type'] = "petugas"; 
     $user = $this->userRepository->create($input);

      $jml_cabang = count($cabang);
      if($jml_cabang > 0){
        for($i=0; $i<$jml_cabang; $i++)
        {
            
          DB::table('detail_petugas')->insert(['user_id'=>$user->id,'branch_id'=>$cabang[$i],'created_at'=>DB::raw('now()')]);
         

        }
      } else {
          
        DB::table('detail_petugas')->insert(['user_id'=>$user->id,'branch_id'=>$cabang,'created_at'=>DB::raw('now()')]);


      }

        
      
  
      
      

        // if ($request->photo == '') 
        // {
        //       $photo = 'img/avatar.png';
        //       $input['photo'] = $this->url->to('/'.$photo);

        // }else{    


        //             $images = base64_encode($request->photo);
        //             $fileFormat = ".jpg";
        //             $userPath = base64_encode(Auth::user()->email);
        //             $dir = Config::get('elfinder.dir')[0];
        //             $files = $dir."/".$userPath;
        //             $fileName = base64_encode(Auth::user()->email . time());
        //             $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
        //             File::makeDirectory($files, 0777, true, true);

        //             if ($image = base64_decode($images, true))
        //             {
        //                 $img = Image::make($image)->save($fullPath, 60);
        //                 $input['photo'] = $this->url->to('/'.$fullPath);
        //             } 


        //  } 

      

      DB::table('role_user')->insert(['role_id'=>$role_id,'user_id'=>$user->id]);

      DB::table('client')->where('status','0')->update(['status'=>'1']);

      $datauser = DB::table('role_user')->where('user_id',$user->id)->get();

    

        Flash::success('Petugas Berhasil Disimpan');
        return redirect(url('petugas'));
    }



    public function show($id,Request $request)
    {
      
        $user = DB::table('users as a')->select('b.branch_id','a.id','a.address','a.photo','a.name','a.phone','a.email','a.religion','a.birthday','a.created_at','a.status')->join('client as b', 'a.client_id', '=', 'b.id')->where('a.id',$id)->get();

         $kode = $user[0]->branch_id;
         $cabang_id = DB::table('detail_petugas as a')->select('a.branch_id','b.name')->join('branch as b', 'a.branch_id', '=', 'b.id')->where('a.user_id',$kode)->get();



        if (empty($user[0])) {
            Flash::error('Petugas Kosong');
            return redirect(url('petugas'));  
        }

      
          return view('petugas.show')->with(['petugas'=> $user,'dates'=> $this->convert($user[0]->birthday),'create'=>$this->created($user[0]->created_at),'cabang'=>$cabang_id]);
    }

    public function edit($id,Request $request)
    {

        $branch = DB::table('branch')->get();
         if(empty($branch))
         {
          $cabang1 = "";
          $cabang2 = "";
         }else{
           $cabang1 = $branch;
           $cabang2 = $branch;
         } 

        $user = DB::table('users as a')->select('b.branch_id','a.id','a.address','a.photo','a.name','a.phone','a.email','a.religion','a.birthday','a.created_at','a.status')->join('client as b', 'a.client_id', '=', 'b.id')->where('a.id',$id)->get();

         $kode = $user[0]->branch_id;
         $cabang_id = DB::table('detail_petugas as a')->select('a.branch_id','b.name')->join('branch as b', 'a.branch_id', '=', 'b.id')->where('a.user_id',$kode)->get();
      
         $jml = DB::table('detail_petugas as a')->where('a.user_id',$kode)->count('a.user_id');


        if (empty($user[0])) {
            Flash::error('Petugas Kosong');

            return redirect(url('petugas'));
        }

        return view('petugas.edit')->with(['petugas'=>$user[0],'cabang1'=>$cabang1,'cabang2'=>$cabang2,'cabang_id'=>$cabang_id,'jml'=>$jml]);

    }

    public function update($id, UpdateUserRequest $request)
    {

       $input = $request->all();
        $user = DB::table('users as a')->select('a.client_id','a.id','a.address','a.photo','a.name','a.phone','a.email','a.religion','a.birthday','a.created_at','a.status','b.branch_id')->join('client as b', 'a.client_id', '=', 'b.id')->where('a.id',$id)->get();

        $cabang = $request->cabang;

        $jml_cabang = count($cabang);
        $hapus_dulu = DB::table('detail_petugas')->where('user_id',$user[0]->branch_id)->delete();
          if($jml_cabang > 0){
            for($i=0; $i<$jml_cabang; $i++){
               DB::table('detail_petugas')->insert(['user_id'=>$user[0]->branch_id,'branch_id'=>$cabang[$i],'created_at'=>DB::raw('now()')]);

            }
          } else {
            DB::table('detail_petugas')->insert(['user_id'=>$user[0]->branch_id,'branch_id'=>$cabang,'created_at'=>DB::raw('now()')]);
        }



         if (empty($user[0])) {
            Flash::error('Petugas Kosong');
             return redirect(url('petugas'));
        }

        if ($request->photo != '') 
        {
              

            $file_gambar  =   $this->url->to(''.$user[0]->photo);  
            File::delete($file_gambar);


                    $images = base64_encode($request->photo);
                    $fileFormat = ".jpg";
                    $userPath = base64_encode(Auth::user()->email);
                    $dir = Config::get('elfinder.dir')[0];
                    $files = $dir."/".$userPath;
                    $fileName = base64_encode(Auth::user()->email . time());
                    $fullPath = $dir."/".$userPath."/".$fileName.$fileFormat;
                    File::makeDirectory($files, 0777, true, true);

                    if ($image = base64_decode($images, true))
                    {
                        $img = Image::make($image)->save($fullPath, 60);
                        $input['photo'] = $this->url->to('/'.$fullPath);
                    } 


         } 

          if($request->name != $user[0]->name)
         {
            DB::table('client')->where('id',$user[0]->client_id)->update(['name'=>$request->name]);
         } 

            

         $users = $this->userRepository->update($input, $id);

        

           Flash::success('Petugas Berhasi Diupdate');
           return redirect(url('petugas'));


    }


    public function destroy($id,Request $request)
    {

        $user = DB::table('users as a')->where('a.id',$id)->get();

        
         
        
        if (empty($user)) {
            Flash::error('Petugas Kosong');

             return redirect(url('petugas'));
        }
        
        
          if(!empty($user[0]->photo))
          {
            $file_gambar  =   $this->url->to(''.$user[0]->photo);  
            File::delete($file_gambar);
          }

    
        //$this->userRepository->delete($id);
          DB::table('client')->where('id',$user[0]->client_id)->delete();

        Flash::success('Petugas Berhasil Dihapus');
        return redirect(url('petugas'));

    }



  public function convert($tgl, $hari_tampil = true)
  {
    $bulan  = array("Jan"
            , "Feb"
            , "Mar"
            , "Apr"
            , "Mei"
            , "Jun"
            , "Jul"
            , "Agu"
            , "Sep"
            , "Okt"
            , "Nov"
            , "Des");
    $hari = array("Senin"
            , "Selasa"
            , "Rabu"
            , "Kamis"
            , "Jum'at"
            , "Sabtu"
            , "Minggu");
    $tahun_split  = substr($tgl, 0, 4);
    $bulan_split  = substr($tgl, 5, 2);
    $hari_split   = substr($tgl, 8, 2);
    $tmpstamp   = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
    $bulan_jadi   = $bulan[date("n", $tmpstamp)-1];
    $hari_jadi    = $hari[date("N", $tmpstamp)-1];
    if(!$hari_tampil)
    $hari_jadi="";
    return $hari_split." ".$bulan_jadi." ".$tahun_split;
  }


  public function created($tgl, $hari_tampil = true)
  {
    $bulan  = array("Jan"
            , "Feb"
            , "Mar"
            , "Apr"
            , "Mei"
            , "Jun"
            , "Jul"
            , "Agu"
            , "Sep"
            , "Okt"
            , "Nov"
            , "Des");
    $hari = array("Senin"
            , "Selasa"
            , "Rabu"
            , "Kamis"
            , "Jum'at"
            , "Sabtu"
            , "Minggu");
    $tahun_split  = substr($tgl, 0, 4);
    $bulan_split  = substr($tgl, 5, 2);
    $hari_split   = substr($tgl, 8, 2);
    $tmpstamp   = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
    $bulan_jadi   = $bulan[date("n", $tmpstamp)-1];
    $hari_jadi    = $hari[date("N", $tmpstamp)-1];
    if(!$hari_tampil)
    $hari_jadi="";
    return $hari_jadi." ".$hari_split." ".$bulan_jadi." ".$tahun_split;
  }

    // end dipakai fix

   

   
    
   

   


   

   


    

   

    

   

    public function bayar(request $request)
    {
          

        $bayar = $request->get('duit');
          $total = $request->get('total'); 

          $tr =  number_format($bayar,0,'','.');
          $tot =  number_format($total,0,'','.');
          
          $kembali = $bayar-$total;
          $count =  number_format($kembali,0,'','.');
          $content = '';

              if($bayar > $total)
              {
                    $content  .= '<td class="cde"> Bayar </td> <td class="pull-left"> : </td> <td class="fer"> <input id="id_bayar" class="form-control" type="text" value="'. $bayar.'" name="bayar" disabled></td>
                               
                                ';

             }else if($total == $bayar){
             
                     $content  .= '<td class="cde"> Bayar </td> <td class="pull-left"> : </td> <td class="fer"> <input id="id_bayar" class="form-control" type="text" value="'. $bayar.'" name="bayar" disabled></td>
                               
                                ';
              }else{
               
                      $content  .= '<td class="cde"> Bayar </td> <td class="pull-left"> : </td> <td class="fer"> <input id="id_bayar" class="form-control" type="text" value="0" name="bayar" disabled></td>
                                
                                ';

              }  

                return $content;
    }

     public function total(request $request)
    {
          

        $bayar = $request->get('duit');
          $total = $request->get('total'); 

          $tr =  number_format($bayar,0,'','.');
          $tot =  number_format($total,0,'','.');
          
          $kembali = $bayar-$total;
          $count =  number_format($kembali,0,'','.');
          $content = '';

              if($bayar > $total)
              {
                    $content  .= '<td class="cde"> Total </td> <td class="pull-left"> : </td> <td class="fer"> <input id="tb" class="form-control" type="text" value="'. $total.'" name="total" disabled></td>
                               
                                ';

             }else if($total == $bayar){
             
                     $content  .= '<td class="cde"> Total </td> <td class="pull-left"> : </td> <td class="fer"> <input id="tb" class="form-control" type="text" value="'. $total.'" name="total" disabled></td>
                               
                                ';
              }else{
               
                      $content  .= '<td class="cde"> Total </td> <td class="pull-left"> : </td> <td class="fer"> <input id="tb" class="form-control" type="text" value="0" name="total" disabled></td>
                                
                                ';

              }  

                return $content;
    }


    public function getBayar(request $request)
    {
          
       $login = Auth::user();
       $bayar = DB::table('transaction')->where(['user_id'=>$login->id,'status'=>'0'])->first()->pay;
       return $bayar;
      
    }


    public function getCostumer(request $request)
    {
          
       $login = Auth::user();
       $order = DB::table('transaction')->where(['user_id'=>$login->id,'status'=>'0'])->first()->costumer_id;
       $costumer = DB::table('users')->where(['id'=>$order])->first()->name;

       return $costumer;
      
    }

    public function getCostumerID(request $request)
    {
          
       $login = Auth::user();
       $costumer_id = DB::table('transaction')->where(['user_id'=>$login->id,'status'=>'0'])->first()->costumer_id;

       return $costumer_id;
      
    }


       public function getCategoryID(request $request)
    {
            $login = Auth::user();
            $order = Order::where(['user_id'=>$login->id])->get();
            $status = "";
            foreach ($order as $row) 
            {
              
              if($row['status'] =="0")
              {
                 $status = Order::where(['user_id'=>$login->id,'status'=>'0'])->groupBy('category_id')->first()->category_id;
              }

            } 
           
            return $status;
    }

    

    public function listcatalog(request $request)
    {  
        $id = $request->get('id');
        $category = DB::table('category')->where('id',$id)->first()->name;
        $this->postRepository->pushCriteria(new CatalogCriteria($request));
        $catalog = $this->postRepository->findByField('category_id',$id);
        $kategori = DB::table('category')->orderBy('id','desc')->get();


        

        return view('kasir.catalog.index')
        ->with(['category'=>$category,'id'=>$id,'catalog'=>$catalog,'kategori'=>$kategori]);
      
    }

    




    
   
}
