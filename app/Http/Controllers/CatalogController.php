<?php

namespace App\Http\Controllers;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PostRepository;

use App\Repositories\UserRepository;
use App\Repositories\ClientRepository;
use App\Repositories\BranchRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\PostCriteria;
use Response;
use Auth;
use App\User;
use App\Models\Device;
use Storage;
use QrCode;
use App\Models\Post;
use DB;

class CatalogController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;
    
    private $branchRepository;
    private $userRepository;
    private $clientRepository;
    protected $url;

    public function __construct(PostRepository $postRepo, BranchRepository $branchRepo,UserRepository $userRepo,ClientRepository $clientRepo,UrlGenerator $url)
    {
        $this->postRepository = $postRepo;
        
        $this->branchRepository = $branchRepo;
        $this->userRepository = $userRepo;
        $this->clientRepository = $clientRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return Response
     */
   

   
	
	public function category(Request $request)
    {

        $id = $request->get('id');
        $search = $request->get('search');
       

             if(empty($search))
             {
                $content = DB::table('category')->select('id','name','photo')->orderBy('id','desc')->get();
             }else{
                $content = DB::table('category')->select('id','name','photo')->where("name", "LIKE","%$search%")->orderBy('id','desc')->get();
             }   

               
       
        $return['data'] = $content;
      

        return $return;
    }

    public function catalog(Request $request)
    {

        $id = $request->get('id');
        $search = $request->get('search');
        if(empty($search))
        {    
            $content = DB::table('post as a')->join('stock as b','a.id','=','b.post_id')->select('a.category_id','a.title','a.price','a.id','b.status','a.type','a.description','a.discount')->where('a.category_id',$id)->orderBy('a.id','asc')->get();

        }else{
            $content = DB::table('post as a')->join('stock as b','a.id','=','b.post_id')->select('a.category_id','a.title','a.price','a.id','b.status','a.type','a.description','a.discount')->where('a.category_id',$id)->where("a.title", "LIKE","%$search%")->orderBy('a.id','asc')->get();
        }
         $title = DB::table('category')->where('id',$id)->select('name')->get();
        $kategori = DB::table('category')->select('id','name','photo')->orderBy('id','desc')->get();  

        $return['data'] = $content;
        $return['title'] = $title;
        $return['kategori'] = $kategori;   

        return $return;
    }
  


    
}
