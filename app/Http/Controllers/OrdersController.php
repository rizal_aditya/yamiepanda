<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use App\Role;
use App\User;
use App\Models\Category;
use App\Models\Post;
use App\Models\Order;
use Hash;
use QrCode;

class OrdersController extends AppBaseController
{
    /** @var  OrderRepository */
 

    public function __construct()
    {
        
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $user = Auth::user();
        if(empty($user->id))
        {
           return redirect(url('login'));
        }

        // $this->orderRepository->pushCriteria(new RequestCriteria($request));
        // $order = $this->orderRepository->all();

       

        $cabang = DB::table('loghistory as a')->select('b.name')->join('branch as b', 'a.branch_id', '=', 'b.id')->where('a.user_id',$user->id)->get();

       

        return view('orders.index')->with(['cabang'=>$cabang[0]->name]);
    }

	
}
