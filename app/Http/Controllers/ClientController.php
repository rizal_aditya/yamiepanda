<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateClientRequest;
use App\Http\Requests\CreateUserRequest;
use App\Repositories\ClientRepository;
use App\Repositories\UserRepository;
use App\Repositories\BranchRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\ClientCriteria;
use DB;
use Response;
use Auth;

class ClientController extends AppBaseController
{
    /** @var  ClientRepository */
    private $clientRepository;
    private $userRepository;

    public function __construct(ClientRepository $clientRepo,UserRepository $userRepo,BranchRepository $branchRepo)
    {
        $this->clientRepository = $clientRepo;
        $this->userRepository = $userRepo;
        $this->branchRepository = $branchRepo;

      } 

    /**
     * Display a listing of the Client.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

         
            $cari = $request->get('cari', null);
            //table Role
            $this->clientRepository->pushCriteria(new ClientCriteria($request));
            $this->clientRepository->pushCriteria(new RequestCriteria($request));
           
            $client = $this->clientRepository->paginate(10);  
          
             // search
             if ($cari!=null)
             {
              return redirect('client/?search=name:'.$cari.'&searchFields=name:like');
      
             }
          

        return view('client.index')
            ->with('client', $client);
    }

    /**
     * Show the form for creating a new Client.
     *
     * @return Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created Client in storage.
     *
     * @param CreateClientRequest $request
     *
     * @return Response
     */
    public function store(CreateClientRequest $request)
    {
        $input = $request->all();
        $data = $this->clientRepository->findByField('email',$input['email']);
         $type      = "server";
        $input['type'] = $type;
        if(empty($data)) {  

        Flash::success('Email already exists !!.');
        return redirect(route('client.index')); 

        }else{




       

        $client = $this->clientRepository->create($input);

        // create user
       
        $pass = "admin";
        $user_pass   = bcrypt($pass);
      
        $user = DB::table('users')
        ->insert(['name'=>$request->name,
            'client_id'=>$client->id,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'address'=>$request->address,
            'status'=>$request->status,
            'type'=>$type,'password'=>$user_pass,
            'created_at'=> new \DateTime()]);

        $dbID = DB::table('users')->where('name',$request->name)->first()->id;

        $role_user = DB::table('role_user')
        ->insert(['role_id'=>$client->role_id,'user_id'=>$dbID]);
       
        Flash::success('Client saved successfully.');

        return redirect(route('client.index'));

         } 
    }


    public function enable($id, Request $request)
    {
        $client = $this->clientRepository->findWithoutFail($id);
        $input = $request['status'];
        $email = $client->email;
        $kode = $client->id;
        $user = $this->userRepository->findByField(['client_id'=> $kode, 'email'=>$email]);


        $userid['client_id'] = $client['id'];
        $userid['name'] = $client['name'];
        $userid['email'] = $client['email'];
        $userid['phone'] = $client['phone'];
        $userid['address'] = $client['address'];
        $password = "layanacomputindo";
        $userid['password'] = bcrypt($password);
        
        if(!empty($user[0])) 
        {
            
          Flash::error('User already registered');
          return redirect(route('client.index')); 

        }else{

        
        
        $client = $this->clientRepository->update($request->all(), $id);
        $user   = $this->userRepository->create($userid);

        $br['user_id'] = $user->id;
        $br['client_id'] = $user->client_id;
        $br['name'] = $user->name;
        $br['email'] = $user->email;
        $br['phone'] = $user->phone;
        $br['address'] = $user->address;
        $br['type'] = 'center';
        $br['status'] = '1';

        $branch   = $this->branchRepository->create($br);
        Flash::success('Status enable and create user successfully.');

        return redirect(route('client.index'));

        }
    }  

    public function disable($id, Request $request)
    {
        $client = $this->clientRepository->findWithoutFail($id);
        $input = $request['status'];
        $email = $client->email;
        $kode = $client->id;

         $user = $this->userRepository->findByField(['client_id'=>$kode,'email'=>$email]);
         $user = $this->userRepository->update($request->all(), $user[0]->id);
         $client = $this->clientRepository->update($request->all(), $id);
         Flash::success('Status disable successfully.');
         return redirect(route('client.index'));

        
   
    }   

    /**
     * Display the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('client.index'));
        }

        return view('client.show')->with('client', $client);
    }

    /**
     * Show the form for editing the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('client.index'));
        }

        return view('client.edit')->with('client', $client);
    }

    /**
     * Update the specified Client in storage.
     *
     * @param  int              $id
     * @param UpdateClientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientRequest $request)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('client.index'));
        }

        $client = $this->clientRepository->update($request->all(), $id);

        Flash::success('Client updated successfully.');

        return redirect(route('client.index'));
    }

    /**
     * Remove the specified Client from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('client.index'));
        }

        $this->clientRepository->delete($id);

        Flash::success('Client deleted successfully.');

        return redirect(route('client.index'));
    }
}
