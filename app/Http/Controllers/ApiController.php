<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\UpdateApiRequest;
use App\Repositories\ApiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\ClientCriteria;


class ApiController extends AppBaseController
{
    /** @var  PersonRepository */
    private $apiRepository;

    public function __construct(ApiRepository $apiRepo)
    {
      $this->apiRepository = $apiRepo;   
    }

    /**
     * Display a listing of the Person.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
             $cari = $request->get('cari', null);
           
            $type = 'client';
            $this->apiRepository->pushCriteria(new ClientCriteria($type));  
            $this->apiRepository->pushCriteria(new RequestCriteria($request));
            $client = $this->apiRepository->paginate(10);

            // search
             if ($cari!=null)
             {
              return redirect('api_key/?search=name:'.$cari.'&searchFields=name:like');
      
             }

        return view('apikey.index')
            ->with('client', $client);
    }

    /**
     * Show the form for creating a new Person.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        $type = $request['type'];
        return view('person.create')
            ->with(['type'=> $type]);

    }

    /**
     * Store a newly created Person in storage.
     *
     * @param CreatePersonRequest $request
     *
     * @return Response
     */
    public function store(CreateClientRequest $request)
    {
        $input = $request->all();
        $type = $request['type'];
      

       

        $person = $this->personRepository->create($input);

        Flash::success('Person saved successfully.');
        return redirect(url('person?type='.$type));
        
    }

    /**
     * Display the specified Person.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
        $client = $this->apiRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('api_key.index'));
        }

        return view('apikey.show')->with('client', $client);
    }

    /**
     * Show the form for editing the specified Person.
     *
     * @param  int $id
     *
     * @return Response
     */


    public function edit($id,Request $request)
    {
       $client = $this->apiRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Client not found');

            return redirect(route('api_key.index'));
        }

        return view('apikey.edit')->with('client', $client);
        
    }

    /**
     * Update the specified Person in storage.
     *
     * @param  int              $id
     * @param UpdatePersonRequest $request
     *
     * @return Response
     */
    


    public function update($id, UpdateApiRequest $request)
    {
    
         $client = $this->apiRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Api Key not found');

            return redirect(route('api_key.index'));
        }

        $client = $this->apiRepository->update($request->all(), $id);

        Flash::success('Api Key updated successfully.');

        return redirect(route('api_key.index'));

    }

    /**
     * Remove the specified Person from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
         $client = $this->apiRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error('Api Key not found');

            return redirect(route('api_key.index'));
        }

        $this->apiRepository->delete($id);

        Flash::success('Api Key deleted successfully.');

        return redirect(route('api_key.index'));
    }
}
