<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use App\Criteria\UserCriteria;
use App\Criteria\UserAdminCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;

class LoginController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    private $roleUserRepository;
    
    public function __construct(UserRepository $userRepo,RoleUserRepository $roleUserRepo)
    {
        $this->userRepository = $userRepo;
        $this->roleUserRepository = $roleUserRepo;
        
    }

    public function index(Request $request)
    {

    	  return view('user.login');
           
    }

  



    public function check(Request $request)
    {

    	  $accsess = $request['email'];
          $endcode = strtr(base64_encode($accsess), '+/', '-_');

    	  return redirect(url('login?access='.$endcode));
    }


     public function auth(Request $request)
    {

            $accsess = $request['email'];
            $endcode = strtr(base64_encode($accsess), '+/', '-_');
            $email = base64_decode(strtr($endcode, '-_', '+/'));


            if(!empty($accsess))
             {   

            return redirect(url('login'))
            ->with('email', $email);

             }else{

                Flash::error('Email not found..!!');
                return redirect('access');

            }
          
    }




}
