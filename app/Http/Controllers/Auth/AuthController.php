<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Auth\CreatesUserProviders;
use App\Providers\CustomUserProvider;
use Illuminate\Http\Request;
use DB;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, CreatesUserProviders;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    protected function getCredentials(Request $request)
    {
        $branch_id = $request->branch_id;
        $email = $request->email;
        $password = $request->password;

        $user = DB::table('users')->where('email',$email)->get();
        $loghistory = DB::table('loghistory')->get();

        if(empty($loghistory))
        {

           DB::table('loghistory')->insert(['user_id'=>$user[0]->id,'branch_id'=>$branch_id,'created_at'=> new \DateTime()]);    
        }else{
             $logcheck = DB::table('loghistory')->where('user_id',$user[0]->id)->get();

             if(empty($logcheck))
             {
               DB::table('loghistory')->insert(['user_id'=>$user[0]->id,'branch_id'=>$branch_id,'created_at'=> new \DateTime()]);
             }else{


          
          if($logcheck[0]->user_id == $user[0]->id)
          {    
              DB::table('loghistory')->update(['branch_id'=>$branch_id,'created_at'=> new \DateTime()]);   
          }else{
            DB::table('loghistory')->insert(['user_id'=>$user[0]->id,'branch_id'=>$branch_id,'created_at'=> new \DateTime()]);
          }

          }
        }

        //echo $user[0]->id;    

        return $request->only('email', 'password');

       
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    protected function createDatabaseProvider($config)
    {
        $connection = $this->app['db']->connection();

        return new CustomUserProvider($connection, $this->app['hash'], $config['table']);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
