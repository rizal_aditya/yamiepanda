<?php

namespace App\Http\Controllers;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use App\Http\Requests;
use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateModalRequest;
use App\Repositories\PostRepository;
use App\Repositories\BranchRepository;

use App\Repositories\PointUserRepository;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\PostCriteria;
use Response;
use Auth;
use App\Models\PointUser;
use App\Models\Post;
use QrCode;
use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $postRepository;
    private $branchRepository;
    private $pointUserRepository;


    public function __construct(PostRepository $postRepo,BranchRepository $branchRepo,PointUserRepository $pointUserRepo)
    {
        $this->middleware('auth');
        $this->postRepository = $postRepo;
        $this->branchRepository = $branchRepo;
        $this->pointUserRepository = $pointUserRepo;
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {



 
     
        return view('home.home');
       
    }

      


 

   

  

}
