<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Routing\UrlGenerator;
use Config;
use File;


class CostumerController extends AppBaseController
{
    /** @var  OrderRepository */
 
    private $userRepository;
    protected $url;

    public function __construct(UserRepository $userRepo,UrlGenerator $url)
    {
         $this->userRepository = $userRepo;
         $this->url = $url;
    }


    public function index(Request $request)
   {

      $costumer = DB::table('users as a')->select('a.id','a.photo','a.name','a.email','a.phone','a.status','a.point','a.type')->where('type', '<>', 'petugas')->where('type', '<>', 'admin')->paginate(10);

       return view('costumer.index')->with(['costumer' => $costumer ]);


   }
   
   public function disable($id,Request $request)
   {
      
      DB::table('users')->where('id',$id)->update(['status'=>'0']);
       Flash::success('Costumer berhasil di nonaktifkan'); 
      return redirect(url('costumer')); 
   }

   public function enable($id,Request $request){

     
      DB::table('users')->where('id',$id)->update(['status'=>'1']);
         Flash::success('Costumer berhasil di aktifkan kembali');
          return redirect(url('costumer')); 
   }


    public function create(Request $request)
    {

           

    }

    public function store(CreateUserRequest $request)
    {
      
       
    }

    public function show($id,Request $request)
    {
      
        $user = DB::table('users as a')->select('a.id','a.address','a.photo','a.name','a.phone','a.email','a.religion','a.birthday','a.created_at','a.status')->where('a.id',$id)->get();

       


        if (empty($user[0])) {
            Flash::error('Costumer Kosong');
            return redirect(url('costumer'));  
        }

      
          return view('petugas.show')->with(['petugas'=> $user,'dates'=> $this->convert($user[0]->birthday),'create'=>$this->created($user[0]->created_at)]);
    }

    public function edit($id,Request $request)
    {

        

    }

    public function update($id, UpdateUserRequest $request)
    {

      


    }


    public function destroy($id,Request $request)
    {

       $user = $this->userRepository->findWithoutFail($id);
         
        
        if (empty($user)) {
            Flash::error('Costumer Kosong');

             return redirect(url('costumer'));
        }
        
        
          if(!empty($user->photo))
          {
            $file_gambar  =   $this->url->to(''.$user->photo);  
            File::delete($file_gambar);
          }

    
        $this->userRepository->delete($id);
        Flash::success('Costumer Berhasil Dihapus');
        return redirect(url('costumer'));

    }



  public function convert($tgl, $hari_tampil = true)
  {
    $bulan  = array("Jan"
            , "Feb"
            , "Mar"
            , "Apr"
            , "Mei"
            , "Jun"
            , "Jul"
            , "Agu"
            , "Sep"
            , "Okt"
            , "Nov"
            , "Des");
    $hari = array("Senin"
            , "Selasa"
            , "Rabu"
            , "Kamis"
            , "Jum'at"
            , "Sabtu"
            , "Minggu");
    $tahun_split  = substr($tgl, 0, 4);
    $bulan_split  = substr($tgl, 5, 2);
    $hari_split   = substr($tgl, 8, 2);
    $tmpstamp   = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
    $bulan_jadi   = $bulan[date("n", $tmpstamp)-1];
    $hari_jadi    = $hari[date("N", $tmpstamp)-1];
    if(!$hari_tampil)
    $hari_jadi="";
    return $hari_split." ".$bulan_jadi." ".$tahun_split;
  }


  public function created($tgl, $hari_tampil = true)
  {
    $bulan  = array("Jan"
            , "Feb"
            , "Mar"
            , "Apr"
            , "Mei"
            , "Jun"
            , "Jul"
            , "Agu"
            , "Sep"
            , "Okt"
            , "Nov"
            , "Des");
    $hari = array("Senin"
            , "Selasa"
            , "Rabu"
            , "Kamis"
            , "Jum'at"
            , "Sabtu"
            , "Minggu");
    $tahun_split  = substr($tgl, 0, 4);
    $bulan_split  = substr($tgl, 5, 2);
    $hari_split   = substr($tgl, 8, 2);
    $tmpstamp   = mktime(0, 0, 0, $bulan_split, $hari_split, $tahun_split);
    $bulan_jadi   = $bulan[date("n", $tmpstamp)-1];
    $hari_jadi    = $hari[date("N", $tmpstamp)-1];
    if(!$hari_tampil)
    $hari_jadi="";
    return $hari_jadi." ".$hari_split." ".$bulan_jadi." ".$tahun_split;
  }

    // end dipakai fix

   

   
    
   

   


   

   


    

   

    

   

    public function bayar(request $request)
    {
          

        $bayar = $request->get('duit');
          $total = $request->get('total'); 

          $tr =  number_format($bayar,0,'','.');
          $tot =  number_format($total,0,'','.');
          
          $kembali = $bayar-$total;
          $count =  number_format($kembali,0,'','.');
          $content = '';

              if($bayar > $total)
              {
                    $content  .= '<td class="cde"> Bayar </td> <td class="pull-left"> : </td> <td class="fer"> <input id="id_bayar" class="form-control" type="text" value="'. $bayar.'" name="bayar" disabled></td>
                               
                                ';

             }else if($total == $bayar){
             
                     $content  .= '<td class="cde"> Bayar </td> <td class="pull-left"> : </td> <td class="fer"> <input id="id_bayar" class="form-control" type="text" value="'. $bayar.'" name="bayar" disabled></td>
                               
                                ';
              }else{
               
                      $content  .= '<td class="cde"> Bayar </td> <td class="pull-left"> : </td> <td class="fer"> <input id="id_bayar" class="form-control" type="text" value="0" name="bayar" disabled></td>
                                
                                ';

              }  

                return $content;
    }

     public function total(request $request)
    {
          

        $bayar = $request->get('duit');
          $total = $request->get('total'); 

          $tr =  number_format($bayar,0,'','.');
          $tot =  number_format($total,0,'','.');
          
          $kembali = $bayar-$total;
          $count =  number_format($kembali,0,'','.');
          $content = '';

              if($bayar > $total)
              {
                    $content  .= '<td class="cde"> Total </td> <td class="pull-left"> : </td> <td class="fer"> <input id="tb" class="form-control" type="text" value="'. $total.'" name="total" disabled></td>
                               
                                ';

             }else if($total == $bayar){
             
                     $content  .= '<td class="cde"> Total </td> <td class="pull-left"> : </td> <td class="fer"> <input id="tb" class="form-control" type="text" value="'. $total.'" name="total" disabled></td>
                               
                                ';
              }else{
               
                      $content  .= '<td class="cde"> Total </td> <td class="pull-left"> : </td> <td class="fer"> <input id="tb" class="form-control" type="text" value="0" name="total" disabled></td>
                                
                                ';

              }  

                return $content;
    }


    public function getBayar(request $request)
    {
          
       $login = Auth::user();
       $bayar = DB::table('transaction')->where(['user_id'=>$login->id,'status'=>'0'])->first()->pay;
       return $bayar;
      
    }


    public function getCostumer(request $request)
    {
          
       $login = Auth::user();
       $order = DB::table('transaction')->where(['user_id'=>$login->id,'status'=>'0'])->first()->costumer_id;
       $costumer = DB::table('users')->where(['id'=>$order])->first()->name;

       return $costumer;
      
    }

    public function getCostumerID(request $request)
    {
          
       $login = Auth::user();
       $costumer_id = DB::table('transaction')->where(['user_id'=>$login->id,'status'=>'0'])->first()->costumer_id;

       return $costumer_id;
      
    }


       public function getCategoryID(request $request)
    {
            $login = Auth::user();
            $order = Order::where(['user_id'=>$login->id])->get();
            $status = "";
            foreach ($order as $row) 
            {
              
              if($row['status'] =="0")
              {
                 $status = Order::where(['user_id'=>$login->id,'status'=>'0'])->groupBy('category_id')->first()->category_id;
              }

            } 
           
            return $status;
    }

    

    public function listcatalog(request $request)
    {  
        $id = $request->get('id');
        $category = DB::table('category')->where('id',$id)->first()->name;
        $this->postRepository->pushCriteria(new CatalogCriteria($request));
        $catalog = $this->postRepository->findByField('category_id',$id);
        $kategori = DB::table('category')->orderBy('id','desc')->get();


        

        return view('kasir.catalog.index')
        ->with(['category'=>$category,'id'=>$id,'catalog'=>$catalog,'kategori'=>$kategori]);
      
    }

    




    
   
}
