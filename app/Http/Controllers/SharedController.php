<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSharedRequest;
use App\Http\Requests\UpdateSharedRequest;
use App\Repositories\SharedRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Shared;
use DB;

class SharedController extends AppBaseController
{
    /** @var  SharedRepository */
    private $sharedRepository;

    public function __construct(SharedRepository $sharedRepo)
    {
        $this->sharedRepository = $sharedRepo;
    }

    /**
     * Display a listing of the Shared.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->sharedRepository->pushCriteria(new RequestCriteria($request));
        $shared = $this->sharedRepository->all();

        return view('shared.index')
            ->with('shared', $shared);
    }

    /**
     * Show the form for creating a new Shared.
     *
     * @return Response
     */
    public function create()
    {
        return view('shared.create');
    }

    /**
     * Store a newly created Shared in storage.
     *
     * @param CreateSharedRequest $request
     *
     * @return Response
     */
    public function store(CreateSharedRequest $request)
    {
        $input = $request->all();

        $shared = $this->sharedRepository->create($input);

        Flash::success('Shared saved successfully.');

        return redirect(route('shared.index'));
    }

    public function send(Request $request)
    {

        DB::table('shared')->update(['status'=>'0']);
        $id = $request->id;
        $status = $request->status;
        DB::table('shared')->where('id',$id)->update(['status'=>$status]);
           

        
    }

    /**
     * Display the specified Shared.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $shared = $this->sharedRepository->findWithoutFail($id);

        if (empty($shared)) {
            Flash::error('Shared not found');

            return redirect(route('shared.index'));
        }

        return view('shared.show')->with('shared', $shared);
    }

    /**
     * Show the form for editing the specified Shared.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $shared = $this->sharedRepository->findWithoutFail($id);

        if (empty($shared)) {
            Flash::error('Shared not found');

            return redirect(route('shared.index'));
        }

        return view('shared.edit')->with('shared', $shared);
    }

    /**
     * Update the specified Shared in storage.
     *
     * @param  int              $id
     * @param UpdateSharedRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSharedRequest $request)
    {
        $shared = $this->sharedRepository->findWithoutFail($id);

        if (empty($shared)) {
            Flash::error('Shared not found');

            return redirect(route('shared.index'));
        }

        $shared = $this->sharedRepository->update($request->all(), $id);

        Flash::success('Shared updated successfully.');

        return redirect(route('shared.index'));
    }

    /**
     * Remove the specified Shared from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $shared = $this->sharedRepository->findWithoutFail($id);

        if (empty($shared)) {
            Flash::error('Shared not found');

            return redirect(route('shared.index'));
        }

        $this->sharedRepository->delete($id);

        Flash::success('Shared deleted successfully.');

        return redirect(route('shared.index'));
    }
}
