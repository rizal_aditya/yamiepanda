<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePointUserRequest;
use App\Http\Requests\UpdatePointUserRequest;
use App\Repositories\PointUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PointUserController extends AppBaseController
{
    /** @var  PointUserRepository */
    private $pointUserRepository;

    public function __construct(PointUserRepository $pointUserRepo)
    {
        $this->pointUserRepository = $pointUserRepo;
    }

    /**
     * Display a listing of the PointUser.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pointUserRepository->pushCriteria(new RequestCriteria($request));
        $pointUser = $this->pointUserRepository->all();

        return view('pointUser.index')
            ->with('pointUser', $pointUser);
    }

    /**
     * Show the form for creating a new PointUser.
     *
     * @return Response
     */
    public function create()
    {
        return view('pointUser.create');
    }

    /**
     * Store a newly created PointUser in storage.
     *
     * @param CreatePointUserRequest $request
     *
     * @return Response
     */
    public function store(CreatePointUserRequest $request)
    {
        $input = $request->all();

        $pointUser = $this->pointUserRepository->create($input);

        Flash::success('PointUser saved successfully.');

        return redirect(route('pointUser.index'));
    }

    /**
     * Display the specified PointUser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pointUser = $this->pointUserRepository->findWithoutFail($id);

        if (empty($pointUser)) {
            Flash::error('PointUser not found');

            return redirect(route('pointUser.index'));
        }

        return view('pointUser.show')->with('pointUser', $pointUser);
    }

    /**
     * Show the form for editing the specified PointUser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pointUser = $this->pointUserRepository->findWithoutFail($id);

        if (empty($pointUser)) {
            Flash::error('PointUser not found');

            return redirect(route('pointUser.index'));
        }

        return view('pointUser.edit')->with('pointUser', $pointUser);
    }

    /**
     * Update the specified PointUser in storage.
     *
     * @param  int              $id
     * @param UpdatePointUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePointUserRequest $request)
    {
        $pointUser = $this->pointUserRepository->findWithoutFail($id);

        if (empty($pointUser)) {
            Flash::error('PointUser not found');

            return redirect(route('pointUser.index'));
        }

        $pointUser = $this->pointUserRepository->update($request->all(), $id);

        Flash::success('PointUser updated successfully.');

        return redirect(route('pointUser.index'));
    }

    /**
     * Remove the specified PointUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pointUser = $this->pointUserRepository->findWithoutFail($id);

        if (empty($pointUser)) {
            Flash::error('PointUser not found');

            return redirect(route('pointUser.index'));
        }

        $this->pointUserRepository->delete($id);

        Flash::success('PointUser deleted successfully.');

        return redirect(route('pointUser.index'));
    }
}
