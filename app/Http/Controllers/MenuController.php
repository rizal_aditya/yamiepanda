<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Repositories\MenuRepository;
use App\Repositories\RoleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\MenuCriteria;
use Response;
use Auth;
class MenuController extends AppBaseController
{
    /** @var  MenuRepository */
    private $menuRepository;
    private $roleRepository;

    public function __construct(MenuRepository $menuRepo, RoleRepository $roleRepo)
    {
        $this->menuRepository = $menuRepo;
        $this->roleRepository = $roleRepo;
    }

    /**
     * Display a listing of the Menu.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
      

         $cari = $request->get('cari', null);
         $type = $request['type'];
         $user = Auth::user();

              
            $this->menuRepository->pushCriteria(new MenuCriteria($type));
           

            $this->menuRepository->pushCriteria(new RequestCriteria($request));
            $menu = $this->menuRepository->paginate(10);

            // search
             if ($cari!=null)
             {
              return redirect('menu/?type='.$type.'&search=name:'.$cari.'&searchFields=name:like');
      
             }



        return view('menu.index')
            ->with(['menu' => $menu,'type'=>$type]);
    }

    /**
     * Show the form for creating a new Menu.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $type = $request['type'];
        $roles = $this->roleRepository->findByField('name',$type);
        $id =   $roles[0]->id;

        return view('menu.create')
            ->with(['type'=> $type,'id'=>$id]);
    }

    /**
     * Store a newly created Menu in storage.
     *
     * @param CreateMenuRequest $request
     *
     * @return Response
     */
    public function store(CreateMenuRequest $request)
    {
    
        $input = $request->all();
        $id = $request['role_id'];
        $roles = $this->roleRepository->findWithoutFail($id);
        $type = $roles->name;

        $menu = $this->menuRepository->create($input);
        Flash::success('Menu saved successfully.');
        return redirect(url('menu?type='.$type));
    }

    /**
     * Display the specified Menu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
        $menu = $this->menuRepository->findWithoutFail($id);
        $type = $request['type'];
        $roles = $this->roleRepository->findByField('name',$type);
        $id =   $roles[0]->id;

        if (empty($menu)) {
            Flash::error('Menu not found');

            return redirect(url('menu?type='.$type));

        }

         return view('menu.show')->with(['menu'=> $menu, 'type' => $type]);
    }

    /**
     * Show the form for editing the specified Menu.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,Request $request)
    {
        $menu = $this->menuRepository->findWithoutFail($id);

        $type = $request['type'];
        $roles = $this->roleRepository->findByField('name',$type);
        $id =   $roles[0]->id;
       

        if (empty($menu)) {
            Flash::error('Menu not found');

             return view('menu.edit')->with(['menu'=> $menu, 'type' => $type,'id'=>$id]);
        }

        return view('menu.edit')->with(['menu'=> $menu, 'type' => $type,'id'=>$id]);
    }

    /**
     * Update the specified Menu in storage.
     *
     * @param  int              $id
     * @param UpdateMenuRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuRequest $request)
    {
         
        $menu = $this->menuRepository->findWithoutFail($id);
        $kode = $menu->role_id;
        
        $roles = $this->roleRepository->findWithoutFail($kode);
        $type =  $roles->name;

        if (empty($menu)) {
            Flash::error('Menu not found');

            return redirect(url('menu?type='.$type));
        }

        $menu = $this->menuRepository->update($request->all(), $id);
        Flash::success('Menu updated successfully.');

        return redirect(url('menu?type='.$type));
    }

    /**
     * Remove the specified Menu from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $menu = $this->menuRepository->findWithoutFail($id);
        $kode = $menu->role_id;
        
        $roles = $this->roleRepository->findWithoutFail($kode);
        $type =  $roles->name;

        if (empty($menu)) {
            Flash::error('Menu not found');

            return redirect(url('menu?type='.$type));
        }

        $this->menuRepository->delete($id);

        Flash::success('Menu deleted successfully.');

        return redirect(url('menu?type='.$type));
    }
}
