<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateBranchRequest;
use App\Http\Requests\UpdateBranchRequest;
use App\Repositories\BranchRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\BranchCriteria;
use Response;
use Auth;
use Illuminate\Routing\UrlGenerator;

class BranchController extends AppBaseController
{
    /** @var  BranchRepository */
    private $branchRepository;

    public function __construct(BranchRepository $branchRepo,UrlGenerator $url)
    {
        $this->branchRepository = $branchRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Branch.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

            $user = Auth::user();
            $cari = $request->get('cari', null);

            $user = Auth::user();
        if(empty($user->id))
        {
           return redirect(url('login'));
        }
                //table Role
            $user_id = $user->id;
            $this->branchRepository->pushCriteria(new BranchCriteria($user_id));
            $this->branchRepository->pushCriteria(new RequestCriteria($request)); 
            $branch = $this->branchRepository->paginate(10);

            // search
             if ($cari!=null)
             {
              return redirect('branch/?search=name:'.$cari.'&searchFields=name:like');
      
             }


        return view('branch.index')
            ->with('branch', $branch);
    }

    /**
     * Show the form for creating a new Branch.
     *
     * @return Response
     */
    public function create()
    {

       

        return view('branch.create');
        
        

        
    }

    /**
     * Store a newly created Branch in storage.
     *
     * @param CreateBranchRequest $request
     *
     * @return Response
     */
    public function store(CreateBranchRequest $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $input = $request->all();
        $input['user_id'] = $user_id;
        $branch = $this->branchRepository->create($input);

        Flash::success('Cabang berhasil disimpan.');

        return redirect(route('cabang.index'));
    }

    /**
     * Display the specified Branch.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $branch = $this->branchRepository->findWithoutFail($id);

        if (empty($branch)) {
            Flash::error('Cabang Kosong');

            return redirect(route('cabang.index'));
        }

        return view('branch.show')->with('branch', $branch);
    }

    /**
     * Show the form for editing the specified Branch.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $branch = $this->branchRepository->findWithoutFail($id);

        if (empty($branch)) {
            Flash::error('Cabang Kosong');

            return redirect(route('branch.index'));
        }

        return view('branch.edit')->with('branch', $branch);
    }

    /**
     * Update the specified Branch in storage.
     *
     * @param  int              $id
     * @param UpdateBranchRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBranchRequest $request)
    {
        $branch = $this->branchRepository->findWithoutFail($id);

        if (empty($branch)) {
            Flash::error('Branch not found');

            return redirect(route('cabang.index'));
        }

        $branch = $this->branchRepository->update($request->all(), $id);

        Flash::success('Cabang berhasil di update.');

       return redirect(route('cabang.index'));
    }

    /**
     * Remove the specified Branch from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $branch = $this->branchRepository->findWithoutFail($id);

        if (empty($branch)) {
            Flash::error('Cabang Kosong');

            return redirect(route('cabang.index'));
        }

        $this->branchRepository->delete($id);

        Flash::success('Cabang berhasil dihapus.');

        return redirect(route('cabang.index'));
    }
}
