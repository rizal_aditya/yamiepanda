<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\BranchRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use App\Role;

class ContactController extends AppBaseController
{
   /** @var  BranchRepository */
    private $branchRepository;

    public function __construct(BranchRepository $branchRepo)
    {
        $this->branchRepository = $branchRepo;
    }

    /**
     * Display a listing of the Branch.
     *
     * @param Request $request
     * @return Response
     */

    public function index(Request $request)
    {
    	$this->branchRepository->pushCriteria(new RequestCriteria($request));
    	$contact = $this->branchRepository->findByField('user_id', Role::where('name','admin')->first()->id);
    	return view('contact.index')
            ->with('contact', $contact);

    }	


}
