<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Repositories\MenuRepository;
use App\Repositories\PostRepository;
use App\Repositories\AssetRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
class FrontendController extends AppBaseController
{
    private $postRepository;
    private $menuRepository;
    private $assetRepository;
    
    public function __construct(PostRepository $postRepo,MenuRepository $menuRepo, AssetRepository $assetRepo)
    {
         $this->postRepository = $postRepo;
         $this->menuRepository = $menuRepo;
         $this->assetRepository = $assetRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $frontend = $this->postRepository->all();
       $menu = $this->menuRepository->all();
       $asset = $this->assetRepository->all();

        $user = Auth::user();

        if(Auth::check())
        {
             return redirect('home');
        }
        

       return view('frontend.index')
            ->with(['frontend' => $frontend,'menu'=> $menu,'asset'=>$asset]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
