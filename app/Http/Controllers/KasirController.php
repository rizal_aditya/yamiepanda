<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Repositories\OrderRepository;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use App\Repositories\RoleUserRepository;
use App\Repositories\RoleRepository;
use App\Repositories\ClientRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use App\Role;
use App\User;
use App\Models\Category;
use App\Models\Post;
use App\Models\Order;
use Hash;
use QrCode;

class KasirController extends AppBaseController
{
    /** @var  OrderRepository */
    private $orderRepository;
    private $postRepository;
    private $userRepository;
    private $roleUserRepository;
    private $roleRepository;
    private $clientRepository;

    public function __construct(OrderRepository $orderRepo,PostRepository $postRepo,UserRepository $userRepo,RoleUserRepository $roleUserRepo,RoleRepository $roleRepo,ClientRepository $clientRepo)
    {
        $this->orderRepository = $orderRepo;
        $this->postRepository = $postRepo;
        $this->userRepository = $userRepo;
        $this->roleUserRepository = $roleUserRepo;
        $this->roleRepository = $roleRepo;
        $this->clientRepository = $clientRepo;
    }

    /**
     * Display a listing of the Order.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->orderRepository->pushCriteria(new RequestCriteria($request));
        $order = $this->orderRepository->all();

        return view('kasir.index')
            ->with('order', $order);
    }




    /**
     * Show the form for creating a new Order.
     *
     * @return Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created Order in storage.
     *
     * @param CreateOrderRequest $request
     *
     * @return Response
     */
  

    public function store(CreateOrderRequest $request)
    {
        
    }


   

    /**
     * Display the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified Order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('order.index'));
        }

        return view('order.edit')->with('order', $order);
    }

    /**
     * Update the specified Order in storage.
     *
     * @param  int              $id
     * @param UpdateOrderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOrderRequest $request)
    {
        $order = $this->orderRepository->findWithoutFail($id);

        if (empty($order)) {
            Flash::error('Order not found');

            return redirect(route('order.index'));
        }

        $order = $this->orderRepository->update($request->all(), $id);

        Flash::success('Order updated successfully.');

        return redirect(route('order.index'));
    }

    /**
     * Remove the specified Order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
          
    }
}
