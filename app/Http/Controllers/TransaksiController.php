<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use App\Repositories\TransactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Auth;
use DB;

class TransaksiController extends AppBaseController
{
    /** @var  TransactionRepository */
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepo)
    {
        $this->transactionRepository = $transactionRepo;
    }

    /**
     * Display a listing of the Transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       
    }

    public function createorder(Request $request)
    {
          

        $user = Auth::user();
        $post_id = $request->get('post_id');
        $costumer_id = $request->get('costumer_id');
        $meja_id = $request->get('meja_id');
        $kode = $request->get('kode');
        $status = "0";
        $qty = "1";

         $date = date('Y-m-d');

        $transaksi = DB::table('transaction')->where(['post_id'=>$post_id,'costumer_id'=>$costumer_id,'user_id'=>$user->id,'status'=>'0'])->get();

          $keranjang = DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'status'=>'0'])->get();

        $produk = DB::table('post as b')->select('b.id as post_id','b.title','b.point','b.discount','b.price','b.jenis')->where(['b.id'=>$post_id])->get();

    

             $diskon = $produk[0]->discount;

             if($diskon !=="0")
             {
               $hitung1 =  $produk[0]->price*$diskon;
               $hitung2 =  $hitung1/100;
               $total  =   $produk[0]->price-$hitung2;

             }else{

              $total = $produk[0]->price; 

             }   
                
             $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
               

             DB::table('transaction')->insert(['branch_id'=>$cabang[0]->branch_id,'keranjang_id'=>$keranjang[0]->id,'user_id'=>$user->id,'order_id'=>$keranjang[0]->order_id,'discount'=> $produk[0]->discount,'costumer_id'=>$costumer_id,'post_id'=> $produk[0]->post_id,'price'=> $produk[0]->price,'qty'=>$qty,'type'=>$kode,'jenis'=>$produk[0]->jenis,'point'=> $produk[0]->point,'status'=>'0','created_at'=> new \DateTime()]);

            


                


    }




    public function member(Request $request)
    {
          
         $id = $request->get('id');
         $user = Auth::user();
         $meja_id = $request->get('meja_id');
         $kode = sprintf("%06d", mt_rand(1, 999999));

          $latest = DB::table('keranjang')->max('order_id');
                
                if (empty($latest)) { 
                    $order_id = '1000001';
                }else{
                    if($latest =="0")
                     {
                        $order_id = '1000001';
                     }else{

                        $order_id = $latest+1;  
                     } 
                   
                }

           $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
                 
         DB::table('keranjang')->insert(['branch_id'=>$cabang[0]->branch_id,'user_id'=>$user->id,'costumer_id'=>$id,'order_id'=>$order_id,'meja_id'=>'0','created_at'=> DB::raw('now()')]);
         $transaksi = DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$id])->get();

          $member = DB::table('users as a')->select('b.order_id','b.id as keranjang_id','a.id as costumer_id','a.name','b.meja_id')
         ->join('keranjang as b', 'a.id', '=', 'b.costumer_id')->where(['a.id'=>$transaksi[0]->costumer_id,'b.status'=>'0'])->get();

         return $member;
    }


  


    public function memberview(Request $request)
    {
          
         
         $user = Auth::user();
         $costumer_id = $request->get('costumer_id');
       
                 $member = DB::table('users as a')->select('a.id','a.name','b.meja_id')
         ->join('keranjang as b', 'a.id', '=', 'b.costumer_id')->where('a.id',$costumer_id)->get();
       
         return $member;
    }

    public function delorderm(Request $request)
    {
         $user = Auth::user();
         $order_id = $request->get('order_id');
         $transaksi = DB::table('keranjang')->where(['user_id'=>$user->id,'order_id'=>$order_id])->get();

         if(!empty($transaksi))
         {
            
            DB::table('keranjang')->where(['user_id'=>$user->id,'order_id'=>$order_id])->delete();
           
         }   


    }


    public function simpandraf(Request $request)
    {
        $user = Auth::user();
         $order_id = $request->get('order_id');
         $costumer_id = $request->get('costumer_id'); 
         $total = $request->get('total'); 
         DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['status'=>'3','total'=>$total]); 
        DB::table('transaction')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['status'=>'3']); 


    }

     public function cekmember(Request $request)
    {

        $user = Auth::user(); 
        $costumer_id = $request->get('id');   

          $costumer = DB::table('keranjang')->select('costumer_id')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'status'=>'3'])->get();



         
          $return['costumer'] =  $costumer;

        return $return; 
    }

     public function detailitem(Request $request)
    {

        $user = Auth::user(); 
        $order_id = $request->get('order_id');   
        $detail = DB::table('transaction as a')->select(DB::raw('SUM(a.qty) as jml'),'a.order_id','b.title as item','b.price',DB::raw('SUM(a.price) as subtotal'))->join('post as b','a.post_id','=','b.id')->where(['a.order_id'=>$order_id,'a.user_id'=>$user->id])->get();

          $total = DB::table('keranjang')->select('total')->where('order_id',$order_id)->get();
          $return['detail'] = $detail;
          $return['total'] = $total;

        return $return; 
    }


      public function updatemeja(Request $request)
      {
        $user = Auth::user(); 
        $meja_id = $request->get('meja_id');
        $costumer_id = $request->get('costumer_id'); 
        $order_id = $request->get('order_id'); 
        DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['meja_id'=>$meja_id]);   

        $meja = DB::table('keranjang')->select('meja_id')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id])->get();
         $return["meja"] = $meja;

         return  $return;
     }



      public function produk(Request $request)
      {
        $user = Auth::user(); 
        $id = $request->get('id');
        $costumer_id = $request->get('costumer_id');    

        DB::table('transaction')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id])->update(['meja_id'=>$id]);      
     }


     public function keranjang(Request $request)
     {

        $user = Auth::user(); 
        if(empty($user->id))
        {
             return redirect(url('login'));
        }


        $proses = DB::table('keranjang as a')->select('a.id','a.order_id','a.costumer_id','b.name as costumer_name','a.meja_id','a.point','a.bayar','a.bank','a.kembali','a.total','a.status','a.type')->join('users as b', 'a.costumer_id', '=', 'b.id')->where(['a.user_id'=>$user->id,'a.status'=>0])->get();


        $print = DB::table('keranjang as a')->select('a.id','a.order_id','a.costumer_id','b.name as costumer_name','a.meja_id','a.point','a.bayar','a.bank','a.kembali','a.total','a.status','a.type')->join('users as b', 'a.costumer_id', '=', 'b.id')->where(['a.user_id'=>$user->id,'a.status'=>1])->get();

        $prosesjml = DB::table('transaction as a')->where(['a.user_id'=>$user->id,'a.status'=>0])->groupBy('a.post_id')->count();

       $itemproses = DB::table('transaction as a')->select(DB::raw('SUM(a.qty) as jml'), DB::raw('SUM(a.price) as subtotal'),DB::raw('SUM(a.discount) as diskon'),'a.id','a.costumer_id','a.noted','b.category_id','a.point','a.post_id','b.title','a.price','a.order_id','a.status')->join('post as b', 'a.post_id', '=', 'b.id')->join('category as c', 'b.category_id', '=', 'c.id')->where(['a.user_id'=>$user->id,'a.status'=>0])->groupBy('a.post_id')->get();

      


        $itemprint = DB::table('transaction as a')->select(DB::raw('SUM(a.qty) as jml'), DB::raw('SUM(a.price) as subtotal'),DB::raw('SUM(a.discount) as diskon'),'a.id','a.costumer_id','a.noted','b.category_id','a.point','a.post_id','b.title','a.price','a.order_id','a.status')->join('post as b', 'a.post_id', '=', 'b.id')->join('category as c', 'b.category_id', '=', 'c.id')->where(['a.user_id'=>$user->id,'a.status'=>1])->groupBy('a.post_id')->get();



        if(!empty($itemproses[0]->order_id))
         {

        $total = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$itemproses[0]->order_id])->sum('price');

        $free = DB::table('transaction')->where(['type'=>'gratis','order_id'=>$itemproses[0]->order_id])->sum('price');


       $point = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$itemproses[0]->order_id])->sum('point');
       }


         if(!empty($itemprint[0]->order_id))
         {

        $total = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$itemprint[0]->order_id])->sum('price');
       $point = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$itemprint[0]->order_id])->sum('point');
       $free = DB::table('transaction')->where(['type'=>'gratis','order_id'=>$itemprint[0]->order_id])->sum('price');
       }

       if(empty($itemproses[0]->order_id))
       {

          if(empty($itemprint[0]->order_id))
          {

            $total = "";
            $point = "";
            $free = "";

          }else{

              $total = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$itemprint[0]->order_id])->sum('price');
       $point = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$itemprint[0]->order_id])->sum('point');
       $free = DB::table('transaction')->where(['type'=>'gratis','order_id'=>$itemprint[0]->order_id])->sum('price');
          }  

          
       } 


          $rekap = DB::table('transaction')->where(['qty'=>'0'])->get(); 
          if(!empty($rekap))
          {

              DB::table('transaction')->where(['qty'=>'0'])->delete();
          }  
       
      

        $return["proses"] = $proses;
        $return["print"] = $print;
        $return["itemproses"] =  $itemproses;
        $return["itemprint"] = $itemprint;
        $return["total"] = $total;
        $return["point"] = $point;
        $return['free']  = $free;
        return $return;  
              
     }

    

     public function total(Request $request)
     {

       $user = Auth::user(); 
       $order_id = $request->get('order_id');
       $total = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$order_id])->sum('total');
       $point = DB::table('transaction')->where(['user_id'=>$user->id,'order_id'=>$order_id])->sum('point');
       
       $return["total"] = $total;
       $return["point"] = $point;
      

      return $return; 
     }


      public function plusorder(Request $request)
    {


       $user = Auth::user(); 
       $id = $request->get('id');
       $kode = $request->get('kode');
       $order =  DB::table('transaction')->where('id',$id)->get();
       $qty = "1";

               $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();

             DB::table('transaction')
             ->insert(['branch_id'=>$cabang[0]->branch_id,'keranjang_id'=>$order[0]->keranjang_id,'user_id'=>$user->id,'order_id'=>$order[0]->order_id,'discount'=> $order[0]->discount,'costumer_id'=>$order[0]->costumer_id,'post_id'=> $order[0]->post_id,'price'=> $order[0]->price,'qty'=>$qty,'type'=>$kode,'jenis'=>$order[0]->jenis,'point'=> $order[0]->point,'status'=>'0','created_at'=> new \DateTime()]);

            


        
       
        
    }

      public function minusorder(Request $request)
    {
        $user = Auth::user(); 
        $id = $request->get('id');

        DB::table('transaction')->where(['id'=>$id,'user_id'=>$user->id])->delete();

     
        
    }

    public function delorder(Request $request)
    {

          $user = Auth::user(); 
          $post_id = $request->get('post_id');
          $type = $request->get('type');

          if($type == "1")
          {

            $kategori = "berbayar";
             DB::table('transaction')->where(['post_id'=>$post_id,'type'=>$kategori])->delete();
          }else if($type == "1"){
            $kategori = "gratis";
             DB::table('transaction')->where(['post_id'=>$post_id,'type'=>$kategori])->delete();
          }else{
             DB::table('transaction')->where(['post_id'=>$post_id])->delete();
          }  

    }

    public function orderbaru(Request $request)
    {
         $user = Auth::user();
        $costumer_id = $request->get('costumer_id');  
         DB::table('keranjang')->where(['user_id'=>$user->id ,'costumer_id'=> $costumer_id])->update(['status'=>'2']);
          DB::table('transaction')->where(['user_id'=>$user->id ,'costumer_id'=> $costumer_id])->update(['status'=>'2']);

    }

    /**
     * Store a newly created Transaction in storage.
     *
     * @param CreateTransactionRequest $request
     *
     * @return Response
     */
    public function detailproduk(Request $request)
    {
       $user = Auth::user();  
       $post_id = $request->get('post_id');

       $produk = DB::table('post as a')->select('a.title','b.order_id','b.noted','b.post_id')->join('transaction as b', 'a.id', '=', 'b.post_id')->where(['a.id'=>$post_id,'b.user_id'=>$user->id])->get();
        return $produk;
       
       
    }
   
    public function printdata(Request $request)
    {

        $user = Auth::user();  
        $costumer_id = $request->get('costumer_id');
        $order_id = $request->get('order_id');
        $kode = $request->get('kode');



        $petugas = DB::table('loghistory as a')->select('b.name as kasir','c.name as branch','c.description','c.telp','a.created_at as tgl_order')->join('users as b', 'a.user_id', '=', 'b.id')->join('branch as c', 'a.branch_id', '=', 'c.id')->where('a.user_id',$user->id)->get();
        
        $costumer =  DB::table('keranjang as a')->select('a.total','a.bayar','a.kembali','a.point','b.phone','a.meja_id','a.order_id','b.name as costumer_name','a.created_at as tgl_order','c.name as petugas')->join('users as b', 'a.costumer_id', '=', 'b.id')->join('users as c', 'a.user_id', '=', 'c.id')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->get();

 $keranjang = DB::table('transaction as a')->select(DB::raw('SUM(a.qty) as jml'),'a.post_id','b.title as produk','a.price','a.discount',DB::raw('SUM(a.price) as subtotal'))->join('post as b', 'a.post_id', '=', 'b.id')->where(['a.order_id'=>$order_id])->groupBy('a.post_id')->get();
      

    if($kode=="1")
    {

     

      $free = DB::table('transaction')->where(['post_id'=>$keranjang[0]->post_id,'order_id'=>$order_id,'type'=>'gratis','status'=>1])->sum('price');

      $discount = DB::table('transaction as a')->where(['a.order_id'=>$order_id,'a.status'=>1])->sum('discount');

    } else{

    
      $free = "";
      $discount ="";
    } 

     
        $return['costumer'] = $costumer;
        $return['keranjang'] = $keranjang;
        $return['petugas']  = $petugas;
        $return['free'] = array('gratis' =>$free);
        $return['discount'] = array('discount' =>$discount);
        return $return;

    }




     public function printlap(Request $request)
    {

        $user = Auth::user();  
       

        $petugas = DB::table('loghistory as a')->select('b.name as kasir','c.name as branch','c.description','c.telp','a.created_at as tgl_order')->join('users as b', 'a.user_id', '=', 'b.id')->join('branch as c', 'a.branch_id', '=', 'c.id')->where('a.user_id',$user->id)->get();

        $date = date("Y-m-d");
        
        $costumer =  DB::table('keranjang as a')->select('a.total','a.bayar','a.kembali','a.point','b.phone','a.meja_id','a.order_id','b.name as costumer_name','a.created_at as tgl_order','c.name as petugas')->join('users as b', 'a.costumer_id', '=', 'b.id')->join('users as c', 'a.user_id', '=', 'c.id')->where('a.created_at', 'like', ''.$date.'%')->get();


         $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
     
       $aosjml =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $aostotal =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');

      $tunaijml =  DB::table('keranjang')->where(['type'=>'tunai','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count('id');
      
      $tunaitotal =  DB::table('keranjang')->where(['type'=>'tunai','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');

       $debitjml =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count('id');
      
        $re =  DB::table('keranjang')->select('bank')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->get();


        for($i=0; $i<$debitjml; $i++)
        {



          $res[$i]['data'] = DB::table('keranjang')->select('bank','total')->where(['type'=>'debit','bank'=>$re[$i]->bank,'status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->get();
          $res[$i]['jml'] = DB::table('keranjang')->where(['type'=>'debit','bank'=>$re[$i]->bank,'status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count();

         
        }  

      
       
       

      
       $debittot =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');

       $free =  DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select(DB::raw('SUM(a.qty) as jml'),'b.title as produk',DB::raw('SUM(a.price) as subtotal'))->where(['a.type'=>'gratis','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.post_id')->get();

       $freejml =  DB::table('transaction')->where(['type'=>'gratis','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $freetot =  DB::table('transaction')->where(['type'=>'gratis','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');


       $depart =  DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select(DB::raw('SUM(a.qty) as jml'),'b.title as produk',DB::raw('SUM(a.price) as subtotal'))->where(['a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.post_id')->get();

       $departjml =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $departtot =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');




       $bevjml  = DB::table('transaction as a')->where(['a.jenis'=>'beverages','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');
       $bevtotal  = DB::table('transaction as a')->where(['a.jenis'=>'beverages','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');


       $foodjml  = DB::table('transaction as a')->where(['a.jenis'=>'food','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');

       $foodtotal  = DB::table('transaction as a')->where(['a.jenis'=>'food','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');

       $otherjml  = DB::table('transaction as a')->where(['a.jenis'=>'other','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');

        $othertotal  = DB::table('transaction as a')->where(['a.jenis'=>'other','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');

        $discount = DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('discount');

        if($debitjml ==null)
        {
          $res = "0";
        }  
        $tax = $aostotal*10/100;
        $odb = $aostotal-$tax+$discount;
        $afterdisc = $odb-$discount;
        $servicecharge = $aostotal*5/100;

        $totalguest = DB::table('keranjang')->select(DB::raw('count(costumer_id) as jml'))->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->groupBy('costumer_id')->get();

        $totalbill = DB::table('keranjang')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count();
      
        $item[0] = array(['jenis'=>'beverages', 'jml'=>$bevjml,'total'=>$bevtotal]);
        $item[1] = array(['jenis'=>'food', 'jml'=>$foodjml,'total'=>$foodtotal]); 
        $item[2] = array(['jenis'=>'other', 'jml'=>$otherjml,'total'=>$othertotal]); 

        

        $return['costumer'] = $costumer;
        $return['petugas']  = $petugas;


        $return['aos']  = array(['jml'=>$aosjml,'total'=>$aostotal]);
        $return['tunai']  = array(['jml'=>$tunaijml,'total'=>$tunaitotal]);
        $return['debit']  = $res;
        $return['debittotal']  = array(['jml'=>$debitjml, 'total'=>$debittot]);

        $return['free'] = $free;
        $return['freetotal']  = array(['jml'=>$freejml, 'total'=>$freetot]);

        $return['depart'] = $depart;
        $return['departtotal']  = array(['jml'=>$departjml, 'total'=>$departtot]);
        $return['produk'] =  $item;
        $return['tax'] = $tax;
        $return['discount'] = $discount;
        $return['odb'] = $odb;
         $return['afterdisc'] = $afterdisc;
         $return['servicecharge'] = $servicecharge;
        $return['totalguest'] = $totalguest;
         $return['totalbill'] = $totalbill;
         
        return $return;

       

    }



   public function printhistory(Request $request)
    {

        $user = Auth::user();  
            $tanggal = $request->get('tanggal');
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');
            
         
                if(empty($tanggal))
                {    
                    $date = $tahun.'-'.$bulan;
                }else{

                   $date = $tahun.'-'.$bulan.'-'.$tanggal; 
                }  

        $petugas = DB::table('loghistory as a')->select('b.name as kasir','c.name as branch','c.description','c.telp','a.created_at as tgl_order')->join('users as b', 'a.user_id', '=', 'b.id')->join('branch as c', 'a.branch_id', '=', 'c.id')->where('a.user_id',$user->id)->get();

      
        
        $costumer =  DB::table('keranjang as a')->select('a.total','a.bayar','a.kembali','a.point','b.phone','a.meja_id','a.order_id','b.name as costumer_name','a.created_at as tgl_order','c.name as petugas')->join('users as b', 'a.costumer_id', '=', 'b.id')->join('users as c', 'a.user_id', '=', 'c.id')->where('a.created_at', 'like', ''.$date.'%')->get();


         $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
     
       $aosjml =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $aostotal =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');

      $tunaijml =  DB::table('keranjang')->where(['type'=>'tunai','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count('id');
      
      $tunaitotal =  DB::table('keranjang')->where(['type'=>'tunai','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');

       $debitjml =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count('id');
      
        $re =  DB::table('keranjang')->select('bank')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->get();


        for($i=0; $i<$debitjml; $i++)
        {



          $res[$i]['data'] = DB::table('keranjang')->select('bank','total')->where(['type'=>'debit','bank'=>$re[$i]->bank,'status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->get();
          $res[$i]['jml'] = DB::table('keranjang')->where(['type'=>'debit','bank'=>$re[$i]->bank,'status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count();

         
        }  

      
       
       

      
       $debittot =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');

       $free =  DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select(DB::raw('SUM(a.qty) as jml'),'b.title as produk',DB::raw('SUM(a.price) as subtotal'))->where(['a.type'=>'gratis','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.post_id')->get();

       $freejml =  DB::table('transaction')->where(['type'=>'gratis','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $freetot =  DB::table('transaction')->where(['type'=>'gratis','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');


       $depart =  DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select(DB::raw('SUM(a.qty) as jml'),'b.title as produk',DB::raw('SUM(a.price) as subtotal'))->where(['a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.post_id')->get();

       $departjml =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $departtot =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');




       $bevjml  = DB::table('transaction as a')->where(['a.jenis'=>'beverages','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');
       $bevtotal  = DB::table('transaction as a')->where(['a.jenis'=>'beverages','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');


       $foodjml  = DB::table('transaction as a')->where(['a.jenis'=>'food','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');

       $foodtotal  = DB::table('transaction as a')->where(['a.jenis'=>'food','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');

       $otherjml  = DB::table('transaction as a')->where(['a.jenis'=>'other','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');

        $othertotal  = DB::table('transaction as a')->where(['a.jenis'=>'other','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');

        $discount = DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('discount');

        if($debitjml ==null)
        {
          $res = "0";
        }  
        $tax = $aostotal*10/100;
        $odb = $aostotal-$tax+$discount;
        $afterdisc = $odb-$discount;
        $servicecharge = $aostotal*5/100;

        $totalguest = DB::table('keranjang')->select(DB::raw('count(costumer_id) as jml'))->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->groupBy('costumer_id')->get();

        $totalbill = DB::table('keranjang')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count();
      
        $item[0] = array(['jenis'=>'beverages', 'jml'=>$bevjml,'total'=>$bevtotal]);
        $item[1] = array(['jenis'=>'food', 'jml'=>$foodjml,'total'=>$foodtotal]); 
        $item[2] = array(['jenis'=>'other', 'jml'=>$otherjml,'total'=>$othertotal]); 

        

        $return['costumer'] = $costumer;
        $return['petugas']  = $petugas;


        $return['aos']  = array(['jml'=>$aosjml,'total'=>$aostotal]);
        $return['tunai']  = array(['jml'=>$tunaijml,'total'=>$tunaitotal]);
        $return['debit']  = $res;
        $return['debittotal']  = array(['jml'=>$debitjml, 'total'=>$debittot]);

        $return['free'] = $free;
        $return['freetotal']  = array(['jml'=>$freejml, 'total'=>$freetot]);

        $return['depart'] = $depart;
        $return['departtotal']  = array(['jml'=>$departjml, 'total'=>$departtot]);
        $return['produk'] =  $item;
        $return['tax'] = $tax;
        $return['discount'] = $discount;
        $return['odb'] = $odb;
         $return['afterdisc'] = $afterdisc;
         $return['servicecharge'] = $servicecharge;
        $return['totalguest'] = $totalguest;
         $return['totalbill'] = $totalbill;
         
        return $return;

       

    }


    public function totalbank(Request $request)
    {
        $bank = $request->get('bank');
        $totalRp = DB::table('keranjang')->where(['type'=>'debit','bank'=>$bank])->sum('total');
        $totaljml = DB::table('keranjang')->where(['type'=>'debit','bank'=>$bank])->count();

        $return['totrp'] = $totalRp;
        $return['totjml'] = $totaljml; 

        return $return; 

     } 

    public function updatenoted(Request $request)
    {
        $user = Auth::user();  
        $costumer_id = $request->get('costumer_id');
        $noted = $request->get('noted');
        $post_id = $request->get('post_id');
        $order_id = $request->get('order_id');
        DB::table('transaction')->where(['post_id'=>$post_id,'order_id'=>$order_id,'costumer_id'=>$costumer_id])->Update(['noted'=>$noted]);

        $transaksi = DB::table('transaction')->select('noted')->where(['post_id'=>$post_id,'order_id'=>$order_id])->get();
        return $transaksi;
    }


     public function prints(Request $request)
    {
        return view('history.prints');
    }

    /**
     * Display the specified Transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function createtransaksi(Request $request)
    {

        $user = Auth::user();  
        $costumer_id = $request->get('costumer_id');
        $bayar = $request->get('bayar');
        $total = $request->get('total');
        $kembali = $request->get('kembali');
        $type = $request->get('type');
        $free = $request->get('free');
         $bank = $request->get('bank');
         $no_atm = $request->get('no_atm');
         $order_id = $request->get('order_id');

           $date = date('Y-m-d');

            if($type =="debit")
            {

                DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['bank'=>$bank,'no_atm'=>$no_atm,'bayar'=>$bayar,'potongan'=>$free,'total'=>$total,'kembali'=>$kembali,'type'=>$type,'status'=>'1']);  

                DB::table('transaction')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['status'=>'1']);  


               
            }else{

                DB::table('keranjang')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['bayar'=>$bayar,'potongan'=>$free,'total'=>$total,'kembali'=>$kembali,'type'=>$type,'status'=>'1']);

                DB::table('transaction')->where(['user_id'=>$user->id,'costumer_id'=>$costumer_id,'order_id'=>$order_id])->update(['status'=>'1']);

                
            }    

           

        $keranjang = DB::table('keranjang as a')->select('a.order_id','b.name','a.meja_id','a.point','a.bayar','a.total','a.kembali','a.type','a.bank')->join('users as b', 'a.costumer_id', '=', 'b.id')->where(['a.user_id'=>$user->id,'a.costumer_id'=>$costumer_id,'order_id'=>$order_id,'a.status'=>'1'])->get();
        return $keranjang;
        
    }

    public function itemedit(Request $request)
    {
        $post_id = $request->get('post_id');

        $first = DB::table('transaction as a')->join('post as b','a.post_id','=','b.id')->select('a.id', DB::raw('SUM(a.qty) as jml'),DB::raw('SUM(a.price) as subtotal'),'a.type','a.jenis','b.title','a.post_id')->where(['a.post_id'=>$post_id,'a.type'=>'gratis']);

        $edite = DB::table('transaction as a')->union($first)->join('post as b','a.post_id','=','b.id')->select('a.id', DB::raw('SUM(a.qty) as jml'),DB::raw('SUM(a.price) as subtotal'),'a.type','a.jenis','b.title','a.post_id')->where(['a.post_id'=>$post_id,'a.type'=>'berbayar'])->groupBy('a.post_id')->get();

        $return['data'] = $edite;
        return $return;

    }


    public function history(Request $request)
    {
            $user = Auth::user(); 
            $tanggal = $request->get('tanggal');
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');
             $cari = $request->get('cari');
            $limit = "10";
         
                if(empty($tanggal))
                {    
                    $date = $tahun.'-'.$bulan;
                }else{

                   $date = $tahun.'-'.$bulan.'-'.$tanggal; 
                }
                  
              $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
               

             

                $history = DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select(DB::raw('SUM(a.qty) as jml'),'b.title as produk',DB::raw('SUM(a.price) as subtotal'),'a.created_at')->where(['a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.post_id')->paginate($limit);

                
                   $historyjml =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');

                  $historytot =  DB::table('transaction')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');


                  $foodjml  = DB::table('transaction as a')->where(['a.jenis'=>'food','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');

       $foodtotal  = DB::table('transaction as a')->where(['a.jenis'=>'food','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');

       $otherjml  = DB::table('transaction as a')->where(['a.jenis'=>'other','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');

        $othertotal  = DB::table('transaction as a')->where(['a.jenis'=>'other','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');

         $bevjml  = DB::table('transaction as a')->where(['a.jenis'=>'beverages','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('qty');
       $bevtotal  = DB::table('transaction as a')->where(['a.jenis'=>'beverages','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->sum('price');
           
             $item[0] = array(['jenis'=>'beverages', 'jml'=>$bevjml,'total'=>$bevtotal]);
             $item[1] = array(['jenis'=>'food', 'jml'=>$foodjml,'total'=>$foodtotal]); 
             $item[2] = array(['jenis'=>'other', 'jml'=>$otherjml,'total'=>$othertotal]);  


             $debitjml =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count('id');

             $debittot =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');
      
        $re =  DB::table('keranjang')->select('bank')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->get();


        for($i=0; $i<$debitjml; $i++)
        {



          $res[$i]['data'] = DB::table('keranjang')->select('bank','total')->where(['type'=>'debit','bank'=>$re[$i]->bank,'status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->get();
          $res[$i]['jml'] = DB::table('keranjang')->where(['type'=>'debit','bank'=>$re[$i]->bank,'status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count();

         
        }  

        $debittot =  DB::table('keranjang')->where(['type'=>'debit','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');

            if($debitjml ==null)
          {
          $res = "0";
           } 


           $tunaijml =  DB::table('keranjang')->where(['type'=>'tunai','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->count('id');
      
      $tunaitotal =  DB::table('keranjang')->where(['type'=>'tunai','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('total');

       $free =  DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select(DB::raw('SUM(a.qty) as jml'),'b.title as produk',DB::raw('SUM(a.price) as subtotal'))->where(['a.type'=>'gratis','a.status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.post_id')->get();

       $freejml =  DB::table('transaction')->where(['type'=>'gratis','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('qty');
       $freetot =  DB::table('transaction')->where(['type'=>'gratis','status'=>2,'branch_id'=>$cabang[0]->branch_id])->where('created_at', 'like', ''.$date.'%')->sum('price');


              $his['food'] = $item;
              $his['history'] = $history;
              $his['historytotal'] = array(['jml'=>$historyjml, 'total'=>$historytot]);  
           
              $his['debit'] = $res;
              $his['debittottal']  = array(['jml'=>$debitjml, 'total'=>$debittot]);
               $his['tunai']  = array(['jml'=>$tunaijml,'total'=>$tunaitotal]);

                $his['free'] = $free;
               $his['freetotal']  = array(['jml'=>$freejml, 'total'=>$freetot]);
             return  $his;  
       
    }
   
    public function chartr(Request $request)
    {
             $user = Auth::user(); 
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');                 
            $date = $tahun.'-'.$bulan;
              $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();
              $chapter = DB::table('branch')->select('name')->where('id',$cabang[0]->branch_id)->get();

            $chart = DB::table('transaction as a')->join('post as b', 'a.post_id', '=', 'b.id')->select('b.title as name', DB::raw('COUNT(post_id) as y'))->where(['a.status'=>'2','branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('post_id')->get();
 
            $return['chart'] = $chart;
            $return['cabang'] = $chapter;
            return $return;
       
    }

     public function chartcabang(Request $request)
    {

            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');                 
            $date = $tahun.'-'.$bulan;

            $chart = DB::table('transaction as a')->join('branch as b', 'a.branch_id', '=', 'b.id')->select('b.name', DB::raw('COUNT(a.branch_id) as y'))->where('a.status','2')->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.branch_id')->get();
 
            $return['chart'] = $chart;
            return $return;
       
    }


     public function chartfood(Request $request)
    {
            $user = Auth::user(); 
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');                 
            $date = $tahun.'-'.$bulan;
             $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();


            $jmlfood = DB::table('transaction as a')->where(['a.status'=>'2','jenis'=>'food','branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.jenis')->count();

            $jmlbev = DB::table('transaction as a')->where(['a.status'=>'2','jenis'=>'beverages','branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.jenis')->count();
            $jmlother = DB::table('transaction as a')->where(['a.status'=>'2','jenis'=>'other','branch_id'=>$cabang[0]->branch_id])->where('a.created_at', 'like', ''.$date.'%')->groupBy('a.jenis')->count();
  
            $return['chart'] = array(['food'=>$jmlfood,'beverages'=>$jmlbev,'other'=>$jmlother]);
       
              
            return $return;
       
    }

    public function getshared(Request $request)
    {


            $shared = DB::table('shared')->get();
            $return['shared'] = $shared;
            return $return;
       
    }


    


    public function latestmember(Request $request)
    {
             $user = Auth::user(); 
            $bulan = $request->get('bulan');
            $tahun = $request->get('tahun');                 
            $dated = $tahun.'-'.$bulan;

             $cabang = DB::table('loghistory')->where('user_id',$user->id)->get();

            $date = date('Y-m');
             $y = date('Y'); 
             $m =  date('m');
             $bl = $m-1;

             if($bl <10)
             {
                $bln = "0".$bl;
             } 

              $latesdate = $y.'-'.$bln;
              $dates = $y.'-'.$m;
           

                $first2 = DB::table('users')->select('id','name','type','photo','created_at')->where('created_at', 'like', ''.$latesdate.'%')->where('type', '<>', 'petugas')->where('type', '<>', 'admin'); 
                $tt = DB::table('users')->select('id','name','type','photo','created_at')->union($first2)->where('created_at', 'like', ''.$dates.'%')->where('type', '<>', 'petugas')->where('type', '<>', 'admin')->orderBy('id','desc')->limit(8)->get();

                $totalall = DB::table('users')->count(); 
              
            
                    $first = DB::table('users')->select('id','name','type','photo','created_at')->where('created_at', 'like', ''.$latesdate.'%')->where('type', '<>', 'petugas')->where('type', '<>', 'admin'); 

                    $member = DB::table('users')->select('id','name','type','photo','created_at')->union($first)->where('created_at', 'like', ''.$dates.'%')->where('type', '<>', 'petugas')->where('type', '<>', 'admin')->orderBy('id','desc')->limit(8)->get();    

                    $petugas = DB::table('users')->where(['type'=>'petugas'])->count();

                    $tunaitot = DB::table('keranjang')->where('created_at', 'like', ''.$dated.'%')->where(['type'=>'tunai','branch_id'=>$cabang[0]->branch_id])->count(); 
                    $tunaiRp = DB::table('keranjang')->where('created_at', 'like', ''.$dated.'%')->where(['type'=>'tunai','branch_id'=>$cabang[0]->branch_id])->sum('total');  


                    $debittot = DB::table('keranjang')->where('created_at', 'like', ''.$dated.'%')->where(['type'=>'debit','branch_id'=>$cabang[0]->branch_id])->count(); 
                    $debitRp = DB::table('keranjang')->where('created_at', 'like', ''.$dated.'%')->where(['type'=>'debit','branch_id'=>$cabang[0]->branch_id])->sum('total'); 

                    $transaksitot = DB::table('transaction')->where('created_at', 'like', ''.$dated.'%')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->count();
                    $transaksiRp = DB::table('transaction')->where('created_at', 'like', ''.$dated.'%')->where(['status'=>2,'branch_id'=>$cabang[0]->branch_id])->sum('price');

                    $app  = DB::table('users')->where(['type'=>'costumer'])->count();
                    $non  = DB::table('users')->where(['type'=>'noncostumer'])->count();

            
             $return['tunai'] = array($tunaitot,$tunaiRp);        
            $return['debit'] = array($debittot,$debitRp);
            $return['member'] = $member;
            $return['total_member'] =  count($tt);
            $return['total_all'] = $totalall;
            $return['detail_member'] = array($app,$non);
            $return['transaksi'] = array($transaksitot,$transaksiRp);
            return $return;
       
    }
 
}
