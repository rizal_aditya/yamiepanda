<?php

namespace App\Http\Controllers;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;
use App\Http\Requests;
use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\PostRepository;
use App\Repositories\AssetRepository;
use App\Repositories\UserRepository;
use App\Repositories\ClientRepository;
use App\Repositories\BranchRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Criteria\PostCriteria;
use Response;
use Auth;
use App\User;
use App\Models\Device;
use Storage;
use QrCode;
use App\Models\Post;
use DB;

class PostController extends AppBaseController
{
    /** @var  PostRepository */
    private $postRepository;
    private $assetRepository;
    private $branchRepository;
    private $userRepository;
    private $clientRepository;
    protected $url;

    public function __construct(PostRepository $postRepo, AssetRepository $assetRepo,BranchRepository $branchRepo,UserRepository $userRepo,ClientRepository $clientRepo,UrlGenerator $url)
    {
        $this->postRepository = $postRepo;
        $this->assetRepository = $assetRepo;
        $this->branchRepository = $branchRepo;
        $this->userRepository = $userRepo;
        $this->clientRepository = $clientRepo;
        $this->url = $url;
    }

    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
         $cari = $request->get('cari', null);
         $type = $request['type'];
         $user = Auth::user();

         
         $this->postRepository->pushCriteria(new PostCriteria($type));
         $this->postRepository->pushCriteria(new RequestCriteria($request));
         // paginator
         $post = $this->postRepository->paginate(10);
         // search
         if ($cari!=null)
         {
          return redirect('post/?type='.$type.'&search=title:'.$cari.'&searchFields=title:like');
  
         }

        return view('post.index')
        ->with(['post' => $post,'type' => $type]);
     
    }

    

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $type = $request['type'];
        $user = $this->userRepository->findByField('type','costumer');
         return view('post.create')
            ->with(['type'=> $type,'user'=>$user]);
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(CreatePostRequest $request)
    {

        $input = $request->all();
        $type = $request['type'];
        $name = $input['title'];
       


        $user = Auth::user();
        $client_id = $user->client_id;
        $post = $this->postRepository->create($input);
           
       
        $usersend = $this->userRepository->findByField(['type'=>'costumer','client_id'=>$client_id]);
        $api_key = $this->clientRepository->findByField('id',$client_id)->first()->api_key;
        if(empty($post->photo))
        {

            $img = '';
        }else{

            $img = $post->photo;
        }    

        if($type == "news")
        {
                
                 $message = array
                (

                "idnews"                => $post->id,
                "judul"                 => $post->title,
                "isi"                   => $post->content,
                "status"                => $post->status,
                "photo"                 => $img,
                "create"                => $post->created_at,
                "merchant"              => $post->user_id,
                "param"                 => "news",
                "sender"                => $post->sender

                );    

        }else if($type == "promo"){

                 $message = array
                (

                "idnews"                => $post->id,
                "judul"                 => $post->title,
                "isi"                   => $post->content,
                "status"                => $post->status,
                "photo"                 => $img,
                "create"                => $post->created_at,
                "merchant"              => $post->user_id,
                "param"                 => "promo",
                "broadcast_type"        => $post->broadcast_type,
                "sender"                => $post->sender

                );    
        }else if($type =="katalog"){


                  $message = array
                (

                "idnews"                => $post->id,
                "judul"                 => $post->title,
                "isi"                   => $post->content,
                "status"                => $post->status,
                "photo"                 => $img,
                "create"                => $post->created_at,
                "merchant"              => $post->user_id,
                "param"                 => "katalog",
                "broadcast_type"        => $post->broadcast_type,
                "sender"                => $post->sender

                );    


        }        


        foreach ($usersend as $row) 
         {
              foreach (Device::where('user_id', $row->id)->get() as $device)
              {
                    $reg_id         = $device->gcm;
                    $nama_member    = $device->name;
                    $gcm_id         =  $this->sendMessageThroughGCM($reg_id,$message,$api_key);
                
              }

         }


       Flash::success('Post saved successfully.');
       return redirect(url('post?type='.$type));
    }

    /**
     * Display the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,Request $request)
    {
        $type = $request['type'];
        $post = $this->postRepository->findWithoutFail($id);
       


        if (empty($post)) {
            Flash::error('Post not found');
            return redirect(url('post?type='.$type));
        }

      
          return view('post.show')->with(['post'=> $post,'type' => $type]);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,Request $request)
    {
        
        $type = $request['type'];
        $post = $this->postRepository->findWithoutFail($id);
        $user = $this->userRepository->findByField('type','costumer');
       
        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(url('post?type='.$type));
        }

        return view('post.edit')->with(['post'=> $post,'type' => $type,'user'=>$user]);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param  int              $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
     
     
    public function update($id, UpdatePostRequest $request)
    {
        $post = $this->postRepository->findWithoutFail($id);
        $type = $request['type'];
        $input = $request->all();
        if (empty($post)) {
            Flash::error('Post not found');

             return redirect(url('post?type='.$type));
        }

        // if($type == "promo")
        // {       
          
        //     $png = QrCode::format('png')->size(100)->generate(1);
        //     $input['qrcode']  = $png;
        // }

        $user = Auth::user();
        $client_id = $user->client_id;
        $post = $this->postRepository->update($input, $id);
           
       
        $user = $this->userRepository->findByField(['type'=>'costumer','client_id'=>$client_id]);
        $api_key = $this->clientRepository->findByField('id',$client_id)->first()->api_key;
        if(empty($post->photo))
        {

            $img = '';
        }else{

            $img = $post->photo;
        }    


         $message = array
        (

        "idnews"                => $post->id,
        "judul"                 => $post->title,
        "isi"                   => $post->content,
        "status"                => $post->status,
        "photo"                 => $img,
        "create"                => $post->created_at,
        "merchant"              => $post->user_id,
        "param"                 => "send",
        "sender"                => $post->sender

        );    

        foreach ($user as $row) 
         {
              foreach (Device::where('user_id', $row->id)->get() as $device)
              {
                    $reg_id         = $device->gcm;
                    $nama_member    = $device->name;
                    $gcm_id         =  $this->sendMessageThroughGCM($reg_id,$message,$api_key);
                
              }

         }

     Flash::success('Post updated successfully.');
     return redirect(url('post?type='.$type));

    }

    /**
     * Remove the specified Post from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
          $post = $this->postRepository->findWithoutFail($id);
          $type = $request['type'];
        
        if (empty($post)) {
            Flash::error('Post not found');

             return redirect(url('post?type='.$type));
        }
        
        
          if(!empty($post->file))
      {
        $file_gambar  =   $this->url->to(''.$post->file);  
        File::delete($file_gambar);
      }

    

        $this->postRepository->delete($id);


        Flash::success('Post deleted successfully.');

            return redirect(url('post?type='.$type));
    }

    public function sendMessageThroughGCM($registatoin_ids, $message, $api_key) {
       
        //Google cloud messaging GCM-API url
        $code = sprintf("%06d", mt_rand(1, 99999));
        $url = 'https://gcm-http.googleapis.com/gcm/send';
        $fields = array(
            'to' => $registatoin_ids,
            'data' => $message,
        );
       // return print json_encode($fields);
        // Update your Google Cloud Messaging API Key
        //define("GOOGLE_API_KEY", $api_key);       
        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);               
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

  


    
}
