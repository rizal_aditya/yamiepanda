<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleUserRepository;
use App\Http\Controllers\AppBaseController;
use App\Criteria\UserCriteria;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use DB;
use Auth;
use App\User;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;
    private $roleUserRepository;


    public function __construct(UserRepository $userRepo,RoleUserRepository $roleUserRepo)
    {
        $this->userRepository = $userRepo;
        $this->roleUserRepository = $roleUserRepo;
       
       
    }

     /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return User
     */
    public function index(Request $request)
    {
            $type = $request['type'];
            $user = Auth::user();
            $client_id = $user->client_id;
            $cari = $request->get('cari', null);
            //table Role
           
            $this->userRepository->pushCriteria(new UserCriteria($type,$client_id));
            $this->userRepository->pushCriteria(new RequestCriteria($request));
            $user = $this->userRepository->paginate(10);


            // search
             if ($cari!=null)
             {
              return redirect('user/?type='.$type.'&search=name:'.$cari.'&searchFields=name:like');
      
             }

        return view('user.index')
            ->with(['user' => $user,'type' => $type]);
    }


    

      /**
     * Show the form for creating a new User.
     *
     * @return User
     */

     public function create(Request $request)
    {

        $type = $request['type'];
        return view('user.create')
            ->with('type', $type);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return User
     */
    public function store(CreateUserRequest $request )
    {
        $type = $request['type'];
        $input = $request->all();
        $input['password'] = bcrypt($request->get('password'));
        $user = $this->userRepository->create($input);

        $id = $user->id;
        $role_id = $request->role_id;

        $dataRole['user_id'] = $id;
        $dataRole['role_id'] = $role_id;

        $roles = $this->roleUserRepository->create($dataRole);

        Flash::success('User saved successfully.');
        return redirect(url('user?type='.$type));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return User
     */
    public function show($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(url('user?type='.$type));
        }

        return view('user.show')->with(['user' => $user,'type'=> $type]);
        
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return User
     */



     public function edit($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);



        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');
           return redirect(url('user?type='.$type));

        }

        return view('user.edit')->with(['user'=> $user, 'type' => $type]);
        
        
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int   $id
     * @param UpdateUser $request
     *
     * @return User
     */
    


    public function update($id, UpdateUserRequest $request)
    {
        $user         = $this->userRepository->findWithoutFail($id);
        $type = $request['type'];
        if (empty($user)) 
        {
            Flash::error('User not found');
            return redirect(url('user?type='.$type));
        }


        $data = $request->except('password', 'password_confirmation');
        if ($request->has('password')) {
            if ($request['password']!='') {
                $data['password'] = bcrypt($request->get('password'));
            }
        }
        $user = $this->userRepository->update($data, $id);


        $id_user = $user->id;
        $role_id = $request->role_id;

        $roleuser = $this->roleUserRepository->findByField(['user_id'=>$id ,'role_id' => $role_id]);

        $dataRole['user_id'] = $id;
        $dataRole['role_id'] = $role_id;
        
        if(!empty($roleuser->toArray()))
        {
        $kode       = $roleuser[0]->user_id;
        $kodeRole   = $roleuser[0]->role_id;

     

            // if($id_user == $kode && $role_id == $kodeRole)
            // {

                 
            //      DB::table('role_user')->where('user_id', '=', $kode)->where('role_id', '=', $kodeRole)->delete();

            // }
            // else
            // {  

            //     $roles = $this->roleUserRepository->create($dataRole);
            // }

        }
        else
        {
                $roles = $this->roleUserRepository->create($dataRole);
        }

        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
    }

    /**
     * Remove the specified Person from storage.
     *
     * @param  int $id
     *
     * @return User
     */

    public function enable($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['status'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function premium($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['access'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }

     public function standar($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['access'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function disable($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['status'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }


     public function notification($id,Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);
        
        $type = $request['type'];
        if (empty($user)) {
            Flash::error('User not found');

           return redirect(url('user?type='.$type));

        }

        $input = $request['status'];
        $user = $this->userRepository->update($request->all(), $id);
        Flash::success('User updated successfully.');
        return redirect(url('user?type='.$type));
        
    }

     /**
     * Remove the specified PersonUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id,Request $request)
    {
        $type = $request['type'];
        $user = $this->userRepository->findWithoutFail($id);
        $kode = $user->id;

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(url('user?type='.$type));
        }

         $this->userRepository->delete($id);
         
         //$this->personRepository->delete($kode);
         //$this->personUserRepository->delete($kode);
         //$this->personClassRepository->delete($kode);

        Flash::success('User deleted successfully.');

        return redirect(url('user?type='.$type));
    }

   

   
}
