<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    //return redirect('id');
    return redirect('home');
    //return view('welcome');
});

/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
    	Route::post('auth', 'AuthAPIController@authenticate');
        Route::post('forgotPassword', 'AuthAPIController@forgot');
        Route::post('user/register', 'UserAPIController@register');
    	Route::group(['middleware' => ['jwt.auth']], function () {
            require config('infyom.laravel_generator.path.api_routes');
        });
    });
});



Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

// Registration Routes...
// Route::get('register', 'Auth\AuthController@getRegister');
// Route::post('register', 'Auth\AuthController@postRegister');

// Password Reset Routes...
Route::get('password/reset', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => ['auth']], function () {

    Route::get('fileupload', [
        'as' => 'fileupload',
        'uses' => 'Barryvdh\Elfinder\ElfinderController@showPopup',
    ]);

    Route::group(['middleware' => ['role:admin']], function () {
    Route::resource('paket', 'PaketController');
    });
    // tampilkan sesudah login









});



Route::get('petugas/cabang', 'PetugasController@cabang');

Route::resource('orders', 'OrdersController');
// catalog
Route::get('catalog/catalog', 'CatalogController@catalog');
Route::get('catalog/category', 'CatalogController@category');

Route::get('home', 'HomeController@index');


Route::resource('asset', 'AssetController');

Route::resource('category', 'CategoryController');
  Route::resource('post', 'PostController');



Route::resource('menu', 'MenuController');


Route::resource('transaction', 'TransactionController');

Route::resource('pointUser', 'PointUserController');

Route::resource('upload', 'UploadController');

Route::resource('shared', 'SharedController');
// Route::resource('stock', 'StockController');
Route::get('history', 'HistoryController@index');

Route::get('stock', 'StockController@index');
Route::get('stock/editstock', 'StockController@editstock');
Route::get('stock/deletestock', 'StockController@deletestock');
Route::get('stock/getcatalog', 'StockController@getcatalog');
Route::get('stock/getstock', 'StockController@getstock');
Route::get('stock/getcategori', 'StockController@getcategori');
Route::post('stock/store', 'StockController@store');
Route::get('stock/checkstock', 'StockController@checkstock');

Route::get('orders', 'OrdersController@index');
Route::get('member/draft', 'MemberController@draft');
Route::get('member', 'MemberController@index');
Route::post('member/draftcancel', 'MemberController@draftcancel');
Route::post('member/store', 'MemberController@store');
Route::post('member/hapus', 'MemberController@hapus');
Route::post('member/point', 'MemberController@point');
Route::get('member/statuspoint', 'MemberController@statuspoint');

//# transaksi member
Route::get('transaksi/getcatalog','TransaksiController@getcatalog'); 
Route::post('transaksi/simpandraf','TransaksiController@simpandraf'); 
Route::get('transaksi/itemedit','TransaksiController@itemedit'); 
Route::get('transaksi/detailitem','TransaksiController@detailitem'); 
Route::get('transaksi/cekmember','TransaksiController@cekmember'); 
Route::get('transaksi/totalbank','TransaksiController@totalbank');
Route::get('transaksi/latestmember','TransaksiController@latestmember'); 
Route::get('transaksi/memberview', 'TransaksiController@memberview');
Route::post('transaksi/member', 'TransaksiController@member');
Route::post('transaksi/delorderm', 'TransaksiController@delorderm');
Route::get('transaksi/updatemeja', 'TransaksiController@updatemeja');
Route::get('transaksi/keranjang', 'TransaksiController@keranjang');
Route::get('transaksi/plusorder', 'TransaksiController@plusorder');
Route::get('transaksi/minusorder', 'TransaksiController@minusorder');
Route::get('transaksi/delorder', 'TransaksiController@delorder');
Route::post('transaksi/createorder', 'TransaksiController@createorder');
Route::get('transaksi/detailproduk', 'TransaksiController@detailproduk');
Route::post('transaksi/updatenoted', 'TransaksiController@updatenoted');
Route::get('transaksi/orderbaru', 'TransaksiController@orderbaru');
Route::get('transaksi/history', 'TransaksiController@history');
Route::get('transaksi/printdata', 'TransaksiController@printdata');
Route::get('transaksi/printlap', 'TransaksiController@printlap');
Route::get('transaksi/printhistory', 'TransaksiController@printhistory');
Route::get('transaksi/chartr', 'TransaksiController@chartr');
Route::get('transaksi/chartcabang', 'TransaksiController@chartcabang');
Route::get('transaksi/chartfood', 'TransaksiController@chartfood');
Route::get('transaksi/getshared', 'TransaksiController@getshared');
Route::post('transaksi/createtransaksi', 'TransaksiController@createtransaksi');

Route::resource('cabang', 'BranchController');
Route::resource('petugas', 'PetugasController');
Route::resource('costumer', 'CostumerController');
Route::get('costumer/{id}/disable', 'CostumerController@disable');
Route::get('costumer/{id}/enable', 'CostumerController@enable');
Route::post('shared/send','SharedController@send'); 
