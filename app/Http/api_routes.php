<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/

/* --- User API --- */

Route::get('user/myProfile', 'UserAPIController@myProfile');

Route::put('user/updatePassword', 'UserAPIController@updatePassword');

Route::put('user/updateProfile', 'UserAPIController@updateProfile');
Route::get('shared/status', 'SharedAPIController@status');
/* --- Device API --- */

Route::post('device/check', 'DeviceAPIController@check');

Route::post('update/check', 'UpdateAPIController@check');

/* --- Client API --- */

Route::get('client', 'ClientAPIController@index');

/* --- Branch API --- */




/* --- Person User, Person Class, API --- */


//Route::get('post', 'PostAPIController@index');
Route::resource('post', 'PostAPIController');

Route::get('asset', 'AssetAPIController@index');

Route::get('news', 'PostAPIController@getNews');
Route::get('promo','PostAPIController@getPromo');
Route::get('katalog','PostAPIController@getKatalog');
Route::get('slider','PostAPIController@getslider');



//Route::get('user/branch', 'UserAPIController@getUserBranch');
Route::group(['middleware' => ['role:admin']], function () {

	Route::resource('user', 'UserAPIController');
	Route::resource('client', 'ClientAPIController');
	Route::resource('branch', 'BranchAPIController');
	
	Route::resource('device', 'DeviceAPIController');
	//Route::resource('post', 'PostAPIController');
	
	
	Route::resource('menu', 'MenuAPIController');
	

});


Route::group(['middleware' => ['role:user']], function () {
	Route::resource('post', 'PostAPIController');
	
	
	Route::resource('user', 'UserAPIController');
	
});




Route::resource('post', 'PostAPIController');



Route::resource('settings', 'SettingsAPIController');


/* --- Person API --- */

Route::resource('category', 'CategoryAPIController');


Route::resource('order', 'OrderAPIController');



Route::resource('point', 'PointUserAPIController');
Route::post('point/store', 'PointUserAPIController@store');
Route::resource('client', 'ClientAPIController');	
Route::resource('modal', 'ModalAPIController');

Route::resource('transaction', 'TransactionAPIController');

Route::resource('point_user', 'PointUserAPIController');

Route::resource('upload', 'UploadAPIController');

Route::resource('shared', 'SharedAPIController');


Route::resource('penjualan', 'PenjualanAPIController');