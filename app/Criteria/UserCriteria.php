<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class PostUserCriteria
 * @package namespace App\Criteria;
 */
class UserCriteria implements CriteriaInterface
{

     public $type;
     public $client_id;
     public function __construct($type,$client_id)
     {
        $this->type = $type;
        $this->client_id = $client_id;
        
     }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        
        
        $userid = Auth::user();
        if($userid->hasRole('admin'))
        {    
          
            $model = $model->where(['type'=> $this->type])->orderBy('id','desc');   
           
         
        }else if($userid->hasRole('user')){
            
            $model = $model->where(['type'=> $this->type, 'client_id' => $this->client_id])->orderBy('id','desc');  
        }
        
        return $model;
    }
}
