<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
use App\Models\Branch;

/**
 * Class PostUserCriteria
 * @package namespace App\Criteria;
 */

class PostCriteria implements CriteriaInterface
{

     public $type;
   
     public function __construct($type)
     {
        $this->type = $type;
        
     }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $userid = Auth::user();
        if($userid->hasRole('admin'))
        {       
            $model = $model->where(['type'=> $this->type])->orderBy('id','desc');    
        }else if($this->type ==""){

             $model = $model->orderBy('id','desc'); 
        }
        return $model;
    }
}
