<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Branch;
use Auth;
/**
 * Class PostCriteria
 * @package namespace App\Criteria;
 */

   

class BranchCriteria implements CriteriaInterface
{

     private $user_id;
     public function __construct($user_id)
     {
        $this->user_id = $user_id;
       
     }
   
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

         $userid = Auth::user();
            if($userid->hasRole('admin'))
            {    
                $model = $model->orderBy('id','desc');

             }else if($userid->hasRole('user')){

                $model = $model->where(['user_id'=> $this->user_id])->orderBy('id','desc');    
            }
         return $model;
    }
}
