<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Branch;
use Auth;
/**
 * Class PostCriteria
 * @package namespace App\Criteria;
 */

   

class SurveyCriteria implements CriteriaInterface
{

   
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        
            $userid = Auth::user();
       
            $model = $model->where(['branch_id'=> Branch::where('user_id',$userid->id)->first()->id])->orderBy('id','desc');   
        
         
         return $model;
    }
}
