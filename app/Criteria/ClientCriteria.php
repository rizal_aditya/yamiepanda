<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use App\Models\Branch;
use Auth;
/**
 * Class PostCriteria
 * @package namespace App\Criteria;
 */

   

class ClientCriteria implements CriteriaInterface
{

   
    
     public function __construct()
     {
        
       
     }
   
   
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
            $userid = Auth::user();
            if($userid->hasRole('admin'))
            {    
                $model = $model->orderBy('id','desc');

             }
         return $model;
    }
}
