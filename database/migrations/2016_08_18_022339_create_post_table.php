<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->increments('id');
            // $table->integer('branch_id')->unsigned();
            // $table->foreign('branch_id')->references('id')->on('branch')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->string('jenis', 100);
            $table->integer('point');
            $table->integer('price');
            $table->integer('discount');
            $table->date('date');
            $table->string('type', 100);
            $table->string('title', 100);
            $table->text('content');
            $table->string('keyword', 100);
            $table->text('description');
            $table->dateTime('start_date');
            $table->dateTime('end_date');         
            $table->string('broadcast_type', 100);
            $table->boolean('status');
            $table->string('photo', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post');
    }
}
