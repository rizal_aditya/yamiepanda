<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKeranjangTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('keranjang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('costumer_id')->unsigned();
            $table->foreign('costumer_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('branch_id')->unsigned();
            $table->foreign('branch_id')->references('id')->on('branch')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('meja_id');
            $table->integer('order_id');
            $table->integer('bayar');
            $table->integer('point');
            $table->integer('total');
            $table->integer('potongan');
            $table->integer('kembali');
            $table->string('jenis');
            $table->string('bank');
            $table->string('type');
            $table->integer('no_atm');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('keranjang');
    }
}
