<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('costumer_id')->unsigned();
            $table->foreign('costumer_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('order_id');
            $table->integer('keranjang_id')->unsigned();
            $table->foreign('keranjang_id')->references('id')->on('keranjang')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('branch_id')->unsigned();
            $table->foreign('branch_id')->references('id')->on('branch')->onUpdate('cascade')->onDelete('cascade');

             $table->integer('qty');
            $table->integer('post_id');
            $table->string('jenis');
            $table->string('type');
            $table->integer('price');
           
            $table->string('noted');
            $table->integer('point');
            $table->integer('discount');
          
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
    }
}
