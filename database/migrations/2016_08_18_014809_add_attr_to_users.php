<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttrToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('client_id')->unsigned()->after("id");
            $table->foreign('client_id')->references('id')->on('client')->onUpdate('cascade')->onDelete('cascade');
            $table->dropUnique('users_email_unique');
            $table->unique(array('email', 'client_id'));
            $table->date('birthday');
            $table->enum('religion', ['Islam', 'Katolik', 'Nasrani', 'Buddha', 'Hindu', 'Konghucu', 'Other'])->after('birthday');
            $table->string('phone');
            $table->text('address');
            $table->text('photo');
            $table->string('type');
            $table->integer('point');
            $table->string('access');
            $table->boolean('status');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthday')->after("remember_token");
            $table->enum('religion', ['Islam', 'Katolik', 'Nasrani', 'Buddha', 'Hindu', 'Konghucu', 'Other'])->after('birthday');
            $table->string('phone', 100)->after('religion');
            $table->text('address')->after('phone');
            $table->text('photo')->after('address');
            $table->boolean('status')->after('photo');
        });
    }
}
