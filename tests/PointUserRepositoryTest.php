<?php

use App\Models\PointUser;
use App\Repositories\PointUserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointUserRepositoryTest extends TestCase
{
    use MakePointUserTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PointUserRepository
     */
    protected $pointUserRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pointUserRepo = App::make(PointUserRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePointUser()
    {
        $pointUser = $this->fakePointUserData();
        $createdPointUser = $this->pointUserRepo->create($pointUser);
        $createdPointUser = $createdPointUser->toArray();
        $this->assertArrayHasKey('id', $createdPointUser);
        $this->assertNotNull($createdPointUser['id'], 'Created PointUser must have id specified');
        $this->assertNotNull(PointUser::find($createdPointUser['id']), 'PointUser with given id must be in DB');
        $this->assertModelData($pointUser, $createdPointUser);
    }

    /**
     * @test read
     */
    public function testReadPointUser()
    {
        $pointUser = $this->makePointUser();
        $dbPointUser = $this->pointUserRepo->find($pointUser->id);
        $dbPointUser = $dbPointUser->toArray();
        $this->assertModelData($pointUser->toArray(), $dbPointUser);
    }

    /**
     * @test update
     */
    public function testUpdatePointUser()
    {
        $pointUser = $this->makePointUser();
        $fakePointUser = $this->fakePointUserData();
        $updatedPointUser = $this->pointUserRepo->update($fakePointUser, $pointUser->id);
        $this->assertModelData($fakePointUser, $updatedPointUser->toArray());
        $dbPointUser = $this->pointUserRepo->find($pointUser->id);
        $this->assertModelData($fakePointUser, $dbPointUser->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePointUser()
    {
        $pointUser = $this->makePointUser();
        $resp = $this->pointUserRepo->delete($pointUser->id);
        $this->assertTrue($resp);
        $this->assertNull(PointUser::find($pointUser->id), 'PointUser should not exist in DB');
    }
}
