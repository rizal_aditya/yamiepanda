<?php

use App\Models\Modal;
use App\Repositories\ModalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModalRepositoryTest extends TestCase
{
    use MakeModalTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ModalRepository
     */
    protected $modalRepo;

    public function setUp()
    {
        parent::setUp();
        $this->modalRepo = App::make(ModalRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateModal()
    {
        $modal = $this->fakeModalData();
        $createdModal = $this->modalRepo->create($modal);
        $createdModal = $createdModal->toArray();
        $this->assertArrayHasKey('id', $createdModal);
        $this->assertNotNull($createdModal['id'], 'Created Modal must have id specified');
        $this->assertNotNull(Modal::find($createdModal['id']), 'Modal with given id must be in DB');
        $this->assertModelData($modal, $createdModal);
    }

    /**
     * @test read
     */
    public function testReadModal()
    {
        $modal = $this->makeModal();
        $dbModal = $this->modalRepo->find($modal->id);
        $dbModal = $dbModal->toArray();
        $this->assertModelData($modal->toArray(), $dbModal);
    }

    /**
     * @test update
     */
    public function testUpdateModal()
    {
        $modal = $this->makeModal();
        $fakeModal = $this->fakeModalData();
        $updatedModal = $this->modalRepo->update($fakeModal, $modal->id);
        $this->assertModelData($fakeModal, $updatedModal->toArray());
        $dbModal = $this->modalRepo->find($modal->id);
        $this->assertModelData($fakeModal, $dbModal->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteModal()
    {
        $modal = $this->makeModal();
        $resp = $this->modalRepo->delete($modal->id);
        $this->assertTrue($resp);
        $this->assertNull(Modal::find($modal->id), 'Modal should not exist in DB');
    }
}
