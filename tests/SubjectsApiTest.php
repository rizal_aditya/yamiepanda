<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubjectsApiTest extends TestCase
{
    use MakeSubjectsTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSubjects()
    {
        $subjects = $this->fakeSubjectsData();
        $this->json('POST', '/api/v1/subjects', $subjects);

        $this->assertApiResponse($subjects);
    }

    /**
     * @test
     */
    public function testReadSubjects()
    {
        $subjects = $this->makeSubjects();
        $this->json('GET', '/api/v1/subjects/'.$subjects->id);

        $this->assertApiResponse($subjects->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSubjects()
    {
        $subjects = $this->makeSubjects();
        $editedSubjects = $this->fakeSubjectsData();

        $this->json('PUT', '/api/v1/subjects/'.$subjects->id, $editedSubjects);

        $this->assertApiResponse($editedSubjects);
    }

    /**
     * @test
     */
    public function testDeleteSubjects()
    {
        $subjects = $this->makeSubjects();
        $this->json('DELETE', '/api/v1/subjects/'.$subjects->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/subjects/'.$subjects->id);

        $this->assertResponseStatus(404);
    }
}
