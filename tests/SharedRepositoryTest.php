<?php

use App\Models\Shared;
use App\Repositories\SharedRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SharedRepositoryTest extends TestCase
{
    use MakeSharedTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SharedRepository
     */
    protected $sharedRepo;

    public function setUp()
    {
        parent::setUp();
        $this->sharedRepo = App::make(SharedRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateShared()
    {
        $shared = $this->fakeSharedData();
        $createdShared = $this->sharedRepo->create($shared);
        $createdShared = $createdShared->toArray();
        $this->assertArrayHasKey('id', $createdShared);
        $this->assertNotNull($createdShared['id'], 'Created Shared must have id specified');
        $this->assertNotNull(Shared::find($createdShared['id']), 'Shared with given id must be in DB');
        $this->assertModelData($shared, $createdShared);
    }

    /**
     * @test read
     */
    public function testReadShared()
    {
        $shared = $this->makeShared();
        $dbShared = $this->sharedRepo->find($shared->id);
        $dbShared = $dbShared->toArray();
        $this->assertModelData($shared->toArray(), $dbShared);
    }

    /**
     * @test update
     */
    public function testUpdateShared()
    {
        $shared = $this->makeShared();
        $fakeShared = $this->fakeSharedData();
        $updatedShared = $this->sharedRepo->update($fakeShared, $shared->id);
        $this->assertModelData($fakeShared, $updatedShared->toArray());
        $dbShared = $this->sharedRepo->find($shared->id);
        $this->assertModelData($fakeShared, $dbShared->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteShared()
    {
        $shared = $this->makeShared();
        $resp = $this->sharedRepo->delete($shared->id);
        $this->assertTrue($resp);
        $this->assertNull(Shared::find($shared->id), 'Shared should not exist in DB');
    }
}
