<?php

use Faker\Factory as Faker;
use App\Models\Shared;
use App\Repositories\SharedRepository;

trait MakeSharedTrait
{
    /**
     * Create fake instance of Shared and save it in database
     *
     * @param array $sharedFields
     * @return Shared
     */
    public function makeShared($sharedFields = [])
    {
        /** @var SharedRepository $sharedRepo */
        $sharedRepo = App::make(SharedRepository::class);
        $theme = $this->fakeSharedData($sharedFields);
        return $sharedRepo->create($theme);
    }

    /**
     * Get fake instance of Shared
     *
     * @param array $sharedFields
     * @return Shared
     */
    public function fakeShared($sharedFields = [])
    {
        return new Shared($this->fakeSharedData($sharedFields));
    }

    /**
     * Get fake data of Shared
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSharedData($sharedFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $sharedFields);
    }
}
