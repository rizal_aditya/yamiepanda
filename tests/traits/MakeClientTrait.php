<?php

use Faker\Factory as Faker;
use App\Models\Client;
use App\Repositories\ClientRepository;

trait MakeClientTrait
{
    /**
     * Create fake instance of Client and save it in database
     *
     * @param array $clientFields
     * @return Client
     */
    public function makeClient($clientFields = [])
    {
        /** @var ClientRepository $clientRepo */
        $clientRepo = App::make(ClientRepository::class);
        $theme = $this->fakeClientData($clientFields);
        return $clientRepo->create($theme);
    }

    /**
     * Get fake instance of Client
     *
     * @param array $clientFields
     * @return Client
     */
    public function fakeClient($clientFields = [])
    {
        return new Client($this->fakeClientData($clientFields));
    }

    /**
     * Get fake data of Client
     *
     * @param array $postFields
     * @return array
     */
    public function fakeClientData($clientFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->randomElement(['profile', 'corporite', 'sekolah', 'corporate', '[D[D[D[D[D[D[D[D[D[D[D[D[D[D[D[D[C[C[C[C[C[C[[3~[3~[3~[3~[C[C[C[C[C[C[[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[C[school', 'corporate', 'retail']),
            'access' => $fake->randomElement(['admin', 'super_admin']),
            'name' => $fake->word,
            'owner' => $fake->word,
            'photo' => $fake->word,
            'reg_number' => $fake->word,
            'phohe' => $fake->word,
            'address' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $clientFields);
    }
}
