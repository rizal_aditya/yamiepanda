<?php

use Faker\Factory as Faker;
use App\Models\Modal;
use App\Repositories\ModalRepository;

trait MakeModalTrait
{
    /**
     * Create fake instance of Modal and save it in database
     *
     * @param array $modalFields
     * @return Modal
     */
    public function makeModal($modalFields = [])
    {
        /** @var ModalRepository $modalRepo */
        $modalRepo = App::make(ModalRepository::class);
        $theme = $this->fakeModalData($modalFields);
        return $modalRepo->create($theme);
    }

    /**
     * Get fake instance of Modal
     *
     * @param array $modalFields
     * @return Modal
     */
    public function fakeModal($modalFields = [])
    {
        return new Modal($this->fakeModalData($modalFields));
    }

    /**
     * Get fake data of Modal
     *
     * @param array $postFields
     * @return array
     */
    public function fakeModalData($modalFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nominal' => $fake->randomDigitNotNull,
            'branch_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $modalFields);
    }
}
