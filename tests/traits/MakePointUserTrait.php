<?php

use Faker\Factory as Faker;
use App\Models\PointUser;
use App\Repositories\PointUserRepository;

trait MakePointUserTrait
{
    /**
     * Create fake instance of PointUser and save it in database
     *
     * @param array $pointUserFields
     * @return PointUser
     */
    public function makePointUser($pointUserFields = [])
    {
        /** @var PointUserRepository $pointUserRepo */
        $pointUserRepo = App::make(PointUserRepository::class);
        $theme = $this->fakePointUserData($pointUserFields);
        return $pointUserRepo->create($theme);
    }

    /**
     * Get fake instance of PointUser
     *
     * @param array $pointUserFields
     * @return PointUser
     */
    public function fakePointUser($pointUserFields = [])
    {
        return new PointUser($this->fakePointUserData($pointUserFields));
    }

    /**
     * Get fake data of PointUser
     *
     * @param array $postFields
     * @return array
     */
    public function fakePointUserData($pointUserFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'transaction_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pointUserFields);
    }
}
