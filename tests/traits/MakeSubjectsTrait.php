<?php

use Faker\Factory as Faker;
use App\Models\Subjects;
use App\Repositories\SubjectsRepository;

trait MakeSubjectsTrait
{
    /**
     * Create fake instance of Subjects and save it in database
     *
     * @param array $subjectsFields
     * @return Subjects
     */
    public function makeSubjects($subjectsFields = [])
    {
        /** @var SubjectsRepository $subjectsRepo */
        $subjectsRepo = App::make(SubjectsRepository::class);
        $theme = $this->fakeSubjectsData($subjectsFields);
        return $subjectsRepo->create($theme);
    }

    /**
     * Get fake instance of Subjects
     *
     * @param array $subjectsFields
     * @return Subjects
     */
    public function fakeSubjects($subjectsFields = [])
    {
        return new Subjects($this->fakeSubjectsData($subjectsFields));
    }

    /**
     * Get fake data of Subjects
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSubjectsData($subjectsFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'level' => $fake->word,
            'Class' => $fake->word,
            'status' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $subjectsFields);
    }
}
