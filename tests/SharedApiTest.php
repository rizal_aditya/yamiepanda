<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SharedApiTest extends TestCase
{
    use MakeSharedTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateShared()
    {
        $shared = $this->fakeSharedData();
        $this->json('POST', '/api/v1/shared', $shared);

        $this->assertApiResponse($shared);
    }

    /**
     * @test
     */
    public function testReadShared()
    {
        $shared = $this->makeShared();
        $this->json('GET', '/api/v1/shared/'.$shared->id);

        $this->assertApiResponse($shared->toArray());
    }

    /**
     * @test
     */
    public function testUpdateShared()
    {
        $shared = $this->makeShared();
        $editedShared = $this->fakeSharedData();

        $this->json('PUT', '/api/v1/shared/'.$shared->id, $editedShared);

        $this->assertApiResponse($editedShared);
    }

    /**
     * @test
     */
    public function testDeleteShared()
    {
        $shared = $this->makeShared();
        $this->json('DELETE', '/api/v1/shared/'.$shared->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/shared/'.$shared->id);

        $this->assertResponseStatus(404);
    }
}
