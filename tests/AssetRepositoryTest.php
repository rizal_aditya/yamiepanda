<?php

use App\Models\Asset;
use App\Repositories\AssetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AssetRepositoryTest extends TestCase
{
    use MakeAssetTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AssetRepository
     */
    protected $assetRepo;

    public function setUp()
    {
        parent::setUp();
        $this->assetRepo = App::make(AssetRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAsset()
    {
        $asset = $this->fakeAssetData();
        $createdAsset = $this->assetRepo->create($asset);
        $createdAsset = $createdAsset->toArray();
        $this->assertArrayHasKey('id', $createdAsset);
        $this->assertNotNull($createdAsset['id'], 'Created Asset must have id specified');
        $this->assertNotNull(Asset::find($createdAsset['id']), 'Asset with given id must be in DB');
        $this->assertModelData($asset, $createdAsset);
    }

    /**
     * @test read
     */
    public function testReadAsset()
    {
        $asset = $this->makeAsset();
        $dbAsset = $this->assetRepo->find($asset->id);
        $dbAsset = $dbAsset->toArray();
        $this->assertModelData($asset->toArray(), $dbAsset);
    }

    /**
     * @test update
     */
    public function testUpdateAsset()
    {
        $asset = $this->makeAsset();
        $fakeAsset = $this->fakeAssetData();
        $updatedAsset = $this->assetRepo->update($fakeAsset, $asset->id);
        $this->assertModelData($fakeAsset, $updatedAsset->toArray());
        $dbAsset = $this->assetRepo->find($asset->id);
        $this->assertModelData($fakeAsset, $dbAsset->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAsset()
    {
        $asset = $this->makeAsset();
        $resp = $this->assetRepo->delete($asset->id);
        $this->assertTrue($resp);
        $this->assertNull(Asset::find($asset->id), 'Asset should not exist in DB');
    }
}
