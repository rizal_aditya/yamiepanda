<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModalApiTest extends TestCase
{
    use MakeModalTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateModal()
    {
        $modal = $this->fakeModalData();
        $this->json('POST', '/api/v1/modal', $modal);

        $this->assertApiResponse($modal);
    }

    /**
     * @test
     */
    public function testReadModal()
    {
        $modal = $this->makeModal();
        $this->json('GET', '/api/v1/modal/'.$modal->id);

        $this->assertApiResponse($modal->toArray());
    }

    /**
     * @test
     */
    public function testUpdateModal()
    {
        $modal = $this->makeModal();
        $editedModal = $this->fakeModalData();

        $this->json('PUT', '/api/v1/modal/'.$modal->id, $editedModal);

        $this->assertApiResponse($editedModal);
    }

    /**
     * @test
     */
    public function testDeleteModal()
    {
        $modal = $this->makeModal();
        $this->json('DELETE', '/api/v1/modal/'.$modal->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/modal/'.$modal->id);

        $this->assertResponseStatus(404);
    }
}
