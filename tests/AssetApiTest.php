<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AssetApiTest extends TestCase
{
    use MakeAssetTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAsset()
    {
        $asset = $this->fakeAssetData();
        $this->json('POST', '/api/v1/asset', $asset);

        $this->assertApiResponse($asset);
    }

    /**
     * @test
     */
    public function testReadAsset()
    {
        $asset = $this->makeAsset();
        $this->json('GET', '/api/v1/asset/'.$asset->id);

        $this->assertApiResponse($asset->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAsset()
    {
        $asset = $this->makeAsset();
        $editedAsset = $this->fakeAssetData();

        $this->json('PUT', '/api/v1/asset/'.$asset->id, $editedAsset);

        $this->assertApiResponse($editedAsset);
    }

    /**
     * @test
     */
    public function testDeleteAsset()
    {
        $asset = $this->makeAsset();
        $this->json('DELETE', '/api/v1/asset/'.$asset->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/asset/'.$asset->id);

        $this->assertResponseStatus(404);
    }
}
