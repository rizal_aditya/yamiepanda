<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointUserApiTest extends TestCase
{
    use MakePointUserTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePointUser()
    {
        $pointUser = $this->fakePointUserData();
        $this->json('POST', '/api/v1/pointUser', $pointUser);

        $this->assertApiResponse($pointUser);
    }

    /**
     * @test
     */
    public function testReadPointUser()
    {
        $pointUser = $this->makePointUser();
        $this->json('GET', '/api/v1/pointUser/'.$pointUser->id);

        $this->assertApiResponse($pointUser->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePointUser()
    {
        $pointUser = $this->makePointUser();
        $editedPointUser = $this->fakePointUserData();

        $this->json('PUT', '/api/v1/pointUser/'.$pointUser->id, $editedPointUser);

        $this->assertApiResponse($editedPointUser);
    }

    /**
     * @test
     */
    public function testDeletePointUser()
    {
        $pointUser = $this->makePointUser();
        $this->json('DELETE', '/api/v1/pointUser/'.$pointUser->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pointUser/'.$pointUser->id);

        $this->assertResponseStatus(404);
    }
}
