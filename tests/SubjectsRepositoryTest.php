<?php

use App\Models\Subjects;
use App\Repositories\SubjectsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SubjectsRepositoryTest extends TestCase
{
    use MakeSubjectsTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SubjectsRepository
     */
    protected $subjectsRepo;

    public function setUp()
    {
        parent::setUp();
        $this->subjectsRepo = App::make(SubjectsRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSubjects()
    {
        $subjects = $this->fakeSubjectsData();
        $createdSubjects = $this->subjectsRepo->create($subjects);
        $createdSubjects = $createdSubjects->toArray();
        $this->assertArrayHasKey('id', $createdSubjects);
        $this->assertNotNull($createdSubjects['id'], 'Created Subjects must have id specified');
        $this->assertNotNull(Subjects::find($createdSubjects['id']), 'Subjects with given id must be in DB');
        $this->assertModelData($subjects, $createdSubjects);
    }

    /**
     * @test read
     */
    public function testReadSubjects()
    {
        $subjects = $this->makeSubjects();
        $dbSubjects = $this->subjectsRepo->find($subjects->id);
        $dbSubjects = $dbSubjects->toArray();
        $this->assertModelData($subjects->toArray(), $dbSubjects);
    }

    /**
     * @test update
     */
    public function testUpdateSubjects()
    {
        $subjects = $this->makeSubjects();
        $fakeSubjects = $this->fakeSubjectsData();
        $updatedSubjects = $this->subjectsRepo->update($fakeSubjects, $subjects->id);
        $this->assertModelData($fakeSubjects, $updatedSubjects->toArray());
        $dbSubjects = $this->subjectsRepo->find($subjects->id);
        $this->assertModelData($fakeSubjects, $dbSubjects->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSubjects()
    {
        $subjects = $this->makeSubjects();
        $resp = $this->subjectsRepo->delete($subjects->id);
        $this->assertTrue($resp);
        $this->assertNull(Subjects::find($subjects->id), 'Subjects should not exist in DB');
    }
}
