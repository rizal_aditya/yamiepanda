(function ( $ ) {

	

	//line
	$.fn.bootstrapLine = function( options ) {

		var settings = $.extend({
			on: 'On',
			off: 'Off	',
			onLabel: '&nbsp;&nbsp;&nbsp;',
			offLabel: '&nbsp;&nbsp;&nbsp;',
			same: false,//same labels for on/off states
			size: 'md',
			onClass: 'primary',
			offClass: 'default'
			}, options );

		settings.size = ' btn-'+settings.size;
		if (settings.same){
			settings.onLabel = settings.on;
			settings.offLabel = settings.off;
		}

		return this.each(function(e) {
			var c = $(this);
			var disabled = c.is(":disabled") ? " disabled" : "";
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var div = $('<div class="btn-share btn-group btn-toggle" style="white-space: nowrap;"></div>').insertAfter(this);
			var on = $('<button class="btn btn-primary '+settings.size+disabled+'" style="float: none;display: inline-block;"></button>').html(settings.on).css('margin-right', '0px').appendTo(div);
			var off = $('<button class="btn btn-danger '+settings.size+disabled+'" style="float: none;display: inline-block;"></button>').html(settings.off).css('margin-left', '0px').appendTo(div);
			var name = $(this).attr('alt');
			function applyChange(b) {
				if(b) {
					on.attr('class', 'btn active btn-' + settings.onClass+settings.size+disabled).html(settings.on).blur();
					off.attr('class', 'btn btn-default '+settings.size+disabled).html(settings.offLabel).blur();


					var status = "1";
					 $.ajax({
				      type:"POST",
				      url: "shared/send",
				      data: {name:name,status:status,_token: CSRF_TOKEN},
				      cache: false,
				      success: function(data){
				       	$('#sosmed').val(name);

				       	$(".img-facebook").attr('style',  'background:url("../img/facebook.png")no-repeat;');
				       	
						$(".img-line").attr('style',  'background:url("../img/line-hover.png")no-repeat;');
						$(".img-instagram").attr('style',  'background:url("../img/instagram.png")no-repeat;');
						
						$("#fbs").hide();
						$("#instagram").hide();
						$("#instagrams").attr('style', 'display: list-item;' );


				      },
				      error: function (data) {
				        alert("Status Line");
				        
				      }
				    });

				}
				else {
					on.attr('class', 'btn btn-default '+settings.size+disabled).html(settings.onLabel).blur();
					off.attr('class', 'btn active btn-' + settings.offClass+settings.size+disabled).text(settings.off).blur();
						


					var status = "0";
					 $.ajax({
				      type:"POST",
				      url: "shared/send",
				      data: {name:name,status:status,_token: CSRF_TOKEN},
				      cache: false,
				      success: function(data){
				       //	$('#sosmed').val("");

				       	$(".img-line").attr('style',  'background:url("../img/line.png")no-repeat;');
						$("#fb").show();
						$("#fbs").hide();
						$("#instagram").show();
						$("#instagrams").hide();

				      },
				      error: function (data) {
				        alert("Status Line");
				        
				      }
				    });
				}
			}
			applyChange(c.is(':checked'));

			on.click(function(e) {e.preventDefault();c.prop("checked", !c.prop("checked")).trigger('change')});
			off.click(function(e) {e.preventDefault();c.prop("checked", !c.prop("checked")).trigger('change')});

			$(this).hide().on('change', function() {
				applyChange(c.is(':checked'))
			});
		});
	};

	$.fn.bootstrapInstagram = function( options ) {

		var settings = $.extend({
			on: 'On',
			off: 'Off	',
			onLabel: '&nbsp;&nbsp;&nbsp;',
			offLabel: '&nbsp;&nbsp;&nbsp;',
			same: false,//same labels for on/off states
			size: 'md',
			onClass: 'primary',
			offClass: 'default'
			}, options );

		settings.size = ' btn-'+settings.size;
		if (settings.same){
			settings.onLabel = settings.on;
			settings.offLabel = settings.off;
		}

		return this.each(function(e) {
			var c = $(this);
			var disabled = c.is(":disabled") ? " disabled" : "";
			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			var div = $('<div class="btn-share btn-group btn-toggle" style="white-space: nowrap;"></div>').insertAfter(this);
			var on = $('<button class="btn btn-primary '+settings.size+disabled+'" style="float: none;display: inline-block;"></button>').html(settings.on).css('margin-right', '0px').appendTo(div);
			var off = $('<button class="btn btn-danger '+settings.size+disabled+'" style="float: none;display: inline-block;"></button>').html(settings.off).css('margin-left', '0px').appendTo(div);
			var name = $(this).attr('alt');
			function applyChange(b) {
				if(b) {
					on.attr('class', 'btn active btn-' + settings.onClass+settings.size+disabled).html(settings.on).blur();
					off.attr('class', 'btn btn-default '+settings.size+disabled).html(settings.offLabel).blur();
					

					var status = "1";
					 $.ajax({
				      type:"POST",
				      url: "shared/send",
				      data: {name:name,status:status,_token: CSRF_TOKEN},
				      cache: false,
				      success: function(data){
				       	$('#sosmed').val(name);

				       	$(".img-facebook").attr('style',  'background:url("../img/facebook.png")no-repeat;');
						$(".img-line").attr('style',  'background:url("../img/line.png")no-repeat;');
						$(".img-instagram").attr('style',  'background:url("../img/instagram-hover.png")no-repeat;');
						
					
						$("#fbs").hide();
						$("#lines").attr('style', 'display: list-item;' );
						$("#line").hide();

				      },
				      error: function (data) {
				        alert("Status Instagram");
				        
				      }
				    });
				}
				else {
					on.attr('class', 'btn btn-default '+settings.size+disabled).html(settings.onLabel).blur();
					off.attr('class', 'btn active btn-' + settings.offClass+settings.size+disabled).text(settings.off).blur();
					

					var status = "0";
					 $.ajax({
				      type:"POST",
				      url: "shared/send",
				      data: {name:name,status:status,_token: CSRF_TOKEN},
				      cache: false,
				      success: function(data){
				       		//$('#sosmed').val("");

				       		$(".img-instagram").attr('style',  'background:url("../img/instagram.png")no-repeat;');
							$("#fb").show();
							$("#fbs").hide();
							$("#line").show();
							$("#lines").hide();

				      },
				      error: function (data) {
				        alert("Status Instagram");
				        
				      }
				    });
				}
			}
			applyChange(c.is(':checked'));

			on.click(function(e) {e.preventDefault();c.prop("checked", !c.prop("checked")).trigger('change')});
			off.click(function(e) {e.preventDefault();c.prop("checked", !c.prop("checked")).trigger('change')});

			$(this).hide().on('change', function() {
				applyChange(c.is(':checked'))
			});
		});
	};
	
} 


( jQuery ));
