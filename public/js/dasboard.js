
$(document).ready(function(){ 
  
var d = new Date(),

     n = d.getMonth(),

    y = d.getFullYear();
    var months = n+01;

    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var year = currentTime.getFullYear();


highchartr(month,year);
highchartCabang(month,year);
highchartfood(month,year);
pilih();
latestmember(month,year);

getshred();






});

function getshred(){

$.ajax({
    type:"GET",  
    url:"transaksi/getshared",
    dataType: "json",
    cache: false,
    success: function(data)
    {

      var jmlData = data["shared"].length;
        var shrd = "";

       for(i=0; i<jmlData; i++)
       {
             var st = data["shared"][i]['status'];

             if(st =="0")
             {
              var status = "1";
             }else{
              var status = "0";
             } 

          var url = window.location.toString();
               var imgs = "/img/avatar.png";
               var photo =  url.imgs;
               var uimg = "70";
               if(photo != null || photo !='')
               {
                  if(st =="0")
                  {
                  photo =  data["shared"][i]['logo'];
                  saklar  ='<div class="keti fbofftext">OFF</div>';
                }else{
                   photo =  data["shared"][i]['logo_hover'];
                   saklar  ='<div class="keti fbofftext">ON</div>';
                }
               } 

           var  img ="";

               shrd +='<li id="fb" class="shared">';
                shrd +='<a id="fb-off" onclick="getklik('+ data["shared"][i]['id'] +','+status+');" class="nht" data-toggle="popover" title="Klik logo '+ data["shared"][i]['name'] +' untuk mengaktifkan" > '; 
                shrd +='<img class="pull-left " src="'+ photo +'">';
                 shrd +=''+ saklar +'';
                 shrd +='</a>'; 


                
                  shrd +='           </li>';



       } 


       $("#shrd").html(shrd);

    }
  });  
}



function getklik(id,status){
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
           $.ajax({
              type:"POST",
              url: "shared/send",
              data: {id:id,status:status,_token: CSRF_TOKEN},
              cache: false,
              success: function(respons){

               

               
                 getshred();
              
                  
               
              },
              error: function (respons) {
               
                
              }
            });



}

function highchartr(bulan,tahun){
    
    if(bulan < 10)
    {
      var mo = "0"+bulan;
    }  

  $.ajax({
    type:"GET",  
    url:"transaksi/chartr",
    data:{bulan:mo,tahun:tahun},
    dataType: "json",
    cache: false,
    success: function(respons)
    {
      
        getJsonTr(respons["chart"],respons["cabang"]);
     }
  });  

}  

function getJsonTr(chart,cabang)
{


 var arrq = chart;
var cp = cabang[0]['name'].split('#');

// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Penjualan Produk '+ cp[1] +''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Penjualan Produk '+ cp[1] +''
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: total <b>{point.y}</b><br/>'
    },

    series: [{
        name: 'Produk',
        colorByPoint: true,
        data: arrq
    }],
   
});

}

function highchartCabang(bulan,tahun){
    
    if(bulan < 10)
    {
      var mo = "0"+bulan;
    }  

  $.ajax({
    type:"GET",  
    url:"transaksi/chartcabang",
    data:{bulan:mo,tahun:tahun},
    dataType: "json",
    cache: false,
    success: function(respons)
    {
      
        getJsonCabang(respons["chart"]);
     }
  });  

}  


function getJsonCabang(chart)
{


 var arrq = chart;


// Create the chart
Highcharts.chart('container-cabang', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Penjualan Percabang'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Penjualan Percabang'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: total <b>{point.y}</b><br/>'
    },

    series: [{
        name: 'Cabang',
        colorByPoint: true,
        data: arrq
    }],
   
});

}

function highchartfood(bulan,tahun){
    
    if(bulan < 10)
    {
      var mo = "0"+bulan;
    }  

  $.ajax({
    type:"GET",  
    url:"transaksi/chartfood",
    data:{bulan:mo,tahun:tahun},
    dataType: "json",
    cache: false,
    success: function(respons)
    {
      
        getJsonFood(respons["chart"]);
     }
  });  

}  



function getJsonFood(chart)
{


 var arrq = chart;

Highcharts.chart('pieChart', {

    title: {
        text: 'Makanan & Minuman'
    },

    // xAxis: {
    //     categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    // },
     tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: total <b>{point.y}</b> <br/>'
    },

    series: [{
        name: 'Jumlah',
        type: 'pie',
        allowPointSelect: true,
        // keys: ['name', 'y', 'selected', 'sliced'],
        data: [
            ['Food', arrq[0]['food']],
            ['Beverages', arrq[0]['beverages']],
            ['Other', arrq[0]['other']]
           
        ],
        
       showInLegend: true
    }]
});


}


function pilih()
{

var d = new Date(),

     n = d.getMonth(),

    y = d.getFullYear();
    var months = n+01;

    var currentTime = new Date();
    var month = currentTime.getMonth() + 1;
    var year = currentTime.getFullYear();


                    var pilih ="";

                    pilih +="<table class='pull-right nty col-sm-12'>";
                      pilih +="<tr>";
                       pilih +="<td class='pull-left col-sm-3'><b>PILIH BULAN</b></td>";

                         
                          pilih +="<td class='pull-left col-sm-3'><b>PILIH TAHUN</b></td>";
                          pilih +="<td class='pull-left col-sm-3'></td>";  

                       pilih +="<tr>";
                           pilih +="<td class='pull-left col-sm-3'>";
                     

                          pilih +="<select    id='months' class='selectpicker'>";

                          pilih +="<option>Pilih Bulan</option>";
                          for (k = 1; k< 13; k++)
                          {
                             if(k ==1)
                             {
                              bulan = "Januari"; 
                             }else if(k ==2){
                              bulan = "Februari";
                             }else if(k ==3){
                              bulan = "Maret";
                             }else if(k ==4){
                              bulan = "April";
                             }else if(k ==5){
                              bulan = "Mei";
                             }else if(k ==6){
                              bulan = "Juni";
                             }else if(k ==7){
                              bulan = "Juli";
                             }else if(k ==8){
                              bulan = "Agustus";
                             }else if(k ==9){
                              bulan = "September";
                             }else if(k ==10){
                              bulan = "Oktober";
                             }else if(k ==11){
                              bulan = "November";
                             }else if(k ==12){
                              bulan = "Desember"; 
                             }

                          
                              
                              if(month == k)
                              {
                                pilih +="<option value='"+ k +"' selected>"+ bulan +"</option>"; 
                              }else{
                                
                                pilih +="<option value='"+ k +"'>"+ bulan +"</option>"; 

                              }
                             
                             
                          }

                          pilih +="</select>";
                       
                          pilih +="</td>";
                           pilih +="<td class='pull-left col-sm-3'>"; 

                          pilih +="<select   id='years' class='selectpicker'>";

                                pilih +="<option value='0'>Pilih Tahun</option>";
                            for (i = new Date().getFullYear(); i > 2014; i--)
                            {
                                if(year == i)
                                {
                                  pilih +="<option value='"+ y +"' selected>"+ y +"</option>"; 

                                }else{  
                                  pilih +="<option value='"+ i +"'>"+ i +"</option>"; 

                                }
                            }

                          
                          pilih +="</select>";
                            pilih +="</td>";
                             pilih +="<td class='pull-left col-sm-3'><button onclick='getcari();' class='btn btn-danger'>Cari</button></td>";
                         pilih +="</tr>";
                     pilih +="</table>";



$(".nbf").html(pilih);

}

function getcari(){
var bulan = $('#months').val();
var tahun = $('#years').val();

highchartr(bulan,tahun);
highchartCabang(bulan,tahun);
highchartfood(bulan,tahun);
latestmember(bulan,tahun);
}

function pilihBulan(){

    var bulan = $('#months').val();
    var currentTime = new Date();
    var year = currentTime.getFullYear();

    highchart(bulan,year);
}


function pilihTahun(){

var bulan = $('#months').val();
var tahun = $('#years').val();

highchart(bulan,tahun);

}

function latestmember(bulan,tahun){

  if(bulan < 10)
    {
      var mo = "0"+bulan;
    }  
            $.ajax({
              type:"GET",
              url: "transaksi/latestmember",
              data:{bulan:mo,tahun:tahun},
              dataType:"json",
              cache: false,
              success: function(data){
              var  jmlData = data['member'].length;
              var total_member =  data['total_member'];
              var total_all =  data['total_all'];
              var transaksi = data['transaksi'];
              var debit = data['debit']; 
              var tunai = data['tunai'];  
              var detail_member = data['detail_member']; 
                   var member = "";

                for(a = 0; a < jmlData; a++)
                {

                       var url = window.location.toString();
                       var img = "/img/avatar.png";
                       var photo =  url.img;
                       var uimg = "70";
                       if(photo != null || photo !='')
                       {
                          photo = data['member'][a]["photo"];
                       } 

                       var dates =  data['member'][a]['created_at'];
                       var waktu = dates.split('-');
          var tahun = waktu[0];


          var th = tahun.slice(2, 4);
          var k = waktu[1];
          var tanggal = waktu[2].split(' '); 

                            if(k ==01)
                             {
                              bulan = "Jan"; 
                             }else if(k ==02){
                              bulan = "Feb";
                             }else if(k ==03){
                              bulan = "Mar";
                             }else if(k ==04){
                              bulan = "Apr";
                             }else if(k ==05){
                              bulan = "Mei";
                             }else if(k ==06){
                              bulan = "Jun";
                             }else if(k ==07){
                              bulan = "Jul";
                             }else if(k ==08){
                              bulan = "Ags";
                             }else if(k ==09){
                              bulan = "Sep";
                             }else if(k ==10){
                              bulan = "Okt";
                             }else if(k ==11){
                              bulan = "Nov";
                             }else if(k ==12){
                              bulan = "Des"; 
                             }



                    member +='<li>';
                     member +=' <img src="'+ photo  +'" alt="User Image">';
                    member +='  <a class="users-list-name" href="#">'+ data['member'][a]["name"] +'</a>';
                     member +=' <span class="users-list-date">'+ tanggal[0]+' '+ bulan +' '+ th +'</span>';
                   member +=' </li>';


                }

                var count = "";
                count +="<span  class='label label-danger'>"+ total_member +" Member Baru</span>";

                var trx = "";
                if(transaksi[1] ==null)
                {
                  var trs = "Rp 0";
                }else{

                  var trs = toRp(transaksi[1]);
                }
                trx += "<span class='info-box-text'>Total Transaksi <b>( "+ transaksi[0] +" )</b></span><span class='info-box-number'>"+ trs +"</span>";

                var cm = "";
                cm +="<span class='info-box-text'>Total Customer <b>( "+ total_all +" )</b></span>";
                cm +="<span class='info-box-number'>Non App ( "+ detail_member[1] +" )</span>";
                cm +="<span class='info-box-number'>App ( "+ detail_member[0] +" )</span>";

                var pe = "";
                 if(debit[1] ==null)
                {
                 
                   var dbt = "Rp 0";
                }else{
                   var dbt = toRp(debit[1]);
                 
                } 
                pe +="<span class='info-box-text'>Debit <b>( "+ debit[0] +" )</b> </span><span class='info-box-number'>"+ dbt +"</span>"; 

                var ca = "";
                if(tunai[1] ==null)
                {
                 
                   var tn = "Rp 0";
                }else{
                   var tn = toRp(tunai[1]);
                 
                } 

                ca +="<span class='info-box-text'>Tunai <b>( "+ tunai[0] +" )</b></span> <span class='info-box-number'>"+ tn  +"</span>";


              
                 
                $("#latesM").html(member);
                $("#mcount").html(count);  
                $("#Trx").html(trx); 
                $("#cst").html(cm);
                 $("#pe").html(pe); 
               $("#ca").html(ca);
                //$("#lastorder").html(last);

              },
              error: function (data) {
               
                
              }
            });

}






function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
}