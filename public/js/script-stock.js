$(document).ready(function(){ 
 // load header 
var page ="";
var category = $('#category').val();
getselect();
gettablestock(category,page);
 getcat();
});


function createstokmodal(){

var view ="";

view +='<div class="modal-dialog" role="document">';
    view +='<div class="pull-left modal-content">';
      view +='<div class="pull-left modal-header col-sm-12">';
        view +='<h5 class="modal-title" id="exampleModalLongTitle">Tambah Stock</h5>';
        view +='<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
          view +='<span aria-hidden="true">&times;</span>';
        view +='</button>';
      view +='</div>';
      view +='<div class="pull-left modal-body col-sm-12">';
       
       view +='<div class="form-group col-sm-12">';
          view +='<label>Kategori :</label>'; 
          view +='<select class="form-control" id="category_id" onchange="getkatalog(this);">';
          view +='</select>';
       view +='</div>';

       view +='<div class="form-group col-sm-12">';
       view +='<div id="katalog"></div>';
       view +='</div>';

       view +='<div class="form-group col-sm-12">';
       view +='<label>Status :</label></br>'; 
        view +='<input id="status1" type="radio" name="status"  onclick="statusval1();"> Stok Habis </br>';
        view +='<input id="status2"  type="radio" name="status" onclick="statusval2();"> Stok Ready </br>';
       view +='</div>';


      view +='</div>';
      view +='<div class="pull-left modal-footer col-sm-12">';
        
        view +='<button onclick="createstock();" type="button" class="btn btn-danger" data-dismiss="modal">Simpan</button>';
      view +='</div>';
    view +='</div>';
  view +='</div>';
 
$('#createstok').html(view);

getcategori();

}

function statusval1(){
  $("#status1").val('1');
  $("#status2").val('');
}

function statusval2(){
  $("#status1").val('');
  $("#status2").val('2');
}


function getcategori(){

$.ajax({
  type:"GET",  
    url:"stock/getcategori",
   
    cache: false,
    success: function(respons){
      
       var vkategori ="";
       jml = respons['data'].length;
       kat = respons['data'];

       vkategori +='<option value="0">Pilih Kategori</option>';
       for(i=0; i<jml; i++)
       { 

       vkategori +="<option value='"+ kat[i]['id'] +"'>"+ kat[i]['name'] +"</option>";

       }  

       $('#category_id').html(vkategori);
   },
    error: function (respons) {
     
        
      }
  });

}

function getcat(){

$.ajax({
  type:"GET",  
    url:"stock/getcategori",
   
    cache: false,
    success: function(respons){
      
       var vkategori ="";
       jml = respons['data'].length;
       kat = respons['data'];

       vkategori +='<option value="0">Pilih Kategori</option>';
       for(i=0; i<jml; i++)
       { 

       vkategori +="<option value='"+ kat[i]['id'] +"'>"+ kat[i]['name'] +"</option>";

       }  

       $('#category').html(vkategori);
   },
    error: function (respons) {
     
        
      }
  });

}

function gettablestock(category,page)
{

  if(category !="")
  {
    category_id = $('#category').val();
  }else{
    category_id = category;
  }  



$("#stokis").html('<tr><td colspan="3"><div class="loading"><img src="css/images/loading.gif" /><br />Loading...</div></td></tr>');
$.ajax({
  type:"GET", 
    data:{category_id:category_id,page:page}, 
    url:"stock/getstock",
    dataType: "json",

    cache: false,
    success: function(respons){
         var st ="";
       var jml = respons['data'].length;
       var stok = respons['data'];

       if(stok =="")
       {
          st +='<tr>';
          st +='<td colspan="5">Stock Kosong</td>';
          st +='</tr>';

       } 

      
       for(i=0; i<jml; i++)
       {

        if(stok[i]['status'] ==1)
        {
          var stt = "Stok Habis";
          var harga = "<p class='coret'>"+toRp(stok[i]['price'])+"</p>";
           var katalog = "<p class='coret'>"+stok[i]['title']+"</p>";
        }else{
          var stt = "Stok Ready";
          var harga = "<p >"+toRp(stok[i]['price'])+"</p>";
          var katalog = "<p>"+ stok[i]['title']+"</p>";
        }  
          st +='<tr>';
          st +='<td>'+ stok[i]['kategori'] +'</td>';
          st +='<td>'+ katalog +'</td>';
          st +='<td>'+ harga +'</td>';
          st +='<td>'+ stt +'</td>';
           st +='<td><button onclick="editstock('+ stok[i]['id'] +');" class="btn btn-default" type="button"><i class="fa fa-pencil"></i></button>';
           st +='<button onclick="deletestock('+ stok[i]['id'] +');" class="btn btn-default" type="button"><i class="fa fa-trash"></i></button></td>';
          st +='</tr>';
       } 

       $('#stokis').html(st);

       var buatFirst ="";
       var  buatPage ="";
       var buatLast ="";
       if(respons['from'] != null)
        {
           var last_page = respons['last_page']; 
           
              buatFirst += "<button  class='btn btn-default' onClick='Getpage(1);'>«</button>";
                 
                  
              for(i = 1; i < last_page+1; i++)
             {
                    
                  if(respons['from'] ==1)
                  {
                    if([i] ==1)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    } 

                  }else if(page == respons['current_page']){

                    if([i] == page)
                    {
                      var aktif = "active";
                    }else{
                      var aktif = "";
                    }  
                 
                  }else{
                    var aktif = "";
                  }

                  

                     buatPage += "<button  class='"+ aktif + " btn btn-default' onClick='Getpage(" + [i] + ");'>" + [i] + "</button>";
                   
             } 

              buatLast += "<button id='nextPage' class='btn btn-default' onClick='Getpage(" + respons['last_page']+");''>»</button>";
             

           
            
         }

          $(".first").html(buatFirst);
         $(".navigation").html(buatPage);
         $(".last").html(buatLast);
         
   },
    error: function (respons) {
     
        
      }
  });


}


function Getpage(page){
  var pages = page;
 var category = $('#category').val();
 
  if(pages ==null | pages ==""){
    
  


  }else{
gettablestock(category,page);
   
  }
}



function editstock(id){
$.ajax({
  type:"GET",  
    url:"stock/editstock",
    data:{id:id},
    cache: false,
    success: function(respons){
 var page = ""; 
  var category = $('#category').val();     
gettablestock(category,page);
        
 
         
   },
    error: function (respons) {
     
        
      }
  });



}


function deletestock(id){
$.ajax({
  type:"GET",  
    url:"stock/deletestock",
    data:{id:id},
    cache: false,
    success: function(respons){
     var page = ""; 
     var category = $('#category').val(); 
gettablestock(category,page);
        
 
         
   },
    error: function (respons) {
     
        
      }
  });



}


function getselect(){
   var view = "";  
     view +="<label >Katalog:</label>"; 
     view +="<select name='post_id' class='form-control'>";   
                    view +="<option value='0'>Pilih katalog</option>";

        view +="</select>";  
         $("#katalog").html(view);            
}

function getkatalog(){
   var category_id = $('#category_id').val();
 

 $.ajax({
  type:"GET",  
    url:"stock/getcatalog",
    dataType: "json",
    data:{category_id:category_id},
    cache: false,
    success: function(respons){
      
         jmlData = respons['data'].length;
                 var view = "";   
                     view +="<label >Katalog :</label>";  
                 
                  view +="<select id='post_id' name='post_id' class='form-control '>";   
                    view +="<option value='0'>Pilih katalog</option>";
                for(a = 0; a < jmlData; a++)
                {  
                        view +="<option value='"+ respons['data'][a]["id"] +"'>"+ respons['data'][a]["title"] +"</option>";  
                }   
                    view +="</select>"; 

                    $("#katalog").html(view);
         
   },
    error: function (respons) {
     
        
      }
  });



}


function createstock(){
  var category_id = $('#category_id').val();
  var post_id = $('#post_id').val();
  var status1 = $('#status1').val();
  var status2 = $('#status2').val();


  if(category_id =="0")
  {
    var alt = "";
     alt +='<ul class="alert alert-danger" style="list-style-type: none">';
                            alt +='<li>Kategori belum dipilih !!</li>';
                    alt +='</ul>';

    $("#alertstock").show().eq(0).delay(1500).fadeOut('slow').html(alt);
    
  }

  if(post_id =="0")
  {

     var alt = "";
     alt +='<ul class="alert alert-danger" style="list-style-type: none">';
                            alt +='<li>Katalog belum dipilih !!</li>';
                    alt +='</ul>';

    $("#alertstock").show().eq(0).delay(1500).fadeOut('slow').html(alt);
  }else{


    $.ajax({
  type:"GET",  
    url:"stock/checkstock",
    dataType: "json",
    data:{post_id:post_id},
    cache: false,
    success: function(respons){
    

      if(respons['data'] =="")
      {

        if(status1 =="on")
        { // kosong

                if(status2 !="on")
                {
                  // kosong nda
                 sendstock(category_id,post_id,status1,status2);

                }else{
                  //kosong kosong
                  var alt = "";
                  alt +='<ul class="alert alert-danger" style="list-style-type: none">';
                  alt +='<li>Status belum dipilih !!</li>';
                  alt +='</ul>';

                  $("#alertstock").show().eq(0).delay(1500).fadeOut('slow').html(alt);

                }


             }else{
                  //nda
                  sendstock(category_id,post_id,status1,status2);
             }

      
      }else{

          if(respons['data'][0]['post_id'] == post_id)
          {

             var alt = "";
              alt +='<ul class="alert alert-danger" style="list-style-type: none">';
              alt +='<li>Katalog sudah di inputkan sebelumnya !!</li>';
              alt +='</ul>';

              $("#alertstock").show().eq(0).delay(1500).fadeOut('slow').html(alt);
          
          }


      }  

         
   },
    error: function (respons) {
     
        
      }
  });


  }








}



function sendstock(category_id,post_id,status1,status2)
{


var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 
$.ajax({
  type:"POST",  
    url:"stock/store",
    data:{category_id:category_id,post_id:post_id,status1:status1,status2:status2,_token: CSRF_TOKEN},
    cache: false,
    success: function(respons){
       
      var page = "";
      var category = $('#category').val();
      gettablestock(category,page);
      
   },
    error: function (respons) {
      //alert("Gagal update noted");
        
      }
  });
}


function toRp(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return 'Rp. ' + rev2.split('').reverse().join('') + ',00';
}

function toRek(angka){
    var rev     = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2    = '';
    for(var i = 0; i < rev.length; i++){
        rev2  += rev[i];
        if((i + 1) % 3 === 0 && i !== (rev.length - 1)){
            rev2 += '.';
        }
    }
    return '' + rev2.split('').reverse().join('') + '';
}